﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Consignela.Model;
using Windows.Storage;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

//include
using Newtonsoft.Json;
using Windows.UI.Popups;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Graphics.Imaging;
using Windows.Storage.AccessCache;
using System.IO;
//using System.Windows.Forms;


namespace Consignela
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AjoutConsigne : Page
    {
        internal static String imgPath = null;
        internal static String imgNom = null;
        internal static int i,j,k;
        private int index;
        internal List<ListConsignes> ListConsTemp;
        internal List<Consigne> consignes = new List<Consigne>();
        private ObservableCollection<Consigne> ListLignesConsignes;
        private Collection<Consigne> listConsignetmp = new Collection<Consigne>();
        internal Consigne consigneTmp = new Consigne();
        internal StorageFile sampleFile;
        byte[] bytes;


        String sourcePath ;
        String targetPath ;


        //private List<Consigne> ListLignesConsignes;


        public AjoutConsigne()
        {
            this.InitializeComponent();
            
            this.loadConsignesData();
            j = 0;
            k = 0;
            index = 0;
            ListLignesConsignes = new ObservableCollection<Consigne>();

            System.Diagnostics.Debug.WriteLine(DateTime.Now.Date.ToString("d"));
            
        }

        private void btn_menu_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }


        private async void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(nomListeConsignes.Text) && listConsignetmp.Count() > 1)
            {
                List<ListConsignes> listListConsignes = new List<ListConsignes>();
                Date date = new Date();
                date.day = DateTime.Today.Day.ToString();
                date.month = DateTime.Today.Month.ToString();
                date.month = date.monthToStr();
                date.year = DateTime.Today.Year.ToString();
                ListConsignes listConsigne = new ListConsignes { id = i, nom = nomListeConsignes.Text, date = DateTime.Today.Date.ToString("dd-MM-yyyy"), dateTime = DateTime.Today.Date, dateLettre = date };
                listConsigne.consignes = listConsignetmp.Cast<Consigne>().ToList();

                if (ListConsTemp != null)
                {
                    listListConsignes = ListConsTemp;
                }
                listListConsignes.Add(listConsigne);
                //sampleFile = await App.storageFolder.GetFileAsync("ConsignesData.json");
                await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
                var stream = await sampleFile.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFile.Path);


                feedbackAjout();
            }
            else
            {
                MessageDialog erreurAjout = new MessageDialog("Veuillez vérifiez tous les champs s.v.p");
                await erreurAjout.ShowAsync();

            }


        }
        private async void feedbackAjout()
        {

            MessageDialog selectConsigne = new MessageDialog("Consignes ajoutées avec succées", "Ajout des consignes");
            await selectConsigne.ShowAsync();
        }

   
        private void btn_AjoutConsigneN_AccessKeyDisplayRequested(UIElement sender, AccessKeyDisplayRequestedEventArgs args)
        {

        }

       

        private void listViewListConsigne_Tapped(object sender, TappedRoutedEventArgs e)
        {
           // consigneTmp = (Consigne)listViewListConsigne.SelectedItem;         
        }

        private async void btn_Ajout_img_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;

            System.Diagnostics.Debug.Write(listViewListConsigne.SelectedItem);
            System.Diagnostics.Debug.Write(consigneTmp);

            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            StorageFile file = await picker.PickSingleFileAsync();
            //string token = StorageApplicationPermissions.FutureAccessList.Add(file);

            //retrieve file mba3ad     StorageFile file1 =   await StorageApplicationPermissions.FutureAccessList.GetFileAsync(token);


            if (file != null)
            {
                imgNom = file.Name;

                if (await App.imageFolder.TryGetItemAsync(imgNom) != null)
                    System.Diagnostics.Debug.WriteLine(imgNom + " exists.");
                else  // Return value of TryGetItemAsync is null.
                    await file.CopyAsync(App.imageFolder);
                var stream = await file.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), file.Path);


                //imgPath = "Assets/" + imgNom;

                System.Diagnostics.Debug.Write(consigneTmp);

                var streamImage = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var imageBtm = new BitmapImage();
                
                imageBtm.SetSource(streamImage);
                //imageBtm.UriSource = new Uri(file.Path, UriKind.RelativeOrAbsolute);
                ListLignesConsignes[consigneTmp.id].imgBitmap = imageBtm;
                ListLignesConsignes[consigneTmp.id].img_path = file.Path; 
                ListLignesConsignes[consigneTmp.id].imgNom = imgNom;

                chargerListConsignetmp();

            }
            else
            {
                //this.tb_img_name.Text = "aucune image";
            }
        }
        public static void FillDecoderExtensions(IList<string> fileTypeFilter)
        {
            IReadOnlyList<BitmapCodecInformation> codecInfoList =
                BitmapDecoder.GetDecoderInformationEnumerator();

            foreach (BitmapCodecInformation decoderInfo in codecInfoList)
            {
                // Each bitmap codec contains a list of file extensions it supports; add each
                // list item to fileTypeFilter.
                foreach (string extension in decoderInfo.FileExtensions)
                {
                    fileTypeFilter.Add(extension);
                }
            }
        }
        private void chargerListConsignetmp()
        {

            if (ListLignesConsignes != null)
            {
                listConsignetmp = ListLignesConsignes;
            }
            listViewListConsigne.ItemsSource = null;
            listViewListConsigne.ItemsSource = listConsignetmp;
            //   System.Diagnostics.Debug.Write(ListLignesConsignes);
            System.Diagnostics.Debug.Write(listConsignetmp);
        }

        private void delete_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            if (listConsignetmp.Count > 1)
            {
                
                listConsignetmp.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConsignetmp)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmp;

            }

        }

        private void TextBox_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        //private void up_Click(object sender, RoutedEventArgs e)
        //{
        //    consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
        //    int indexConsigne = listConsignetmp.IndexOf(consigneTmp);

            
        //    listConsignetmp[indexConsigne].id--;
        //    listConsignetmp[indexConsigne - 1].id++;

        //    listConsignetmp[indexConsigne] = listConsignetmp[indexConsigne - 1];
        //    listConsignetmp[indexConsigne - 1] = consigneTmp;
            

           
        //    listViewListConsigne.ItemsSource = null;
        //    listViewListConsigne.ItemsSource = listConsignetmp;

        //}

     

        private void up_Tapped(object sender, TappedRoutedEventArgs e)
        {
          
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmp.IndexOf(consigneTmp);
            if(indexConsigne > 0)
            {
                listConsignetmp[indexConsigne].id--;
                listConsignetmp[indexConsigne - 1].id++;

                listConsignetmp[indexConsigne] = listConsignetmp[indexConsigne - 1];
                listConsignetmp[indexConsigne - 1] = consigneTmp;



                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmp;
            }
            
        }
   
        private void down_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmp.IndexOf(consigneTmp);

            if(indexConsigne < listConsignetmp.Count()-1)
            {
                listConsignetmp[indexConsigne].id++;
                listConsignetmp[indexConsigne + 1].id--;

                listConsignetmp[indexConsigne] = listConsignetmp[indexConsigne + 1];
                listConsignetmp[indexConsigne + 1] = consigneTmp;



                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmp;
            }
            
        }

        private void btn_ajoutLigneConsigne_Click(object sender, RoutedEventArgs e)
        {
            chargerListConsignetmp();
            if (listConsignetmp.Count() != 0)
            {
                index = listConsignetmp[listConsignetmp.Count - 1].id + 1;
            }
            else
            {
                index = 0;
            }

            Consigne ligneConsigne = new Consigne { id = index,description="",img_path= "Assets/icons/addimg.png" };
            ListLignesConsignes.Add(ligneConsigne);
            index++;
            
        }

        private async void loadConsignesData()
        {
            //imageFolder = await App.storageFolder.GetFolderAsync("images");

            //targetPath = imageFolder.Path;

            if (App.authentification)
            {
                sampleFile = await App.storageFolder.GetFileAsync(App.logIn + "-consignes.json");
            }
            else
            {
                sampleFile = await App.storageFolder.GetFileAsync("ConsignesData.json");
            }
            
            if(sampleFile != null)
            {
                string ConsigneStr = await FileIO.ReadTextAsync(sampleFile);

                ListConsTemp = JsonConvert.DeserializeObject<List<ListConsignes>>(ConsigneStr);
                if (ListConsTemp.Count != 0)
                {
                    i = ListConsTemp[ListConsTemp.Count - 1].id+1;
                }
                else
                {
                    i = 0;
                }
                
            }
            else
            {
                MessageDialog fileMissing = new MessageDialog("Le fichier est introuvable", "Fichier introuvable");
                await fileMissing.ShowAsync();
            }
            
        }

        
    }


}
