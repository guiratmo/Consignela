﻿using System;
using System.Collections.Generic;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Consignela.Model;
using Newtonsoft.Json;
using Windows.UI.Popups;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Web.Http;

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Select_Presc_Page : Page
    {
        internal String str_Consigne_json;
        internal List<ListConsignes> listListConsignes;
        internal List<Consigne> listConsigneTmp = new List<Consigne>();

        internal String nomConsigne;
        internal bool consChoisie = false;
        internal String msgFeedback;
        internal ListConsignes listConstmp;
        private ListConsignes listConstmp2 = new ListConsignes();
        int indexConstmp;

        int a = 0, b = 0;

        private int index, i, indexDebut, indexFin;
        internal Consigne consigneTmp = new Consigne();
        internal static String imgPath = null;
        internal static String imgNom = null;
        internal String TxtResponse;
        public static StorageFile sampleFile;

        List<string> nameList = new List<string>();
        List<string> elementList = new List<string>();
        List<CheckBox> checkBoxListSharingSpace = new List<CheckBox>();
        public static List<CheckBox> checkBoxListIndex = new List<CheckBox>();

        static List<string> idSharingSpace = new List<string>();
        byte[] bytes;
        public StorageFile downloadedFile;
        public int indeximages = 0;
        public bool searchName = false, searchDate = false;
        //

        List<Prescription> newListPrescriptions;
        List<Medicament> newMedicament;
        internal String strJsonPresc;
        internal bool prescChoisie = false;
        internal String nomPrescription;

        internal List<Medicament> tabListMedicaments;
        internal List<PrescriptionTab> prescriptionTabList;
        internal String strJsonTabPresc;
        //internal String nomMomentJour;
        internal List<GridView> gridViewTabMedicament = new List<GridView>();
        internal Prescription prescriptionTmp;
        internal StorageFile sampleFilePrescription;
        public static StorageFile sampleFilePrescriptionTab;

        public Select_Presc_Page()
        {
            
            this.InitializeComponent();
          
           
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadPrescription();
            loadPrescriptionTabList();
            loadGridView();

            tgb_tabulaire.IsChecked = false;
            tgb_Verbale.IsChecked = true;

            prescTabulaire.Opacity = 0;
            typePresButton.Visibility = Visibility.Collapsed;


        }
        public static string getJsonFromUrl(string url)
        {
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            var response = client.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                // by calling .Result you are performing a synchronous call
                var responseContent = response.Content;

                // by calling .Result you are synchronously reading the result
                string responseString = responseContent.ReadAsStringAsync().Result;

                return responseString;
            }
            else
            {
                return "error";
            }

        }


        //private void back_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.Frame.CanGoBack)
        //    {
        //        this.Frame.GoBack();
        //    }
        //}

        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        public async void loadPrescription()
        {
            if (App.authentification)
            {

                sampleFilePrescription = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionData.json");
                strJsonPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-PrescriptionData.json");
                newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
                await FileIO.WriteTextAsync(sampleFilePrescription, JsonConvert.SerializeObject(newListPrescriptions));
            }
            else
            {
                sampleFilePrescription = await App.storageFolder.GetFileAsync("PrescriptionData.json");
                strJsonPresc = await FileIO.ReadTextAsync(sampleFilePrescription);
                newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
            }





            //if (App.authentification)
            //{
            //    sampleFilePrescription = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionData.json");
            //    strJsonPresc = await FileIO.ReadTextAsync(sampleFilePrescription);
            //    //strJsonPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-Prescription.json");
            //}
            //else
            //{
            //    sampleFilePrescription = await App.storageFolder.GetFileAsync("PrescriptionData.json");
            //    strJsonPresc = await FileIO.ReadTextAsync(sampleFilePrescription);
            //}
             
            //    newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);

                gridViewPresc.ItemsSource = null;
                gridViewPresc.ItemsSource = newListPrescriptions;
                //newListPrescriptions.Reverse();                
        }

        public async void loadPrescriptionTabList()
        {
            if (App.authentification)
            {
                sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionTabData.json");
                strJsonTabPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-PrescriptionTabData.json");
                prescriptionTabList = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);
                await FileIO.WriteTextAsync(sampleFilePrescriptionTab, JsonConvert.SerializeObject(prescriptionTabList));
            }
            else
            {
                sampleFilePrescription = await App.storageFolder.GetFileAsync("PrescriptionTabData.json");
                strJsonTabPresc = await FileIO.ReadTextAsync(sampleFilePrescriptionTab);
                prescriptionTabList = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);
            }




            //if (App.authentification)
            //{
            //    sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionTabData.json");
            //    strJsonTabPresc = await FileIO.ReadTextAsync(sampleFilePrescriptionTab);
            //    //strJsonTabPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-PrescriptionTabData.json");
            //}
            //else
            //{
            //    sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync("PrescriptionTabData.json");
            //    strJsonTabPresc = await FileIO.ReadTextAsync(sampleFilePrescriptionTab);
            //}
            //    //strJsonTabPresc = File.ReadAllText(@"PrescriptionTabData.json");
            //    prescriptionTabList = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);
           
        }

        private void gridViewPresc_Tapped(object sender, TappedRoutedEventArgs e)
        {
            info_label.Visibility = Visibility.Collapsed;
            typePresButton.Visibility = Visibility.Visible;

            prescriptionTmp = (Prescription)gridViewPresc.SelectedItem;
            newMedicament = newListPrescriptions[prescriptionTmp.id].medicaments;

            listViewSelectPresc.ItemsSource = null;
            listViewSelectPresc.ItemsSource = newMedicament;

            gridViewNomsMedicaments.ItemsSource = null;
            gridViewNomsMedicaments.ItemsSource = newMedicament;

            nomPrescription = newListPrescriptions[prescriptionTmp.id].nom;
            tb_nomCons_bar.Text = nomPrescription;

            for (int i = 0; i < 21; i++)
            {
                if ((prescriptionTabList[prescriptionTmp.id].momentJour[i].listMedicaments) != null)
                {                 
                    tabListMedicaments = prescriptionTabList[prescriptionTmp.id].momentJour[i].listMedicaments;
                    gridViewTabMedicament[i].ItemsSource = null;
                    gridViewTabMedicament[i].ItemsSource = tabListMedicaments;
                }
            }
            prescChoisie = true;

        }

        private void gridViewPresc_ItemClick(object sender, ItemClickEventArgs e)
        {
     
        }

        private async void btn_selectPresc_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (prescChoisie)
            {
                if (App.getPrescriptionSelect() == null)
                {
                    MainMenu.progressBarValue += 50;
                }
                
                App.setPrescriptionSelect(newListPrescriptions[prescriptionTmp.id]);
                App.prescriptionTabSelect = prescriptionTabList[prescriptionTmp.id];
                msgFeedback = "la prescription " + nomPrescription + " a bien été  choisie";
            }
            else
            {
                msgFeedback = "La prescription est non choisise";
            }
            
            MessageDialog selectConsigne = new MessageDialog(msgFeedback, "Selection de la prescription");
            await selectConsigne.ShowAsync();
        }

        private void tgb_Verbale_Click(object sender, RoutedEventArgs e)
        {
            if (tgb_Verbale.IsChecked == true)
            {
                
                prescTabulaire.Opacity = 0;
                listViewSelectPresc.Opacity = 1;
                tgb_tabulaire.IsChecked = false;
            }
        }

        private void tgb_tabulaire_Click(object sender, RoutedEventArgs e)
        {
            if (tgb_tabulaire.IsChecked == true)
            {
                
                prescTabulaire.Opacity = 1;
                listViewSelectPresc.Opacity = 0;
                tgb_Verbale.IsChecked = false;
            }
          
        }

        private void searchPrescription_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(searchPrescription.Text))
            {
                gridViewPresc.ItemsSource = null;
                gridViewPresc.ItemsSource = newListPrescriptions.FindAll(listConsigne => listConsigne.nom.Contains(searchPrescription.Text));
            }
            else
            {
                gridViewPresc.ItemsSource = null;
                gridViewPresc.ItemsSource = newListPrescriptions;
            }
        }
        private void OnLayoutUpdatedIndex(object sender, object e)
        {
            if (indexPopupChild.ActualWidth == 0 && indexPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.indexPopup.HorizontalOffset;
            double ActualVerticalOffset = this.indexPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - indexPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - indexPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.indexPopup.HorizontalOffset = NewHorizontalOffset;
                this.indexPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void OnLayoutUpdatedSharing(object sender, object e)
        {
            if (sharingSpacePopupChild.ActualWidth == 0 && sharingSpacePopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.sharingSpacePopup.HorizontalOffset;
            double ActualVerticalOffset = this.sharingSpacePopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - sharingSpacePopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - sharingSpacePopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.sharingSpacePopup.HorizontalOffset = NewHorizontalOffset;
                this.sharingSpacePopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void OnLayoutUpdatedAuthen(object sender, object e)
        {
            if (authenPopupChild.ActualWidth == 0 && authenPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void quitIndexPopup_Click(object sender, RoutedEventArgs e)
        {
            if (indexPopup.IsOpen)
            {
                indexPopup.IsOpen = false;
            }

        }

        //************ à prendre ***
        private async void indexSave_Click(object sender, RoutedEventArgs e)
        {
            Index.showInfo(checkBoxListIndex, sampleFilePrescriptionTab,idSharingSpace);
            MessageDialog saveServerFile = new MessageDialog("La prescription a bien été enregistrée");
            await saveServerFile.ShowAsync();
        }
        private void quitSharingPopup_Click(object sender, RoutedEventArgs e)
        {
            if (sharingSpacePopup.IsOpen)
            {
                sharingSpacePopup.IsOpen = false;
            }
        }

        private async void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            await FileIO.WriteTextAsync(sampleFilePrescriptionTab, JsonConvert.SerializeObject(prescriptionTabList));
            enregistrementServeur();
        }


        private async void enregistrementServeur()

        {
            if (!authenPopup.IsOpen && App.authentification == false)
            {
                authenPopup.IsOpen = true;

            }
            else
            {
                if (!sharingSpacePopup.IsOpen)
                {
                    sharingSpacePopup.IsOpen = true;
                   
                }
                var stream = await sampleFilePrescriptionTab.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFilePrescriptionTab.Path);

                //SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();
                for (int i = 0; i < a; i++)
                {

                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }
            }
        }

        //*********************

        public void showSharingSpaceInfo()
        {
            idSharingSpace.Clear();
            // foreach (CheckBox checkbox in checkBoxListSharingSpace)
            for (int i = 0; i < checkBoxListSharingSpace.Count; i++)
            {
                if (checkBoxListSharingSpace[i].IsChecked == true)
                {
                    string[] split = elementList[i].Split('#');
                    idSharingSpace.Add(split[1]);
                }
            }
        }
        private async void showShSpInfo_Click(object sender, RoutedEventArgs e)
        {
            showSharingSpaceInfo();
            if (idSharingSpace.Count != 0)
            {

                Index.getIndex();
                List<string> nom_cpt = Index.getnom_cpt();
                checkBoxListIndex.Clear();
                IndexStackPanel1.Children.Clear();

                for (int i = 0; i < nom_cpt.Count; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nom_cpt[i];
                    newCheckBox.Name = "checkbox" + i;

                    checkBoxListIndex.Add(newCheckBox);
                    IndexStackPanel1.Children.Add(newCheckBox);

                }
                if (!indexPopup.IsOpen) { indexPopup.IsOpen = true; }
            }
            else
            {
                MessageDialog SharingSpaceNotSelected = new MessageDialog("Veuillez séléctionner l'espace de partage");
                await SharingSpaceNotSelected.ShowAsync();
            }

        }
        private void quitAuthenPopup_Click(object sender, RoutedEventArgs e)
        {
            if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
        }
        public async Task Post(string website, string args)
        {

            //init client
            Windows.Web.Http.HttpClient hc = new Windows.Web.Http.HttpClient();
            //send post request
            Windows.Web.Http.HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            TxtResponse = await hrm.Content.ReadAsStringAsync();
            App.txtresponseLogin = TxtResponse;
            //System.Diagnostics.Debug.Write(TxtResponse);
            hc.Dispose();
        }

        private async void connecter_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login.Text + "\"" + ",\"pass\":" + "\"" + pass.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login.Text;
                App.password = pass.Password;
                App.authentification = true;

                if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
                //fileToByte();
                var stream = await sampleFilePrescriptionTab.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFilePrescriptionTab.Path);

                SharingSpace.getSharingSapce();
                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();

                for (int i = 0; i < a; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }



            }
            else
            {
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
        }


    }
}
