﻿#pragma checksum "E:\mohamed\Consignela\code\Consignela\Consignela\Pilulier.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9A799A705CB39E43785C9A2474120A94"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Consignela
{
    partial class Pilulier : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        internal class XamlBindingSetters
        {
            public static void Set_Windows_UI_Xaml_Controls_ItemsControl_ItemsSource(global::Windows.UI.Xaml.Controls.ItemsControl obj, global::System.Object value, string targetNullValue)
            {
                if (value == null && targetNullValue != null)
                {
                    value = (global::System.Object) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::System.Object), targetNullValue);
                }
                obj.ItemsSource = value;
            }
            public static void Set_Windows_UI_Xaml_Controls_Image_Source(global::Windows.UI.Xaml.Controls.Image obj, global::Windows.UI.Xaml.Media.ImageSource value, string targetNullValue)
            {
                if (value == null && targetNullValue != null)
                {
                    value = (global::Windows.UI.Xaml.Media.ImageSource) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::Windows.UI.Xaml.Media.ImageSource), targetNullValue);
                }
                obj.Source = value;
            }
            public static void Set_Windows_UI_Xaml_Controls_TextBlock_Text(global::Windows.UI.Xaml.Controls.TextBlock obj, global::System.String value, string targetNullValue)
            {
                if (value == null && targetNullValue != null)
                {
                    value = targetNullValue;
                }
                obj.Text = value ?? global::System.String.Empty;
            }
        };

        private class Pilulier_obj2_Bindings :
            global::Windows.UI.Xaml.IDataTemplateExtension,
            global::Windows.UI.Xaml.Markup.IComponentConnector,
            IPilulier_Bindings
        {
            private global::Consignela.Medicament dataRoot;
            private bool initialized = false;
            private const int NOT_PHASED = (1 << 31);
            private const int DATA_CHANGED = (1 << 30);
            private bool removedDataContextHandler = false;

            // Fields for each control that has bindings.
            private global::Windows.UI.Xaml.Controls.Image obj3;
            private global::Windows.UI.Xaml.Controls.TextBlock obj4;

            public Pilulier_obj2_Bindings()
            {
            }

            // IComponentConnector

            public void Connect(int connectionId, global::System.Object target)
            {
                switch(connectionId)
                {
                    case 3:
                        this.obj3 = (global::Windows.UI.Xaml.Controls.Image)target;
                        break;
                    case 4:
                        this.obj4 = (global::Windows.UI.Xaml.Controls.TextBlock)target;
                        break;
                    default:
                        break;
                }
            }

            public void DataContextChangedHandler(global::Windows.UI.Xaml.FrameworkElement sender, global::Windows.UI.Xaml.DataContextChangedEventArgs args)
            {
                 global::Consignela.Medicament data = args.NewValue as global::Consignela.Medicament;
                 if (args.NewValue != null && data == null)
                 {
                    throw new global::System.ArgumentException("Incorrect type passed into template. Based on the x:DataType global::Consignela.Medicament was expected.");
                 }
                 this.SetDataRoot(data);
                 this.Update();
            }

            // IDataTemplateExtension

            public bool ProcessBinding(uint phase)
            {
                throw new global::System.NotImplementedException();
            }

            public int ProcessBindings(global::Windows.UI.Xaml.Controls.ContainerContentChangingEventArgs args)
            {
                int nextPhase = -1;
                switch(args.Phase)
                {
                    case 0:
                        nextPhase = -1;
                        this.SetDataRoot(args.Item as global::Consignela.Medicament);
                        if (!removedDataContextHandler)
                        {
                            removedDataContextHandler = true;
                            ((global::Windows.UI.Xaml.Controls.StackPanel)args.ItemContainer.ContentTemplateRoot).DataContextChanged -= this.DataContextChangedHandler;
                        }
                        this.initialized = true;
                        break;
                }
                this.Update_((global::Consignela.Medicament) args.Item, 1 << (int)args.Phase);
                return nextPhase;
            }

            public void ResetTemplate()
            {
            }

            // IPilulier_Bindings

            public void Initialize()
            {
                if (!this.initialized)
                {
                    this.Update();
                }
            }
            
            public void Update()
            {
                this.Update_(this.dataRoot, NOT_PHASED);
                this.initialized = true;
            }

            public void StopTracking()
            {
            }

            // Pilulier_obj2_Bindings

            public void SetDataRoot(global::Consignela.Medicament newDataRoot)
            {
                this.dataRoot = newDataRoot;
            }

            // Update methods for each path node used in binding steps.
            private void Update_(global::Consignela.Medicament obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_imageMedicament(obj.imageMedicament, phase);
                        this.Update_nbrTab(obj.nbrTab, phase);
                    }
                }
            }
            private void Update_imageMedicament(global::Consignela.Model.MediImages obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_imageMedicament_imageUri(obj.imageUri, phase);
                    }
                }
            }
            private void Update_imageMedicament_imageUri(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_Image_Source(this.obj3, (global::Windows.UI.Xaml.Media.ImageSource) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::Windows.UI.Xaml.Media.ImageSource), obj), null);
                }
            }
            private void Update_nbrTab(global::System.Int32 obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_TextBlock_Text(this.obj4, obj.ToString(), null);
                }
            }
        }

        private class Pilulier_obj28_Bindings :
            global::Windows.UI.Xaml.IDataTemplateExtension,
            global::Windows.UI.Xaml.Markup.IComponentConnector,
            IPilulier_Bindings
        {
            private global::Consignela.Medicament dataRoot;
            private bool initialized = false;
            private const int NOT_PHASED = (1 << 31);
            private const int DATA_CHANGED = (1 << 30);
            private bool removedDataContextHandler = false;

            // Fields for each control that has bindings.
            private global::Windows.UI.Xaml.Controls.TextBlock obj29;
            private global::Windows.UI.Xaml.Controls.Image obj30;
            private global::Windows.UI.Xaml.Controls.TextBlock obj31;

            public Pilulier_obj28_Bindings()
            {
            }

            // IComponentConnector

            public void Connect(int connectionId, global::System.Object target)
            {
                switch(connectionId)
                {
                    case 29:
                        this.obj29 = (global::Windows.UI.Xaml.Controls.TextBlock)target;
                        break;
                    case 30:
                        this.obj30 = (global::Windows.UI.Xaml.Controls.Image)target;
                        break;
                    case 31:
                        this.obj31 = (global::Windows.UI.Xaml.Controls.TextBlock)target;
                        break;
                    default:
                        break;
                }
            }

            public void DataContextChangedHandler(global::Windows.UI.Xaml.FrameworkElement sender, global::Windows.UI.Xaml.DataContextChangedEventArgs args)
            {
                 global::Consignela.Medicament data = args.NewValue as global::Consignela.Medicament;
                 if (args.NewValue != null && data == null)
                 {
                    throw new global::System.ArgumentException("Incorrect type passed into template. Based on the x:DataType global::Consignela.Medicament was expected.");
                 }
                 this.SetDataRoot(data);
                 this.Update();
            }

            // IDataTemplateExtension

            public bool ProcessBinding(uint phase)
            {
                throw new global::System.NotImplementedException();
            }

            public int ProcessBindings(global::Windows.UI.Xaml.Controls.ContainerContentChangingEventArgs args)
            {
                int nextPhase = -1;
                switch(args.Phase)
                {
                    case 0:
                        nextPhase = -1;
                        this.SetDataRoot(args.Item as global::Consignela.Medicament);
                        if (!removedDataContextHandler)
                        {
                            removedDataContextHandler = true;
                            ((global::Windows.UI.Xaml.Controls.RelativePanel)args.ItemContainer.ContentTemplateRoot).DataContextChanged -= this.DataContextChangedHandler;
                        }
                        this.initialized = true;
                        break;
                }
                this.Update_((global::Consignela.Medicament) args.Item, 1 << (int)args.Phase);
                return nextPhase;
            }

            public void ResetTemplate()
            {
            }

            // IPilulier_Bindings

            public void Initialize()
            {
                if (!this.initialized)
                {
                    this.Update();
                }
            }
            
            public void Update()
            {
                this.Update_(this.dataRoot, NOT_PHASED);
                this.initialized = true;
            }

            public void StopTracking()
            {
            }

            // Pilulier_obj28_Bindings

            public void SetDataRoot(global::Consignela.Medicament newDataRoot)
            {
                this.dataRoot = newDataRoot;
            }

            // Update methods for each path node used in binding steps.
            private void Update_(global::Consignela.Medicament obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_nbr(obj.nbr, phase);
                        this.Update_imageMedicament(obj.imageMedicament, phase);
                        this.Update_nom(obj.nom, phase);
                    }
                }
            }
            private void Update_nbr(global::System.Int32 obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_TextBlock_Text(this.obj29, obj.ToString(), null);
                }
            }
            private void Update_imageMedicament(global::Consignela.Model.MediImages obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_imageMedicament_imageUri(obj.imageUri, phase);
                    }
                }
            }
            private void Update_imageMedicament_imageUri(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_Image_Source(this.obj30, (global::Windows.UI.Xaml.Media.ImageSource) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::Windows.UI.Xaml.Media.ImageSource), obj), null);
                }
            }
            private void Update_nom(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_TextBlock_Text(this.obj31, obj, null);
                }
            }
        }

        private class Pilulier_obj1_Bindings :
            global::Windows.UI.Xaml.Markup.IComponentConnector,
            IPilulier_Bindings
        {
            private global::Consignela.Pilulier dataRoot;
            private bool initialized = false;
            private const int NOT_PHASED = (1 << 31);
            private const int DATA_CHANGED = (1 << 30);

            // Fields for each control that has bindings.
            private global::Windows.UI.Xaml.Controls.GridView obj27;

            public Pilulier_obj1_Bindings()
            {
            }

            // IComponentConnector

            public void Connect(int connectionId, global::System.Object target)
            {
                switch(connectionId)
                {
                    case 27:
                        this.obj27 = (global::Windows.UI.Xaml.Controls.GridView)target;
                        break;
                    default:
                        break;
                }
            }

            // IPilulier_Bindings

            public void Initialize()
            {
                if (!this.initialized)
                {
                    this.Update();
                }
            }
            
            public void Update()
            {
                this.Update_(this.dataRoot, NOT_PHASED);
                this.initialized = true;
            }

            public void StopTracking()
            {
            }

            // Pilulier_obj1_Bindings

            public void SetDataRoot(global::Consignela.Pilulier newDataRoot)
            {
                this.dataRoot = newDataRoot;
            }

            public void Loading(global::Windows.UI.Xaml.FrameworkElement src, object data)
            {
                this.Initialize();
            }

            // Update methods for each path node used in binding steps.
            private void Update_(global::Consignela.Pilulier obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_listMedicament(obj.listMedicament, phase);
                    }
                }
            }
            private void Update_listMedicament(global::System.Collections.Generic.List<global::Consignela.Medicament> obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_ItemsControl_ItemsSource(this.obj27, obj, null);
                }
            }
        }
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 5:
                {
                    this.gridTabulaire = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 6:
                {
                    this.gridViewTabMedicament11 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 139 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament11).DoubleTapped += this.gridViewTabMedicament11_DoubleTapped;
                    #line 139 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament11).Tapped += this.gridViewTabMedicament11_Tapped;
                    #line default
                }
                break;
            case 7:
                {
                    this.gridViewTabMedicament12 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 141 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament12).DoubleTapped += this.gridViewTabMedicament12_DoubleTapped;
                    #line 141 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament12).Tapped += this.gridViewTabMedicament12_Tapped;
                    #line default
                }
                break;
            case 8:
                {
                    this.gridViewTabMedicament13 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 143 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament13).DoubleTapped += this.gridViewTabMedicament13_DoubleTapped;
                    #line 143 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament13).Tapped += this.gridViewTabMedicament13_Tapped;
                    #line default
                }
                break;
            case 9:
                {
                    this.gridViewTabMedicament14 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 145 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament14).DoubleTapped += this.gridViewTabMedicament14_DoubleTapped;
                    #line 145 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament14).Tapped += this.gridViewTabMedicament14_Tapped;
                    #line default
                }
                break;
            case 10:
                {
                    this.gridViewTabMedicament15 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 147 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament15).DoubleTapped += this.gridViewTabMedicament15_DoubleTapped;
                    #line 147 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament15).Tapped += this.gridViewTabMedicament15_Tapped;
                    #line default
                }
                break;
            case 11:
                {
                    this.gridViewTabMedicament16 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 149 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament16).DoubleTapped += this.gridViewTabMedicament16_DoubleTapped;
                    #line 149 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament16).Tapped += this.gridViewTabMedicament16_Tapped;
                    #line default
                }
                break;
            case 12:
                {
                    this.gridViewTabMedicament17 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 151 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament17).DoubleTapped += this.gridViewTabMedicament17_DoubleTapped;
                    #line 151 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament17).Tapped += this.gridViewTabMedicament17_Tapped;
                    #line default
                }
                break;
            case 13:
                {
                    this.gridViewTabMedicament21 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 154 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament21).DoubleTapped += this.gridViewTabMedicament21_DoubleTapped;
                    #line 154 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament21).Tapped += this.gridViewTabMedicament21_Tapped;
                    #line default
                }
                break;
            case 14:
                {
                    this.gridViewTabMedicament22 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 156 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament22).DoubleTapped += this.gridViewTabMedicament22_DoubleTapped;
                    #line 156 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament22).Tapped += this.gridViewTabMedicament22_Tapped;
                    #line default
                }
                break;
            case 15:
                {
                    this.gridViewTabMedicament23 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 158 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament23).DoubleTapped += this.gridViewTabMedicament23_DoubleTapped;
                    #line 158 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament23).Tapped += this.gridViewTabMedicament23_Tapped;
                    #line default
                }
                break;
            case 16:
                {
                    this.gridViewTabMedicament24 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 160 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament24).DoubleTapped += this.gridViewTabMedicament24_DoubleTapped;
                    #line 160 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament24).Tapped += this.gridViewTabMedicament24_Tapped;
                    #line default
                }
                break;
            case 17:
                {
                    this.gridViewTabMedicament25 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 162 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament25).DoubleTapped += this.gridViewTabMedicament25_DoubleTapped;
                    #line 162 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament25).Tapped += this.gridViewTabMedicament25_Tapped;
                    #line default
                }
                break;
            case 18:
                {
                    this.gridViewTabMedicament26 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 164 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament26).DoubleTapped += this.gridViewTabMedicament26_DoubleTapped;
                    #line 164 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament26).Tapped += this.gridViewTabMedicament26_Tapped;
                    #line default
                }
                break;
            case 19:
                {
                    this.gridViewTabMedicament27 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 166 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament27).DoubleTapped += this.gridViewTabMedicament27_DoubleTapped;
                    #line 166 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament27).Tapped += this.gridViewTabMedicament27_Tapped;
                    #line default
                }
                break;
            case 20:
                {
                    this.gridViewTabMedicament31 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 170 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament31).DoubleTapped += this.gridViewTabMedicament31_DoubleTapped;
                    #line 170 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament31).Tapped += this.gridViewTabMedicament31_Tapped;
                    #line default
                }
                break;
            case 21:
                {
                    this.gridViewTabMedicament32 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 172 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament32).DoubleTapped += this.gridViewTabMedicament32_DoubleTapped;
                    #line 172 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament32).Tapped += this.gridViewTabMedicament32_Tapped;
                    #line default
                }
                break;
            case 22:
                {
                    this.gridViewTabMedicament33 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 174 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament33).DoubleTapped += this.gridViewTabMedicament33_DoubleTapped;
                    #line 174 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament33).Tapped += this.gridViewTabMedicament33_Tapped;
                    #line default
                }
                break;
            case 23:
                {
                    this.gridViewTabMedicament34 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 176 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament34).DoubleTapped += this.gridViewTabMedicament34_DoubleTapped;
                    #line 176 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament34).Tapped += this.gridViewTabMedicament34_Tapped;
                    #line default
                }
                break;
            case 24:
                {
                    this.gridViewTabMedicament35 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 178 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament35).DoubleTapped += this.gridViewTabMedicament35_DoubleTapped;
                    #line 178 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament35).Tapped += this.gridViewTabMedicament35_Tapped;
                    #line default
                }
                break;
            case 25:
                {
                    this.gridViewTabMedicament36 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 180 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament36).DoubleTapped += this.gridViewTabMedicament36_DoubleTapped;
                    #line 180 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament36).Tapped += this.gridViewTabMedicament36_Tapped;
                    #line default
                }
                break;
            case 26:
                {
                    this.gridViewTabMedicament37 = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 182 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament37).DoubleTapped += this.gridViewTabMedicament37_DoubleTapped;
                    #line 182 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewTabMedicament37).Tapped += this.gridViewTabMedicament37_Tapped;
                    #line default
                }
                break;
            case 27:
                {
                    this.gridViewBoiteMedicaments = (global::Windows.UI.Xaml.Controls.GridView)(target);
                    #line 63 "..\..\..\Pilulier.xaml"
                    ((global::Windows.UI.Xaml.Controls.GridView)this.gridViewBoiteMedicaments).Tapped += this.gridViewBoiteMedicaments_Tapped;
                    #line default
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            switch(connectionId)
            {
            case 1:
                {
                    global::Windows.UI.Xaml.Controls.Page element1 = (global::Windows.UI.Xaml.Controls.Page)target;
                    Pilulier_obj1_Bindings bindings = new Pilulier_obj1_Bindings();
                    returnValue = bindings;
                    bindings.SetDataRoot(this);
                    this.Bindings = bindings;
                    element1.Loading += bindings.Loading;
                }
                break;
            case 2:
                {
                    global::Windows.UI.Xaml.Controls.StackPanel element2 = (global::Windows.UI.Xaml.Controls.StackPanel)target;
                    Pilulier_obj2_Bindings bindings = new Pilulier_obj2_Bindings();
                    returnValue = bindings;
                    bindings.SetDataRoot((global::Consignela.Medicament) element2.DataContext);
                    element2.DataContextChanged += bindings.DataContextChangedHandler;
                    global::Windows.UI.Xaml.DataTemplate.SetExtensionInstance(element2, bindings);
                }
                break;
            case 28:
                {
                    global::Windows.UI.Xaml.Controls.RelativePanel element28 = (global::Windows.UI.Xaml.Controls.RelativePanel)target;
                    Pilulier_obj28_Bindings bindings = new Pilulier_obj28_Bindings();
                    returnValue = bindings;
                    bindings.SetDataRoot((global::Consignela.Medicament) element28.DataContext);
                    element28.DataContextChanged += bindings.DataContextChangedHandler;
                    global::Windows.UI.Xaml.DataTemplate.SetExtensionInstance(element28, bindings);
                }
                break;
            }
            return returnValue;
        }
    }
}

