﻿#pragma checksum "E:\mohamed\Consignela\code\Consignela\Consignela\Ajout_pres_page.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "C7DD16900BE12BEF8D3F0757846311BC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Consignela
{
    partial class Ajout_pres_page : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        internal class XamlBindingSetters
        {
            public static void Set_Windows_UI_Xaml_Controls_ItemsControl_ItemsSource(global::Windows.UI.Xaml.Controls.ItemsControl obj, global::System.Object value, string targetNullValue)
            {
                if (value == null && targetNullValue != null)
                {
                    value = (global::System.Object) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::System.Object), targetNullValue);
                }
                obj.ItemsSource = value;
            }
            public static void Set_Windows_UI_Xaml_Controls_TextBlock_Text(global::Windows.UI.Xaml.Controls.TextBlock obj, global::System.String value, string targetNullValue)
            {
                if (value == null && targetNullValue != null)
                {
                    value = targetNullValue;
                }
                obj.Text = value ?? global::System.String.Empty;
            }
            public static void Set_Windows_UI_Xaml_Controls_Image_Source(global::Windows.UI.Xaml.Controls.Image obj, global::Windows.UI.Xaml.Media.ImageSource value, string targetNullValue)
            {
                if (value == null && targetNullValue != null)
                {
                    value = (global::Windows.UI.Xaml.Media.ImageSource) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::Windows.UI.Xaml.Media.ImageSource), targetNullValue);
                }
                obj.Source = value;
            }
        };

        private class Ajout_pres_page_obj7_Bindings :
            global::Windows.UI.Xaml.IDataTemplateExtension,
            global::Windows.UI.Xaml.Markup.IComponentConnector,
            IAjout_pres_page_Bindings
        {
            private global::Consignela.Medicament dataRoot;
            private bool initialized = false;
            private const int NOT_PHASED = (1 << 31);
            private const int DATA_CHANGED = (1 << 30);
            private bool removedDataContextHandler = false;

            // Fields for each control that has bindings.
            private global::Windows.UI.Xaml.Controls.TextBlock obj8;
            private global::Windows.UI.Xaml.Controls.TextBlock obj9;
            private global::Windows.UI.Xaml.Controls.TextBlock obj10;
            private global::Windows.UI.Xaml.Controls.Image obj11;

            public Ajout_pres_page_obj7_Bindings()
            {
            }

            // IComponentConnector

            public void Connect(int connectionId, global::System.Object target)
            {
                switch(connectionId)
                {
                    case 8:
                        this.obj8 = (global::Windows.UI.Xaml.Controls.TextBlock)target;
                        break;
                    case 9:
                        this.obj9 = (global::Windows.UI.Xaml.Controls.TextBlock)target;
                        break;
                    case 10:
                        this.obj10 = (global::Windows.UI.Xaml.Controls.TextBlock)target;
                        break;
                    case 11:
                        this.obj11 = (global::Windows.UI.Xaml.Controls.Image)target;
                        break;
                    default:
                        break;
                }
            }

            public void DataContextChangedHandler(global::Windows.UI.Xaml.FrameworkElement sender, global::Windows.UI.Xaml.DataContextChangedEventArgs args)
            {
                 global::Consignela.Medicament data = args.NewValue as global::Consignela.Medicament;
                 if (args.NewValue != null && data == null)
                 {
                    throw new global::System.ArgumentException("Incorrect type passed into template. Based on the x:DataType global::Consignela.Medicament was expected.");
                 }
                 this.SetDataRoot(data);
                 this.Update();
            }

            // IDataTemplateExtension

            public bool ProcessBinding(uint phase)
            {
                throw new global::System.NotImplementedException();
            }

            public int ProcessBindings(global::Windows.UI.Xaml.Controls.ContainerContentChangingEventArgs args)
            {
                int nextPhase = -1;
                switch(args.Phase)
                {
                    case 0:
                        nextPhase = -1;
                        this.SetDataRoot(args.Item as global::Consignela.Medicament);
                        if (!removedDataContextHandler)
                        {
                            removedDataContextHandler = true;
                            ((global::Windows.UI.Xaml.Controls.StackPanel)args.ItemContainer.ContentTemplateRoot).DataContextChanged -= this.DataContextChangedHandler;
                        }
                        this.initialized = true;
                        break;
                }
                this.Update_((global::Consignela.Medicament) args.Item, 1 << (int)args.Phase);
                return nextPhase;
            }

            public void ResetTemplate()
            {
            }

            // IAjout_pres_page_Bindings

            public void Initialize()
            {
                if (!this.initialized)
                {
                    this.Update();
                }
            }
            
            public void Update()
            {
                this.Update_(this.dataRoot, NOT_PHASED);
                this.initialized = true;
            }

            public void StopTracking()
            {
            }

            // Ajout_pres_page_obj7_Bindings

            public void SetDataRoot(global::Consignela.Medicament newDataRoot)
            {
                this.dataRoot = newDataRoot;
            }

            // Update methods for each path node used in binding steps.
            private void Update_(global::Consignela.Medicament obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_descrReg(obj.descrReg, phase);
                        this.Update_descrIrreg(obj.descrIrreg, phase);
                        this.Update_nom(obj.nom, phase);
                        this.Update_imageMedicament(obj.imageMedicament, phase);
                    }
                }
            }
            private void Update_descrReg(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_TextBlock_Text(this.obj8, obj, null);
                }
            }
            private void Update_descrIrreg(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_TextBlock_Text(this.obj9, obj, null);
                }
            }
            private void Update_nom(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_TextBlock_Text(this.obj10, obj, null);
                }
            }
            private void Update_imageMedicament(global::Consignela.Model.MediImages obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_imageMedicament_imageUri(obj.imageUri, phase);
                    }
                }
            }
            private void Update_imageMedicament_imageUri(global::System.String obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_Image_Source(this.obj11, (global::Windows.UI.Xaml.Media.ImageSource) global::Windows.UI.Xaml.Markup.XamlBindingHelper.ConvertValue(typeof(global::Windows.UI.Xaml.Media.ImageSource), obj), null);
                }
            }
        }

        private class Ajout_pres_page_obj1_Bindings :
            global::Windows.UI.Xaml.Markup.IComponentConnector,
            IAjout_pres_page_Bindings
        {
            private global::Consignela.Ajout_pres_page dataRoot;
            private bool initialized = false;
            private const int NOT_PHASED = (1 << 31);
            private const int DATA_CHANGED = (1 << 30);

            // Fields for each control that has bindings.
            private global::Windows.UI.Xaml.Controls.GridView obj6;
            private global::Windows.UI.Xaml.Controls.ListView obj14;

            public Ajout_pres_page_obj1_Bindings()
            {
            }

            // IComponentConnector

            public void Connect(int connectionId, global::System.Object target)
            {
                switch(connectionId)
                {
                    case 6:
                        this.obj6 = (global::Windows.UI.Xaml.Controls.GridView)target;
                        break;
                    case 14:
                        this.obj14 = (global::Windows.UI.Xaml.Controls.ListView)target;
                        break;
                    default:
                        break;
                }
            }

            // IAjout_pres_page_Bindings

            public void Initialize()
            {
                if (!this.initialized)
                {
                    this.Update();
                }
            }
            
            public void Update()
            {
                this.Update_(this.dataRoot, NOT_PHASED);
                this.initialized = true;
            }

            public void StopTracking()
            {
            }

            // Ajout_pres_page_obj1_Bindings

            public void SetDataRoot(global::Consignela.Ajout_pres_page newDataRoot)
            {
                this.dataRoot = newDataRoot;
            }

            public void Loading(global::Windows.UI.Xaml.FrameworkElement src, object data)
            {
                this.Initialize();
            }

            // Update methods for each path node used in binding steps.
            private void Update_(global::Consignela.Ajout_pres_page obj, int phase)
            {
                if (obj != null)
                {
                    if ((phase & (NOT_PHASED | (1 << 0))) != 0)
                    {
                        this.Update_Medicaments(obj.Medicaments, phase);
                        this.Update_AjoutIrrigulier(obj.AjoutIrrigulier, phase);
                    }
                }
            }
            private void Update_Medicaments(global::System.Collections.Generic.List<global::Consignela.Medicament> obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_ItemsControl_ItemsSource(this.obj6, obj, null);
                }
            }
            private void Update_AjoutIrrigulier(global::System.Collections.Generic.List<global::System.String> obj, int phase)
            {
                if((phase & ((1 << 0) | NOT_PHASED )) != 0)
                {
                    XamlBindingSetters.Set_Windows_UI_Xaml_Controls_ItemsControl_ItemsSource(this.obj14, obj, null);
                }
            }
        }
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 2:
                {
                    this.fieldMissing = (global::Windows.UI.Xaml.Controls.Border)(target);
                }
                break;
            case 3:
                {
                    this.nbrMediError = (global::Windows.UI.Xaml.Controls.Border)(target);
                }
                break;
            case 4:
                {
                    this.btn_savePresc = (global::Windows.UI.Xaml.Controls.AppBarButton)(target);
                    #line 32 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.AppBarButton)this.btn_savePresc).Click += this.btn_savePresc_Click;
                    #line default
                }
                break;
            case 5:
                {
                    this.tb_date_presc = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 6:
                {
                    this.gridViewPresc = (global::Windows.UI.Xaml.Controls.GridView)(target);
                }
                break;
            case 12:
                {
                    this.nomPrescAffichage = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 13:
                {
                    this.btn_saveMedi = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 149 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.btn_saveMedi).Click += this.btn_saveMedi_Click;
                    #line default
                }
                break;
            case 14:
                {
                    this.listAjoutIrrigulier = (global::Windows.UI.Xaml.Controls.ListView)(target);
                }
                break;
            case 15:
                {
                    global::Windows.UI.Xaml.Controls.Border element15 = (global::Windows.UI.Xaml.Controls.Border)(target);
                    #line 140 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Border)element15).Tapped += this.delete_Click;
                    #line default
                }
                break;
            case 16:
                {
                    this.togb_ajoutIrr = (global::Windows.UI.Xaml.Controls.Primitives.ToggleButton)(target);
                    #line 117 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Primitives.ToggleButton)this.togb_ajoutIrr).Checked += this.togb_ajoutIrr_Checked;
                    #line default
                }
                break;
            case 17:
                {
                    this.nbr_medi_irreg = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                    #line 118 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.TextBox)this.nbr_medi_irreg).TextChanged += this.nbr_medi_irreg_TextChanged;
                    #line default
                }
                break;
            case 18:
                {
                    this.cb_momentIr = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                }
                break;
            case 19:
                {
                    this.cb_jourIr = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                }
                break;
            case 20:
                {
                    this.confirmer = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 133 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.confirmer).Click += this.confirmer_Click;
                    #line default
                }
                break;
            case 21:
                {
                    this.descritionRegulier = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 22:
                {
                    this.togb_ajoutReg = (global::Windows.UI.Xaml.Controls.Primitives.ToggleButton)(target);
                    #line 82 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Primitives.ToggleButton)this.togb_ajoutReg).Checked += this.togb_ajoutReg_Checked;
                    #line default
                }
                break;
            case 23:
                {
                    this.nbr_medi_reg = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                    #line 84 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.TextBox)this.nbr_medi_reg).TextChanged += this.nbr_medi_reg_TextChanged;
                    #line default
                }
                break;
            case 24:
                {
                    this.cb_momentR = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                }
                break;
            case 25:
                {
                    this.cb_jourRD = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                    #line 90 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.ComboBox)this.cb_jourRD).SelectionChanged += this.cb_jourRD_SelectionChanged;
                    #line default
                }
                break;
            case 26:
                {
                    this.cb_jourRF = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                    #line 99 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.ComboBox)this.cb_jourRF).SelectionChanged += this.cb_jourRF_SelectionChanged;
                    #line default
                }
                break;
            case 27:
                {
                    this.confirmerReg = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 108 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.confirmerReg).Click += this.confirmerReg_Click;
                    #line default
                }
                break;
            case 28:
                {
                    this.cb_medicamentImg = (global::Windows.UI.Xaml.Controls.ComboBox)(target);
                }
                break;
            case 29:
                {
                    this.nom_medi = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 30:
                {
                    this.nbr_medi = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                    #line 61 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.TextBox)this.nbr_medi).KeyDown += this.nbr_medi_KeyDown;
                    #line 61 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.TextBox)this.nbr_medi).TextChanged += this.nbr_medi_TextChanged;
                    #line default
                }
                break;
            case 31:
                {
                    this.presc_date = (global::Windows.UI.Xaml.Controls.DatePicker)(target);
                }
                break;
            case 32:
                {
                    this.btn_saveDate = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 54 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)this.btn_saveDate).Click += this.btn_saveDate_Click;
                    #line default
                }
                break;
            case 33:
                {
                    this.presc_name = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                    #line 48 "..\..\..\Ajout_pres_page.xaml"
                    ((global::Windows.UI.Xaml.Controls.TextBox)this.presc_name).TextChanged += this.presc_name_TextChanged;
                    #line default
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            switch(connectionId)
            {
            case 1:
                {
                    global::Windows.UI.Xaml.Controls.Page element1 = (global::Windows.UI.Xaml.Controls.Page)target;
                    Ajout_pres_page_obj1_Bindings bindings = new Ajout_pres_page_obj1_Bindings();
                    returnValue = bindings;
                    bindings.SetDataRoot(this);
                    this.Bindings = bindings;
                    element1.Loading += bindings.Loading;
                }
                break;
            case 7:
                {
                    global::Windows.UI.Xaml.Controls.StackPanel element7 = (global::Windows.UI.Xaml.Controls.StackPanel)target;
                    Ajout_pres_page_obj7_Bindings bindings = new Ajout_pres_page_obj7_Bindings();
                    returnValue = bindings;
                    bindings.SetDataRoot((global::Consignela.Medicament) element7.DataContext);
                    element7.DataContextChanged += bindings.DataContextChangedHandler;
                    global::Windows.UI.Xaml.DataTemplate.SetExtensionInstance(element7, bindings);
                }
                break;
            }
            return returnValue;
        }
    }
}

