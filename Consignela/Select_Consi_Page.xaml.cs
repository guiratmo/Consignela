﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Consignela.Model;
using Newtonsoft.Json;
using Windows.UI.Popups;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using System.Net;
using Windows.UI.Xaml.Media;
using Windows.UI;



// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Select_Consi_Page : Page
    {
        internal String str_Consigne_json;
        internal List<ListConsignes> listListConsignes;
        internal List<Consigne> listConsigneTmp = new List<Consigne>();
        
        internal String nomConsigne;
        internal bool consChoisie = false;
        internal String msgFeedback;
        internal ListConsignes listConstmp;
        private ListConsignes listConstmp2 = new ListConsignes();
        int indexConstmp;

        private ObservableCollection<Consigne> ListLignesConsignes = new ObservableCollection<Consigne>();
        private Collection<Consigne> listConsignetmpCollection = new Collection<Consigne>();

        private ObservableCollection<Consigne> ListLignesConsignesModif = new ObservableCollection<Consigne>();
        private Collection<Consigne> ListLignesConsignesModifCollec = new Collection<Consigne>();

        int a = 0, b = 0;

        private int index,i,indexDebut, indexFin;
        internal Consigne consigneTmp = new Consigne();
        internal static String imgPath = null;
        internal static String imgNom = null;
        internal String TxtResponse;
        public static StorageFile sampleFile;
        
        List<string> nameList = new List<string>();
        List<string> elementList = new List<string>();
        List<CheckBox> checkBoxListSharingSpace = new List<CheckBox>();
        public static List<CheckBox> checkBoxListIndex = new List<CheckBox>();

        static List<string> idSharingSpace = new List<string>();
        byte[] bytes;
        public StorageFile downloadedFile;
        public int indeximages = 0;
        public bool searchName =false, searchDate = false;



        public Select_Consi_Page()
        {         
            this.InitializeComponent();
          
                    
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           
                chargerDonnees();
            
            //beginPopupOpen();
            initialiserAffichage();                      
        }
        public string getNomFichier()
        {
            return sampleFile.Name ;
        }

        public async void chargerDonnees()
        {
           
            
            if (App.authentification)
            {
                sampleFile = await App.storageFolder.GetFileAsync(App.logIn + "-consignes.json");
                str_Consigne_json = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/"+App.logIn+"-consignes.json");
                listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);
                await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
            }
            else
            {
                sampleFile = await App.storageFolder.GetFileAsync("ConsignesData.json");
                str_Consigne_json = await FileIO.ReadTextAsync(sampleFile);
                listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);
            }
           

            //str_Consigne_json = File.ReadAllText(@"ConsignesData.json");
            //listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);

            //foreach (ListConsignes listConsigne in listListConsignes)
            //{
            //    foreach (Consigne consigne in listConsigne.consignes)
            //    {
            //        if (!String.IsNullOrEmpty(consigne.imgNom))
            //        {
            //            DownloadImage(consigne.imgNom);
            //        }
            //    }
            //    indeximages++;
            //}
            //if (indeximages == listListConsignes.Count-1)
            //{
                foreach (ListConsignes listConsigne in listListConsignes)
                {
                    foreach (Consigne consigne in listConsigne.consignes)
                    {
                        if (!String.IsNullOrEmpty(consigne.imgNom))
                        {
                            //DownloadImage(consigne.imgNom);
                            StorageFile imageFile = await App.imageFolder.GetFileAsync(consigne.imgNom);
                            BitmapImage img = new BitmapImage();
                            img = await LoadImage(imageFile);
                            consigne.imgBitmap = img;
                        }
                        else
                        {
                            BitmapImage img = new BitmapImage(new Uri(" ms-appx://Consignela/Assets/landscape.png", UriKind.RelativeOrAbsolute));
                            //img = await LoadImage(imageFile);
                            consigne.imgBitmap = img;
                        }

                    }
                }

                if (listListConsignes.Count != 0)
                {
                    i = listListConsignes[listListConsignes.Count - 1].id + 1;
                }
                else
                {
                    i = 0;
                }
                rechargerListViewConsigne();


            

            //else
            //{
            //    MessageDialog fileMissing = new MessageDialog("Le fichier des listes de consignes est introuvable", "Fichier introuvable");
            //    await fileMissing.ShowAsync();
            //}


            //listListConsignes.Reverse();
        }
        private void initialiserAffichage()
        {
            this.modify.IsEnabled = false;
            //this.delete.IsEnabled = false;
            tb_nomConsigne.Text = "Nom de la liste de Consigne";
            gridViewConsignes.ItemsSource = null;
            afficheListeConsigne.Visibility = Visibility.Collapsed;

        }
        //private void beginPopupOpen()
        //{
        //    if (!beginPopup.IsOpen) { beginPopup.IsOpen = true; }
        //    contentSelection.Opacity = 0.1;
        //    barSelection.IsEnabled = false;
        //    gridViewConsi.IsEnabled = false;
           
        //    searchConsigne.IsEnabled = false;
        //}

        public static string getJsonFromUrl(string url)
        {
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            var response = client.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                // by calling .Result you are performing a synchronous call
                var responseContent = response.Content;

                // by calling .Result you are synchronously reading the result
                string responseString = responseContent.ReadAsStringAsync().Result;

                return responseString;
            }
            else
            {
                return "error";
            }

        }
        private static async Task<BitmapImage> LoadImage(StorageFile file)
        {
            BitmapImage bitmapImage = new BitmapImage();
            FileRandomAccessStream stream = (FileRandomAccessStream)await file.OpenAsync(FileAccessMode.Read);

            bitmapImage.SetSource(stream);

            return bitmapImage;

        }
        

        public void rechargerListViewConsigne()
        {
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;
        }
        public void rechargerListViewLignesConsigne()
        {
            gridViewConsignes.ItemsSource = null;
            gridViewConsignes.ItemsSource = listConsigneTmp;
        }


        private void gridViewConsi_Tapped(object sender, TappedRoutedEventArgs e)
        {
            b = 3;
            System.Diagnostics.Debug.WriteLine(a);

            if (listListConsignes.Count != 0)
            {
                modify.IsEnabled = true;
                //delete.IsEnabled = true;
                listConstmp = (ListConsignes)gridViewConsi.SelectedItem;
                //ListConsignes listConstmp2 = new ListConsignes();
                listConstmp2 = listConstmp;
                //listConstmp2.nom = listConstmp.nom;
                //listConstmp2.id = listConstmp.id;
                //listConstmp2.date = listConstmp.date;
                //listConstmp2.dateLettre = listConstmp.dateLettre;
                //listConstmp2.consignes = listConstmp.consignes;


                indexConstmp = listListConsignes.IndexOf(listConstmp);

                //System.Diagnostics.Debug.Write(listListConsignes);
                listConsigneTmp = listListConsignes[listConstmp.id].consignes;
                ListLignesConsignesModif = new ObservableCollection<Consigne>(listConsigneTmp);
                //System.Diagnostics.Debug.Write(ListLignesConsignesModif);
                nomConsigne = listListConsignes[listConstmp.id].nom;
                tb_nomConsigne.Text = nomConsigne;

                afficheListeConsigne.Visibility = Visibility.Visible;
                gridViewConsignes.ItemsSource = null;
                gridViewConsignes.ItemsSource = listConsigneTmp;

                
                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
               
                consChoisie = true;
            }

        }

      
       
             
        private void exitPopup_Click(object sender, RoutedEventArgs e)
        {
            
        }

        public void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            if (ModifierPopup.IsOpen) { ModifierPopup.IsOpen = false; }
            contentSelection.Opacity = 1;
            barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;
            
            searchConsigne.IsEnabled = true;
        }

        private void OnLayoutSearchPopup(object sender, object e)
        {
            if (shearchPopupChild.ActualWidth == 0 && shearchPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.shearchPopup.HorizontalOffset;
            double ActualVerticalOffset = this.shearchPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - shearchPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - shearchPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.shearchPopup.HorizontalOffset = NewHorizontalOffset;
                this.shearchPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void OnLayoutUpdatedIndex(object sender, object e)
        {
            if (indexPopupChild.ActualWidth == 0 && indexPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.indexPopup.HorizontalOffset;
            double ActualVerticalOffset = this.indexPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - indexPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - indexPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.indexPopup.HorizontalOffset = NewHorizontalOffset;
                this.indexPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void OnLayoutUpdatedSharing(object sender, object e)
        {
            if (sharingSpacePopupChild.ActualWidth == 0 && sharingSpacePopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.sharingSpacePopup.HorizontalOffset;
            double ActualVerticalOffset = this.sharingSpacePopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - sharingSpacePopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - sharingSpacePopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.sharingSpacePopup.HorizontalOffset = NewHorizontalOffset;
                this.sharingSpacePopup.VerticalOffset = NewVerticalOffset;
            }
        }
        //private void OnLayoutUpdatedSave(object sender, object e)
        //{
        //    if (savePopupChild.ActualWidth == 0 && savePopupChild.ActualHeight == 0)
        //    {
        //        return;
        //    }

        //    double ActualHorizontalOffset = this.savePopup.HorizontalOffset;
        //    double ActualVerticalOffset = this.savePopup.VerticalOffset;

        //    double NewHorizontalOffset = (Window.Current.Bounds.Width - savePopupChild.ActualWidth) / 2;
        //    double NewVerticalOffset = (Window.Current.Bounds.Height - savePopupChild.ActualHeight) / 2;

        //    if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
        //    {
        //        this.savePopup.HorizontalOffset = NewHorizontalOffset;
        //        this.savePopup.VerticalOffset = NewVerticalOffset;
        //    }
        //}

        private void OnLayoutUpdatedAuthen(object sender, object e)
        {
            if (authenPopupChild.ActualWidth == 0 && authenPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        //private void OnLayoutUpdatedBegin(object sender, object e)
        //{
        //    if (beginPopupChild.ActualWidth == 0 && beginPopupChild.ActualHeight == 0)
        //    {
        //        return;
        //    }

        //    double ActualHorizontalOffset = this.beginPopup.HorizontalOffset;
        //    double ActualVerticalOffset = this.beginPopup.VerticalOffset;

        //    double NewHorizontalOffset = (Window.Current.Bounds.Width - beginPopupChild.ActualWidth) / 2;
        //    double NewVerticalOffset = (Window.Current.Bounds.Height - beginPopupChild.ActualHeight) / 2;

        //    if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
        //    {
        //        this.beginPopup.HorizontalOffset = NewHorizontalOffset;
        //        this.beginPopup.VerticalOffset = NewVerticalOffset;
        //    }
        //}
        private void OnLayoutUpdated(object sender, object e)
        {
            if (popupChild.ActualWidth == 0 && popupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.ModifierPopup.HorizontalOffset;
            double ActualVerticalOffset = this.ModifierPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - popupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - popupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.ModifierPopup.HorizontalOffset = NewHorizontalOffset;
                this.ModifierPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void OnLayoutUpdatedAjout(object sender, object e)
        {
            if (ajoutPopupChild.ActualWidth == 0 && ajoutPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.ajoutPopup.HorizontalOffset;
            double ActualVerticalOffset = this.ajoutPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - ajoutPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - ajoutPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.ajoutPopup.HorizontalOffset = NewHorizontalOffset;
                this.ajoutPopup.VerticalOffset = NewVerticalOffset;
            }
        }

       

        private void tb_decrModif_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void ajoutConsigne_Click(object sender, RoutedEventArgs e)
        {
            if (!ajoutPopup.IsOpen) { ajoutPopup.IsOpen = true; }
            contentSelection.Opacity = 0.5;
            barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;
            modify.IsEnabled = false;
            searchConsigne.IsEnabled = false;

        }

        private void CloseAjoutPopupClicked(object sender, RoutedEventArgs e)
        {
            if (ajoutPopup.IsOpen) { ajoutPopup.IsOpen = false; }
            contentSelection.Opacity = 1;
            barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchConsigne.IsEnabled = true;

        }
        private void chargerListConsignetmp()
        {

            if (ListLignesConsignes != null)
            {
                listConsignetmpCollection = ListLignesConsignes;
            }
            listViewListConsigne.ItemsSource = null;
            listViewListConsigne.ItemsSource = listConsignetmpCollection;
            
        }
        private void btn_ajoutLigneConsigne_Click(object sender, RoutedEventArgs e)
        {
            chargerListConsignetmp();
            System.Diagnostics.Debug.Write(listConsignetmpCollection);
            if (listConsignetmpCollection.Count() != 0)
            {
                index = listConsignetmpCollection[listConsignetmpCollection.Count - 1].id + 1;
            }
            else
            {
                index = 0;
            }

            Consigne ligneConsigne = new Consigne { id = index, description = "", img_path = "Assets/icons/addimg.png" };
            ListLignesConsignes.Add(ligneConsigne);
            index++;
        }

        private async void btn_Ajout_img_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;

            System.Diagnostics.Debug.Write(listViewListConsigne.SelectedItem);
            System.Diagnostics.Debug.Write(consigneTmp);

            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();

            if (file != null)
            {

                imgNom = file.Name;
                if (await App.imageFolder.TryGetItemAsync(imgNom) != null)
                    System.Diagnostics.Debug.WriteLine(imgNom + " exists.");
                else  // Return value of TryGetItemAsync is null.
                    await file.CopyAsync(App.imageFolder);

                //imgPath = "Assets/" + imgNom;
                var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var imageBtm = new BitmapImage();

                imageBtm.SetSource(stream);
                ListLignesConsignes[consigneTmp.id].img_path = file.Path;

                ListLignesConsignes[consigneTmp.id].imgBitmap = imageBtm;             
                ListLignesConsignes[consigneTmp.id].imgNom = imgNom;

                chargerListConsignetmp();

            }
            else
            {
                //this.tb_img_name.Text = "aucune image";
            }
        }

        private void deleteAjout_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            if (listConsignetmpCollection.Count > 1)
            {

                listConsignetmpCollection.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConsignetmpCollection)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;

            }
        }

        //private void Searchbtn_Click(object sender, RoutedEventArgs e)
        //{
        //    if (beginPopup.IsOpen) { beginPopup.IsOpen = false; }
        //    contentSelection.Opacity = 1;          
        //    barSelection.IsEnabled = true;
        //    gridViewConsi.IsEnabled = true;
            
        //    searchConsigne.IsEnabled = true;
        //}

        private void ajoutListeConsignebtn_Click(object sender, RoutedEventArgs e)
        {
            if (!ajoutPopup.IsOpen) { ajoutPopup.IsOpen = true; }
            contentSelection.Opacity = 0.1;
            barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;

            searchConsigne.IsEnabled = false;
        }
        private void chargerListConsignetmpModif()
        {

            if (ListLignesConsignesModif != null)
            {
                ListLignesConsignesModifCollec = ListLignesConsignesModif;
            }
            ListViewConsignesModif.ItemsSource = null;
            ListViewConsignesModif.ItemsSource = ListLignesConsignesModifCollec;

        }
        private void modify_Click(object sender, RoutedEventArgs e)
        {
            popupConsigneNom.Text = listListConsignes[listConstmp.id].nom;
            if (!ModifierPopup.IsOpen) { ModifierPopup.IsOpen = true; }
            ModifierPopup.Height = Window.Current.Bounds.Height;
            contentSelection.Opacity = 0.5;
            barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;
            searchConsigne.IsEnabled = false;
        }
        private async void enregistrerSous_Click(object sender, RoutedEventArgs e)
        {
            listConstmp.nom = popupConsigneNom.Text;
            tb_nomConsigne.Text = popupConsigneNom.Text;
            //listConstmp.consignes[0].description = ListViewConsignes.SelectedItem.ToString();
            listConstmp.consignes = ListViewConsignesModif.Items.OfType<Consigne>().ToList();
            
            listListConsignes.Add(listConstmp);
            listListConsignes[listListConsignes.Count-1] .id = listListConsignes.Count-1;

            listListConsignes[indexConstmp] = listConstmp2;
            rechargerListViewConsigne();
            rechargerListViewLignesConsigne();

            ClosePopupClicked(sender, e);
            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }
        private async void modifConsSave_Click(object sender, RoutedEventArgs e)
        {
            listConstmp = listConstmp2;
            listConstmp.nom = popupConsigneNom.Text;
            tb_nomConsigne.Text = popupConsigneNom.Text;
            //listConstmp.consignes[0].description = ListViewConsignes.SelectedItem.ToString();
            listConstmp.consignes = ListViewConsignesModif.Items.OfType<Consigne>().ToList();

            listListConsignes[listConstmp.id] = listConstmp;

            rechargerListViewConsigne();
            rechargerListViewLignesConsigne();

            ClosePopupClicked(sender, e);
            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }
        private void deleteModif_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            if (listConstmp2.consignes.Count > 1)
            {

                listConstmp2.consignes.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConstmp2.consignes)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                ListViewConsignesModif.ItemsSource = null;
                ListViewConsignesModif.ItemsSource = listConstmp2.consignes;

            }
        }

        private async void imgModif_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;

            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                imgNom = file.Name;
                if (await App.imageFolder.TryGetItemAsync(imgNom) != null)
                    System.Diagnostics.Debug.WriteLine(imgNom + " exists.");
                else  // Return value of TryGetItemAsync is null.
                    await file.CopyAsync(App.imageFolder);

                var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var imageBtm = new BitmapImage();

                imageBtm.SetSource(stream);

                
                ListLignesConsignesModif[consigneTmp.id].imgBitmap = imageBtm;
                ListLignesConsignesModif[consigneTmp.id].img_path = file.Path;
                ListLignesConsignesModif[consigneTmp.id].imgNom = imgNom;

                chargerListConsignetmpModif();

            }
            else
            {
                //this.tb_img_name.Text = "aucune image";
            }
        }
        private async void delete_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog deleteConsigne = new MessageDialog("Attention voulez-vous vraiment supprimer cette consigne ?", "Suppression de la consigne");
            deleteConsigne.Commands.Add(new UICommand("Supprimer", supprimerConsigne, 1));
            deleteConsigne.Commands.Add(new UICommand("Annuler", CancelCommand, 2));
            await deleteConsigne.ShowAsync();
            
        }
        public async void supprimerConsigne(IUICommand command)
        {
            listListConsignes.Remove(listConstmp);
            int index = 0;
            foreach (ListConsignes listConsigneD in listListConsignes)
            {
                listConsigneD.id = index;
                index++;
            }
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;
            initialiserAffichage();

            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }
        public void CancelCommand(IUICommand command)
        {
            
        }
        private async void selection_Click(object sender, RoutedEventArgs e)
        {
            if (consChoisie)
            {
                if (App.consignesSelect == null)
                {
                    MainMenu.progressBarValue += 50;
                }

                App.consignesSelect = listListConsignes[listConstmp.id];
                msgFeedback = "la consigne " + nomConsigne + " a bien été  choisie";
            }
            else
            {
                msgFeedback = "La Consigne est non choisise";
            }
            
            
            
            MessageDialog selectConsigne = new MessageDialog(msgFeedback, "Selection de la consigne");
            await selectConsigne.ShowAsync();
        }

        private async void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
            enregistrementServeur();
                      
        }

        //private void quitSavePopup_Click(object sender, RoutedEventArgs e)
        //{
        //    if (savePopup.IsOpen) {
        //        savePopup.IsOpen = false;
        //        contentSelection.Opacity = 1;
        //        barSelection.IsEnabled = true;
        //        gridViewConsi.IsEnabled = true;
        //        searchConsigne.IsEnabled = true;
        //    }
            
        //}

        private void quitAuthenPopup_Click(object sender, RoutedEventArgs e)
        {
            if (authenPopup.IsOpen) {authenPopup.IsOpen = false;}
        }
        //public async void fileToByte()
        //{
        //    //StorageFile testfile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("E:/consi/brahim-Prescription.json"));

        //    var stream = await sampleFile.OpenStreamForReadAsync();
        //    bytes = new byte[(int)stream.Length];
        //    stream.Read(bytes, 0, (int)stream.Length);

        //}
        private async void enregistrementServeur()

        {
           
            if (!authenPopup.IsOpen && App.authentification == false)
            {
                authenPopup.IsOpen = true;

            }
            else
            {
                if (!sharingSpacePopup.IsOpen)
                {
                    sharingSpacePopup.IsOpen = true;
                    contentSelection.Opacity = 0.5;
                    barSelection.IsEnabled = false;
                    gridViewConsi.IsEnabled = false;
                    searchConsigne.IsEnabled = false;
                }
                var stream = await sampleFile.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFile.Path);

               // SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();
                for (int i = 0; i < a; i++)
                {

                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }
            }
        }
        //private async void saveServeur_Click(object sender, RoutedEventArgs e)
        //{

          
        //}

        private void priveSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!authenPopup.IsOpen && App.authentification == false) { authenPopup.IsOpen = true; }
            
        }

        private void partageSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!authenPopup.IsOpen && App.authentification == false) { authenPopup.IsOpen = true; }
            
        }

        private async void connecter_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login.Text + "\"" + ",\"pass\":" + "\"" + pass.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login.Text;
                App.password = pass.Password;
                App.authentification = true;
                
                if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
                //fileToByte();
                var stream = await sampleFile.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFile.Path);

                SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();

                for (int i = 0; i < a; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }



            }
            else
            {
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
        }
        public async Task Post(string website, string args)
        {

            //init client
            Windows.Web.Http.HttpClient hc = new Windows.Web.Http.HttpClient();
            //send post request
            Windows.Web.Http.HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            TxtResponse = await hrm.Content.ReadAsStringAsync();
            App.txtresponseLogin = TxtResponse;
            //System.Diagnostics.Debug.Write(TxtResponse);
            hc.Dispose();
        }

        private void quitSharingPopup_Click(object sender, RoutedEventArgs e)
        {
            if (sharingSpacePopup.IsOpen)
            {
                sharingSpacePopup.IsOpen = false;              
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void showShSpInfo_Click(object sender, RoutedEventArgs e)
        {
            showSharingSpaceInfo();
            if (idSharingSpace.Count != 0)
            {

                Index.getIndex();
                List<string> nom_cpt = Index.getnom_cpt();
                checkBoxListIndex.Clear();
                IndexStackPanel1.Children.Clear();

                for (int i = 0; i < nom_cpt.Count; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nom_cpt[i];
                    newCheckBox.Name = "checkbox" + i;

                    checkBoxListIndex.Add(newCheckBox);
                    IndexStackPanel1.Children.Add(newCheckBox);

                }
                if (!indexPopup.IsOpen) { indexPopup.IsOpen = true; }
            }
            else
            {
                MessageDialog SharingSpaceNotSelected = new MessageDialog("Veuillez séléctionner l'espace de partage");
                await SharingSpaceNotSelected.ShowAsync();
            }

        }
        public void showSharingSpaceInfo()
        {
            idSharingSpace.Clear();
            // foreach (CheckBox checkbox in checkBoxListSharingSpace)
            for (int i = 0; i < checkBoxListSharingSpace.Count; i++)
            {
                if (checkBoxListSharingSpace[i].IsChecked == true)
                {
                    string[] split = elementList[i].Split('#');
                    idSharingSpace.Add(split[1]);                   
                }
            }
        }
        public static List<CheckBox> getcheckBoxListIndex ()
        {
            return checkBoxListIndex;
        }
        public static List<string> getIdSharingSpace()
        {
            return idSharingSpace;
        }

        private void quitIndexPopup_Click(object sender, RoutedEventArgs e)
        {
            if (indexPopup.IsOpen)
            {
                indexPopup.IsOpen = false;
            }

        }

        private async void indexSave_Click(object sender, RoutedEventArgs e)
        {
            Index.showInfo(checkBoxListIndex,sampleFile, idSharingSpace);
            MessageDialog saveServerFile = new MessageDialog("les listes des consignes ont bien été enregistrées ", "Enregistrement Listes des consignes dans le serveur");
            await saveServerFile.ShowAsync();
        }

        private void up_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmpCollection.IndexOf(consigneTmp);
            if (indexConsigne > 0)
            {
                listConsignetmpCollection[indexConsigne].id--;
                listConsignetmpCollection[indexConsigne - 1].id++;

                listConsignetmpCollection[indexConsigne] = listConsignetmpCollection[indexConsigne - 1];
                listConsignetmpCollection[indexConsigne - 1] = consigneTmp;
    
                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;
            }
        }

        private void down_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmpCollection.IndexOf(consigneTmp);

            if (indexConsigne < listConsignetmpCollection.Count() - 1)
            {
                listConsignetmpCollection[indexConsigne].id++;
                listConsignetmpCollection[indexConsigne + 1].id--;

                listConsignetmpCollection[indexConsigne] = listConsignetmpCollection[indexConsigne + 1];
                listConsignetmpCollection[indexConsigne + 1] = consigneTmp;



                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;
            }
        }

        private void up_Tapped_modif(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            int indexConsigne = ListLignesConsignesModif.IndexOf(consigneTmp);
            if (indexConsigne > 0)
            {
                ListLignesConsignesModif[indexConsigne].id--;
                ListLignesConsignesModif[indexConsigne - 1].id++;

                ListLignesConsignesModif[indexConsigne] = ListLignesConsignesModif[indexConsigne - 1];
                ListLignesConsignesModif[indexConsigne - 1] = consigneTmp;



                ListViewConsignesModif.ItemsSource = null;
                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
            }
        }


        private void recherche_click(object sender, RoutedEventArgs e)
        {
            if(searchName)
            {
                if (!String.IsNullOrEmpty(searchConsigne.Text))
                {
                    gridViewConsi.ItemsSource = null;

                    gridViewConsi.ItemsSource = listListConsignes.FindAll(listConsigne => listConsigne.nom.Contains(searchConsigne.Text));
                    // System.Diagnostics.Debug.Write(gridViewConsi);
                }
                else
                {
                    gridViewConsi.ItemsSource = null;
                    gridViewConsi.ItemsSource = listListConsignes;
                }

            }
            if(searchDate)
            {
                bool vd = true, vf = true;

                foreach (ListConsignes listConsi in listListConsignes)
                {
                    if ((DateTime.Compare(listConsi.dateTime.Date, dateDebut.Date.DateTime.Date) > 0 || DateTime.Compare(listConsi.dateTime.Date, dateDebut.Date.DateTime.Date) == 0) && vd)
                    {
                        indexDebut = listConsi.id;
                        vd = false;
                    }

                    if ((DateTime.Compare(listConsi.dateTime.Date, dateFin.Date.DateTime.Date) < 0 || DateTime.Compare(listConsi.dateTime.Date, dateFin.Date.DateTime.Date) == 0))
                    {
                        indexFin = listConsi.id + 1;
                        //vf = false;
                        //break;
                    }
                }
                List<ListConsignes> listListConsiTmp = listListConsignes.Skip(indexDebut).Take(indexFin - indexDebut).Cast<ListConsignes>().ToList();

                System.Diagnostics.Debug.WriteLine(dateDebut.Date.DateTime.Date);

                gridViewConsi.ItemsSource = null;
                gridViewConsi.ItemsSource = listListConsiTmp;
                
            }
            if (shearchPopup.IsOpen)
            {
                shearchPopup.IsOpen = false;
            }

        }

        private void RechercheListeConsigne_Click(object sender, RoutedEventArgs e)
        {
            if (!shearchPopup.IsOpen) { shearchPopup.IsOpen = true; }
            
        }

        private void quitSearchPopup_Click(object sender, RoutedEventArgs e)
        {
            if (shearchPopup.IsOpen)
            {
                shearchPopup.IsOpen = false;
            }
        }


        private void dateDebut_Tapped(object sender, TappedRoutedEventArgs e)
        {
            searchName = false;
            searchDate = true;
            searchConsigne.Text = "";
            stackPanelSearch.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            relativePanelDate.Background = new SolidColorBrush(Windows.UI.Colors.WhiteSmoke);
        }

        private void searchConsigne_GotFocus(object sender, RoutedEventArgs e)
        {
            searchName = true;
            searchDate = false;          
            stackPanelSearch.Background = new SolidColorBrush(Color.FromArgb(255, 230, 230, 230));
            relativePanelDate.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void delete_Click(object sender, TappedRoutedEventArgs e)
        {

        }

        private void dateFin_Tapped(object sender, TappedRoutedEventArgs e)
        {
            searchName = false;
            searchDate = true;
            searchConsigne.Text = "";
            stackPanelSearch.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            relativePanelDate.Background = new SolidColorBrush(Windows.UI.Colors.WhiteSmoke);
        }

        private void down_Tapped_modif(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            int indexConsigne = ListLignesConsignesModif.IndexOf(consigneTmp);

            if (indexConsigne < ListLignesConsignesModif.Count() - 1)
            {
                ListLignesConsignesModif[indexConsigne].id++;
                ListLignesConsignesModif[indexConsigne + 1].id--;

                ListLignesConsignesModif[indexConsigne] = ListLignesConsignesModif[indexConsigne + 1];
                ListLignesConsignesModif[indexConsigne + 1] = consigneTmp;



                ListViewConsignesModif.ItemsSource = null;
                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
            }
         
        }

        private async void ajoutConsSave_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(popupAJoutConsigneNom.Text))
            {
                Date date = new Date();
                date.day = DateTime.Today.Day.ToString();
                date.month = DateTime.Today.Month.ToString();
                date.month = date.monthToStr();
                date.year = DateTime.Today.Year.ToString();

                ListConsignes newListConsignes = new ListConsignes { id = i, nom = popupAJoutConsigneNom.Text, date = DateTime.Today.Date.ToString("dd-MM-yyyy"), dateTime = DateTime.Today.Date, dateLettre = date };

                newListConsignes.consignes = listConsignetmpCollection.Cast<Consigne>().ToList();
                listListConsignes.Add(newListConsignes);
                i++;
                gridViewConsi.ItemsSource = null;
                gridViewConsi.ItemsSource = listListConsignes;

                await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
                //File.WriteAllText(@"ConsignesData.json", JsonConvert.SerializeObject(listListConsignes));
            }
            if (ajoutPopup.IsOpen) { ajoutPopup.IsOpen = false; }
            contentSelection.Opacity = 1;
            barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchConsigne.IsEnabled = true;


        }
         
        //still not taken for the preparerExperience page

        public async void DownloadImage(String imageName)
        {

            string fileName = string.Empty;
            string fileExtension = string.Empty;
            try
            {
                // fileName = System.IO.Path.GetFileName(txtLink.Text).Substring(0, System.IO.Path.GetFileName(txtLink.Text).Length - 4); // 4: .PNG (type file)
                fileName = imageName;
                fileExtension = imageName.Substring(imageName.LastIndexOf('.'));
                //fileExtension = ".png";
            }
            catch
            {
                MessageDialog dialog = new MessageDialog("Could not determine the name and the extension of the file");
                dialog.ShowAsync();
            }

            StorageFolder folder = App.imageFolder;
            if (folder != null)
            {
                try
                {
                   
                    Uri address = new Uri("http://memorae.hds.utc.fr/api/documents/"+ imageName, UriKind.Absolute);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
                    WebResponse response = await request.GetResponseAsync();
                    Stream stream = response.GetResponseStream();
                    StorageFile file = await folder.CreateFileAsync(fileName + fileExtension, CreationCollisionOption.GenerateUniqueName);
                    await Windows.Storage.FileIO.WriteBytesAsync(file, ReadStream(stream));
                    downloadedFile = file;
                  
                }

                catch
                {
                    MessageDialog dialog = new MessageDialog("Erreur de téléchargement des images");
                    dialog.ShowAsync();
                   
                    downloadedFile = null;

                }
            }

        }

        public byte[] ReadStream(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }

        }
    }
}
