﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Consignela.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Pilot_Exp_Page : Page
    {
        public String date;
        private String type;
        public String utilisation;
        public String pilulier;
        public String format;
        public String mode;
        public String navigation;
        public String modeFlou;


        private String codeGenere;
       
        public Pilot_Exp_Page()
        {
            this.InitializeComponent();
        }

       

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }

        //private void back_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.Frame.CanGoBack)
        //    {
        //        this.Frame.GoBack();
        //    }
        //}

        private void tgb_Ag_Checked(object sender, RoutedEventArgs e)
        {
            type = "AG";
            tgb_Pk.IsChecked = false;
            tgb_Co.IsChecked = false;
            tgb_Au.IsChecked = false;
        }

        private void tgb_Pk_Checked(object sender, RoutedEventArgs e)
        {
            type = "PK";
            tgb_Ag.IsChecked = false;
            tgb_Co.IsChecked = false;
            tgb_Au.IsChecked = false;
        }

        private void tgb_Co_Checked(object sender, RoutedEventArgs e)
        {
            type = "CO";
            tgb_Ag.IsChecked = false;
            tgb_Pk.IsChecked = false;
            tgb_Au.IsChecked = false;
        }

        private void tgb_Au_Checked(object sender, RoutedEventArgs e)
        {
            type = "AU";
            tgb_Ag.IsChecked = false;
            tgb_Pk.IsChecked = false;
            tgb_Co.IsChecked = false;
        }

        private void tgb_Ent_Checked(object sender, RoutedEventArgs e)
        {
            utilisation = "entr";
            tgb_Pass.IsChecked = false;
        }

        private void tgb_Pass_Checked(object sender, RoutedEventArgs e)
        {
            utilisation = "pass";
            tgb_Ent.IsChecked = false;
        }

        private void tgb_activ_Checked(object sender, RoutedEventArgs e)
        {
            navigation = "activee";
            tgb_desac.IsChecked = false;
        }

        private void tgb_desac_Checked(object sender, RoutedEventArgs e)
        {
            navigation = "desactivee";
            tgb_activ.IsChecked = false;
        }

        private void tgb_verb_Checked(object sender, RoutedEventArgs e)
        {
            format = "verb";
            tgb_tab.IsChecked = false;
        }

        private void tgb_tab_Checked(object sender, RoutedEventArgs e)
        {
            format = "tab";
            tgb_verb.IsChecked = false;
        }

        private void tgb_PilTab_Checked(object sender, RoutedEventArgs e)
        {
            pilulier = "tab";
            tgb_PilCirc.IsChecked = false;
        }

        private void tgb_PilCirc_Checked(object sender, RoutedEventArgs e)
        {
            pilulier = "circ";
            tgb_PilTab.IsChecked = false;
        }

        private void tgb_Mono_Checked(object sender, RoutedEventArgs e)
        {
            mode = "mono";
            tgb_Multi.IsChecked = false;
        }

        private void tgb_Multi_Checked(object sender, RoutedEventArgs e)
        {
            mode = "multi";
            tgb_Mono.IsChecked = false;
        }

        private void tgb_FlouActiv_Checked(object sender, RoutedEventArgs e)
        {
            modeFlou = "activee"; 
            tgb_FlouDesac.IsChecked = false;
        }

        private void tgb_FlouDesac_Checked(object sender, RoutedEventArgs e)
        {
            modeFlou = "desactivee";
            tgb_FlouActiv.IsChecked = false;
        }

        private void DatePicker_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            DateTime now = DateTime.Today;
            int age ,month;
            if (now.Year >= date_naiss.Date.Year)
            {
                age = now.Year - date_naiss.Date.Year;
                month = Math.Abs((DateTime.Today.Date.Month) - (date_naiss.Date.Month));

                if (DateTime.Today.Date.Month < date_naiss.Date.Month)
                {
                    age--;
                    month = 12-Math.Abs((date_naiss.Date.Month) - (DateTime.Today.Date.Month));
                }
                tb_age.Text = age.ToString();
                tb_mois.Text = month.ToString();
            }



           
           // tb_mois.Text = (Math.Abs((DateTime.Today.Date.Month) - (date_naiss.Date.Month))).ToString();
            
        }

        private void btn_Gen_Click(object sender, RoutedEventArgs e)
        {
            if(!String.IsNullOrEmpty(tb_nomExperimentateur.Text) && !String.IsNullOrEmpty(tb_numParticipant.Text) && !String.IsNullOrEmpty(tb_nomParticipant.Text) && !String.IsNullOrEmpty(tb_age.Text) && !String.IsNullOrEmpty(type))
            {
                fieldMissing.Opacity = 0;
                codeGenere = "S" + tb_numParticipant.Text + "_" + tb_nomParticipant.Text + tb_age.Text + "_" + tb_mois.Text + type + "_" + dateExp.Date.ToString("dd-MM-yyyy");
                tb_codeGen.Text = codeGenere;

            }
            else
            {
                fieldMissing.Opacity = 1;
            }
        }

        private void expLaunch_Click(object sender, RoutedEventArgs e)
        {
             //System.Diagnostics.Debug.Write(codeGenere);
            App.codeParticipant = codeGenere;
            this.Frame.Navigate(typeof(Experience), null);

            App.experimentateur = tb_nomExperimentateur.Text;
            App.date = dateExp.Date.ToString("dd-MM-yyyy");
            App.type = type;
            App.utilisation = utilisation;
            App.pilulier = pilulier;
            App.format = format;
            App.mode = mode;
            App.navigation = navigation;

        }
    }
}

