﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Consignela.Model;
using Windows.Web.Http;
using Windows.UI;
using System.Threading;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PreparerExperience : Page
    {
        public String bouttonChoisi ="";
        internal String str_Consigne_json;
        internal List<ListConsignes> listListConsignes;
        internal List<Consigne> listConsigneTmp = new List<Consigne>();

        internal String nomConsigne;
        internal bool consChoisie = false;
        internal String msgFeedback;
        internal ListConsignes listConstmp;
        private ListConsignes listConstmp2 = new ListConsignes();
        int indexConstmp;

        private ObservableCollection<Consigne> ListLignesConsignes = new ObservableCollection<Consigne>();
        private Collection<Consigne> listConsignetmpCollection = new Collection<Consigne>();

        private ObservableCollection<Consigne> ListLignesConsignesModif = new ObservableCollection<Consigne>();
        private Collection<Consigne> ListLignesConsignesModifCollec = new Collection<Consigne>();

        int a = 0, b = 0;

        private int index, i, indexDebut, indexFin;
        internal Consigne consigneTmp = new Consigne();
        internal static String imgPath = null;
        internal static String imgNom = null;
        internal String TxtResponse;
        public static StorageFile sampleFile;

        List<string> nameList = new List<string>();
        List<string> elementList = new List<string>();
        List<CheckBox> checkBoxListSharingSpace = new List<CheckBox>();
        public static List<CheckBox> checkBoxListIndex = new List<CheckBox>();

        static List<string> idSharingSpace = new List<string>();
        byte[] bytes;
        public StorageFile downloadedFile;
        public int indeximages = 0;
        public bool searchName = false, searchDate = false;

        //***

        List<Prescription> newListPrescriptions;
        List<Medicament> newMedicament;
        internal String strJsonPresc;
        internal bool prescChoisie = false;
        internal String nomPrescription;

        internal List<Medicament> tabListMedicaments;
        internal List<PrescriptionTab> prescriptionTabList;
        internal String strJsonTabPresc;
        //internal String nomMomentJour;
        internal List<GridView> gridViewTabMedicament = new List<GridView>();
        internal Prescription prescriptionTmp;
        internal StorageFile sampleFilePrescription;
        public static StorageFile sampleFilePrescriptionTab;


        internal List<ExperienceConf> ListExperiencesConf;
        internal List<ExperienceConf> ListExperiencesConfTmp;
        internal ExperienceConf experienceConfTmp;
        public static StorageFile sampleFileExperienceConf;
        internal String strJsonExpConf;
        internal int indexExp;

        public PreparerExperience()
        {
            this.InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
             //selectionPrescription.IsEnabled = false;           
            LancerExperience.IsEnabled = false;
            EnregistrerExpConf.IsEnabled = false;
            chargerDonnees();
            initialiserAffichage();

            loadPrescription();
            loadPrescriptionTabList();
            loadGridView();

            loadExperienceConf();

        }

        // **************************selection Liste Consignes***********************
        public async void chargerDonnees()
        {


            if (App.authentification)
            {
                sampleFile = await App.storageFolder.GetFileAsync(App.logIn + "-consignes.json");
                str_Consigne_json = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-consignes.json");
                listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);
                await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
            }
            else
            {
                sampleFile = await App.storageFolder.GetFileAsync("ConsignesData.json");
                str_Consigne_json = await FileIO.ReadTextAsync(sampleFile);
                listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);
            }


            //str_Consigne_json = File.ReadAllText(@"ConsignesData.json");
            //listListConsignes = JsonConvert.DeserializeObject<List<ListConsignes>>(str_Consigne_json);

            //foreach (ListConsignes listConsigne in listListConsignes)
            //{
            //    foreach (Consigne consigne in listConsigne.consignes)
            //    {
            //        if (!String.IsNullOrEmpty(consigne.imgNom))
            //        {
            //            DownloadImage(consigne.imgNom);
            //        }
            //    }
            //    indeximages++;
            //}
            //if (indeximages == listListConsignes.Count-1)
            //{
            foreach (ListConsignes listConsigne in listListConsignes)
            {
                foreach (Consigne consigne in listConsigne.consignes)
                {
                    if (!String.IsNullOrEmpty(consigne.imgNom))
                    {
                        //DownloadImage(consigne.imgNom);
                        StorageFile imageFile = await App.imageFolder.GetFileAsync(consigne.imgNom);
                        BitmapImage img = new BitmapImage();
                        img = await LoadImage(imageFile);
                        consigne.imgBitmap = img;
                    }
                    else
                    {
                        BitmapImage img = new BitmapImage(new Uri(" ms-appx://Consignela/Assets/landscape.png", UriKind.RelativeOrAbsolute));
                        //img = await LoadImage(imageFile);
                        consigne.imgBitmap = img;
                    }

                }
            }

            if (listListConsignes.Count != 0)
            {
                i = listListConsignes[listListConsignes.Count - 1].id + 1;
            }
            else
            {
                i = 0;
            }
            rechargerListViewConsigne();


        }
        private void initialiserAffichage()
        {
            this.modify.IsEnabled = false;
            //this.delete.IsEnabled = false;
            tb_nomConsigne.Text = "Nom de la liste de Consigne";
            gridViewConsignes.ItemsSource = null;
            afficheListeConsigne.Visibility = Visibility.Collapsed;

        }

        public static string getJsonFromUrl(string url)
        {
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            var response = client.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                // by calling .Result you are performing a synchronous call
                var responseContent = response.Content;

                // by calling .Result you are synchronously reading the result
                string responseString = responseContent.ReadAsStringAsync().Result;

                return responseString;
            }
            else
            {
                return "error";
            }

        }
        private static async Task<BitmapImage> LoadImage(StorageFile file)
        {
            BitmapImage bitmapImage = new BitmapImage();
            FileRandomAccessStream stream = (FileRandomAccessStream)await file.OpenAsync(FileAccessMode.Read);

            bitmapImage.SetSource(stream);

            return bitmapImage;

        }


        public void rechargerListViewConsigne()
        {
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;
        }
        public void rechargerListViewLignesConsigne()
        {
            gridViewConsignes.ItemsSource = null;
            gridViewConsignes.ItemsSource = listConsigneTmp;
        }


        private void gridViewConsi_Tapped(object sender, TappedRoutedEventArgs e)
        {
            b = 3;
            System.Diagnostics.Debug.WriteLine(a);

            if (listListConsignes.Count != 0)
            {
                modify.IsEnabled = true;
                //delete.IsEnabled = true;
                listConstmp = (ListConsignes)gridViewConsi.SelectedItem;
                //ListConsignes listConstmp2 = new ListConsignes();
                listConstmp2 = listConstmp;
                //listConstmp2.nom = listConstmp.nom;
                //listConstmp2.id = listConstmp.id;
                //listConstmp2.date = listConstmp.date;
                //listConstmp2.dateLettre = listConstmp.dateLettre;
                //listConstmp2.consignes = listConstmp.consignes;


                indexConstmp = listListConsignes.IndexOf(listConstmp);

                //System.Diagnostics.Debug.Write(listListConsignes);
                listConsigneTmp = listListConsignes[listConstmp.id].consignes;
                ListLignesConsignesModif = new ObservableCollection<Consigne>(listConsigneTmp);
                //System.Diagnostics.Debug.Write(ListLignesConsignesModif);
                nomConsigne = listListConsignes[listConstmp.id].nom;
                tb_nomConsigne.Text = nomConsigne;

                afficheListeConsigne.Visibility = Visibility.Visible;
                gridViewConsignes.ItemsSource = null;
                gridViewConsignes.ItemsSource = listConsigneTmp;


                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;

                consChoisie = true;
            }

        }
        public void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            if (ModifierPopup.IsOpen) { ModifierPopup.IsOpen = false; }
            contentSelection.Opacity = 1;
            //barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchWithName.IsEnabled = true;
        }
        private void ajoutConsigne_Click(object sender, RoutedEventArgs e)
        {
            if (!ajoutPopup.IsOpen) { ajoutPopup.IsOpen = true; }
            contentSelection.Opacity = 0.5;
            //barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;
            modify.IsEnabled = false;
            searchWithName.IsEnabled = false;

        }

        private void CloseAjoutPopupClicked(object sender, RoutedEventArgs e)
        {
            if (ajoutPopup.IsOpen) { ajoutPopup.IsOpen = false; }
            contentSelection.Opacity = 1;
            //barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchWithName.IsEnabled = true;

        }
        private void chargerListConsignetmp()
        {

            if (ListLignesConsignes != null)
            {
                listConsignetmpCollection = ListLignesConsignes;
            }
            listViewListConsigne.ItemsSource = null;
            listViewListConsigne.ItemsSource = listConsignetmpCollection;

        }
        private void btn_ajoutLigneConsigne_Click(object sender, RoutedEventArgs e)
        {
            chargerListConsignetmp();
            System.Diagnostics.Debug.Write(listConsignetmpCollection);
            if (listConsignetmpCollection.Count() != 0)
            {
                index = listConsignetmpCollection[listConsignetmpCollection.Count - 1].id + 1;
            }
            else
            {
                index = 0;
            }

            Consigne ligneConsigne = new Consigne { id = index, description = "", img_path = "Assets/icons/addimg.png" };
            ListLignesConsignes.Add(ligneConsigne);
            index++;
        }

        private async void btn_Ajout_img_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;

            System.Diagnostics.Debug.Write(listViewListConsigne.SelectedItem);
            System.Diagnostics.Debug.Write(consigneTmp);

            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();

            if (file != null)
            {

                imgNom = file.Name;
                if (await App.imageFolder.TryGetItemAsync(imgNom) != null)
                    System.Diagnostics.Debug.WriteLine(imgNom + " exists.");
                else  // Return value of TryGetItemAsync is null.
                    await file.CopyAsync(App.imageFolder);

                //imgPath = "Assets/" + imgNom;
                var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var imageBtm = new BitmapImage();

                imageBtm.SetSource(stream);
                ListLignesConsignes[consigneTmp.id].img_path = file.Path;

                ListLignesConsignes[consigneTmp.id].imgBitmap = imageBtm;
                ListLignesConsignes[consigneTmp.id].imgNom = imgNom;

                chargerListConsignetmp();

            }
            else
            {
                //this.tb_img_name.Text = "aucune image";
            }
        }

        private void deleteAjout_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            if (listConsignetmpCollection.Count > 1)
            {

                listConsignetmpCollection.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConsignetmpCollection)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;

            }
        }
        private void ajoutListeConsignebtn_Click(object sender, RoutedEventArgs e)
        {
            if (!ajoutPopup.IsOpen) { ajoutPopup.IsOpen = true; }
            contentSelection.Opacity = 0.1;
            //barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;

            searchWithName.IsEnabled = false;
        }
        private void chargerListConsignetmpModif()
        {

            if (ListLignesConsignesModif != null)
            {
                ListLignesConsignesModifCollec = ListLignesConsignesModif;
            }
            ListViewConsignesModif.ItemsSource = null;
            ListViewConsignesModif.ItemsSource = ListLignesConsignesModifCollec;

        }
        private void modify_Click(object sender, RoutedEventArgs e)
        {
            popupConsigneNom.Text = listListConsignes[listConstmp.id].nom;
            if (!ModifierPopup.IsOpen) { ModifierPopup.IsOpen = true; }
            ModifierPopup.Height = Window.Current.Bounds.Height;
            contentSelection.Opacity = 0.5;
            //barSelection.IsEnabled = false;
            gridViewConsi.IsEnabled = false;
            searchWithName.IsEnabled = false;
        }
        private async void enregistrerSous_Click(object sender, RoutedEventArgs e)
        {
            listConstmp.nom = popupConsigneNom.Text;
            tb_nomConsigne.Text = popupConsigneNom.Text;
            //listConstmp.consignes[0].description = ListViewConsignes.SelectedItem.ToString();
            listConstmp.consignes = ListViewConsignesModif.Items.OfType<Consigne>().ToList();

            listListConsignes.Add(listConstmp);
            listListConsignes[listListConsignes.Count - 1].id = listListConsignes.Count - 1;

            listListConsignes[indexConstmp] = listConstmp2;
            rechargerListViewConsigne();
            rechargerListViewLignesConsigne();

            ClosePopupClicked(sender, e);
            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }
        private async void modifConsSave_Click(object sender, RoutedEventArgs e)
        {
            listConstmp = listConstmp2;
            listConstmp.nom = popupConsigneNom.Text;
            tb_nomConsigne.Text = popupConsigneNom.Text;
            //listConstmp.consignes[0].description = ListViewConsignes.SelectedItem.ToString();
            listConstmp.consignes = ListViewConsignesModif.Items.OfType<Consigne>().ToList();

            listListConsignes[listConstmp.id] = listConstmp;

            rechargerListViewConsigne();
            rechargerListViewLignesConsigne();

            ClosePopupClicked(sender, e);
            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }
        private void deleteModif_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            if (listConstmp2.consignes.Count > 1)
            {

                listConstmp2.consignes.Remove(consigneTmp);
                int indexC = 0;
                foreach (Consigne consigne in listConstmp2.consignes)
                {
                    consigne.id = indexC;
                    indexC++;
                }
                ListViewConsignesModif.ItemsSource = null;
                ListViewConsignesModif.ItemsSource = listConstmp2.consignes;

            }
        }

        private async void imgModif_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;

            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                imgNom = file.Name;
                if (await App.imageFolder.TryGetItemAsync(imgNom) != null)
                    System.Diagnostics.Debug.WriteLine(imgNom + " exists.");
                else  // Return value of TryGetItemAsync is null.
                    await file.CopyAsync(App.imageFolder);

                var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var imageBtm = new BitmapImage();

                imageBtm.SetSource(stream);


                ListLignesConsignesModif[consigneTmp.id].imgBitmap = imageBtm;
                ListLignesConsignesModif[consigneTmp.id].img_path = file.Path;
                ListLignesConsignesModif[consigneTmp.id].imgNom = imgNom;

                chargerListConsignetmpModif();

            }
            else
            {
                //this.tb_img_name.Text = "aucune image";
            }
        }
        private async void delete_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog deleteConsigne = new MessageDialog("Attention voulez-vous vraiment supprimer cette consigne ?", "Suppression de la consigne");
            deleteConsigne.Commands.Add(new UICommand("Supprimer", supprimerConsigne, 1));
            deleteConsigne.Commands.Add(new UICommand("Annuler", CancelCommand, 2));
            await deleteConsigne.ShowAsync();

        }
        public async void supprimerConsigne(IUICommand command)
        {
            listListConsignes.Remove(listConstmp);
            int index = 0;
            foreach (ListConsignes listConsigneD in listListConsignes)
            {
                listConsigneD.id = index;
                index++;
            }
            gridViewConsi.ItemsSource = null;
            gridViewConsi.ItemsSource = listListConsignes;
            initialiserAffichage();

            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
        }
        public void CancelCommand(IUICommand command)
        {

        }
        public void consigneChoisie()
        {
            textSelectConsiBtn.Text = "Liste des consignes choisie : " + nomConsigne;
            selectionConsigne.Background = new SolidColorBrush(Windows.UI.Colors.White);
            selectionConsigne.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionConsigne.BorderThickness = new Thickness(2.0);
            textSelectConsiBtn.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
        }
        private void selection_Click(object sender, RoutedEventArgs e)
        {
            if (consChoisie)
            {
                if (App.consignesSelect == null)
                {
                    MainMenu.progressBarValue += 50;
                }

                App.consignesSelect = listListConsignes[listConstmp.id];
                msgFeedback = "la consigne " + nomConsigne + " a bien été  choisie";
                consigneChoisie();
                selectionPrescription.IsEnabled = true;
            }
            else
            {
                msgFeedback = "La Consigne est non choisise";
            }



            //MessageDialog selectConsigne = new MessageDialog(msgFeedback, "Selection de la consigne");
            //await selectConsigne.ShowAsync();
        }

        private async void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
            enregistrementServeur(sampleFile);

        }
        private void quitAuthenPopup_Click(object sender, RoutedEventArgs e)
        {
            if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
        }
       
        private void priveSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!authenPopup.IsOpen && App.authentification == false) { authenPopup.IsOpen = true; }

        }

        private void partageSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!authenPopup.IsOpen && App.authentification == false) { authenPopup.IsOpen = true; }

        }

        // **************** à rectifier !!!!!!!!!!!!
        private async void connecter_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login.Text + "\"" + ",\"pass\":" + "\"" + pass.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login.Text;
                App.password = pass.Password;
                App.authentification = true;

                if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
                //fileToByte();
                var stream = await sampleFile.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFile.Path);

                SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();

                for (int i = 0; i < a; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }



            }
            else
            {
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
        }
        public async Task Post(string website, string args)
        {

            //init client
            Windows.Web.Http.HttpClient hc = new Windows.Web.Http.HttpClient();
            //send post request
            Windows.Web.Http.HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            TxtResponse = await hrm.Content.ReadAsStringAsync();
            App.txtresponseLogin = TxtResponse;
            //System.Diagnostics.Debug.Write(TxtResponse);
            hc.Dispose();
        }

        private void quitSharingPopup_Click(object sender, RoutedEventArgs e)
        {
            if (sharingSpacePopup.IsOpen)
            {
                sharingSpacePopup.IsOpen = false;
            }
        }
        private async void showShSpInfo_Click(object sender, RoutedEventArgs e)
        {
            showSharingSpaceInfo();
            if (idSharingSpace.Count != 0)
            {

                Index.getIndex();
                List<string> nom_cpt = Index.getnom_cpt();
                checkBoxListIndex.Clear();
                IndexStackPanel1.Children.Clear();

                for (int i = 0; i < nom_cpt.Count; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nom_cpt[i];
                    newCheckBox.Name = "checkbox" + i;

                    checkBoxListIndex.Add(newCheckBox);
                    IndexStackPanel1.Children.Add(newCheckBox);

                }
                if (!indexPopup.IsOpen) { indexPopup.IsOpen = true; }
            }
            else
            {
                MessageDialog SharingSpaceNotSelected = new MessageDialog("Veuillez séléctionner l'espace de partage");
                await SharingSpaceNotSelected.ShowAsync();
            }

        }
        public void showSharingSpaceInfo()
        {
            idSharingSpace.Clear();
            // foreach (CheckBox checkbox in checkBoxListSharingSpace)
            for (int i = 0; i < checkBoxListSharingSpace.Count; i++)
            {
                if (checkBoxListSharingSpace[i].IsChecked == true)
                {
                    string[] split = elementList[i].Split('#');
                    idSharingSpace.Add(split[1]);
                }
            }
        }
        public static List<CheckBox> getcheckBoxListIndex()
        {
            return checkBoxListIndex;
        }
        public static List<string> getIdSharingSpace()
        {
            return idSharingSpace;
        }

        private void quitIndexPopup_Click(object sender, RoutedEventArgs e)
        {
            if (indexPopup.IsOpen)
            {
                indexPopup.IsOpen = false;
            }

        }

        

        private void up_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmpCollection.IndexOf(consigneTmp);
            if (indexConsigne > 0)
            {
                listConsignetmpCollection[indexConsigne].id--;
                listConsignetmpCollection[indexConsigne - 1].id++;

                listConsignetmpCollection[indexConsigne] = listConsignetmpCollection[indexConsigne - 1];
                listConsignetmpCollection[indexConsigne - 1] = consigneTmp;

                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;
            }
        }

        private void down_Tapped(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)listViewListConsigne.SelectedItem;
            int indexConsigne = listConsignetmpCollection.IndexOf(consigneTmp);

            if (indexConsigne < listConsignetmpCollection.Count() - 1)
            {
                listConsignetmpCollection[indexConsigne].id++;
                listConsignetmpCollection[indexConsigne + 1].id--;

                listConsignetmpCollection[indexConsigne] = listConsignetmpCollection[indexConsigne + 1];
                listConsignetmpCollection[indexConsigne + 1] = consigneTmp;



                listViewListConsigne.ItemsSource = null;
                listViewListConsigne.ItemsSource = listConsignetmpCollection;
            }
        }

        private void up_Tapped_modif(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            int indexConsigne = ListLignesConsignesModif.IndexOf(consigneTmp);
            if (indexConsigne > 0)
            {
                ListLignesConsignesModif[indexConsigne].id--;
                ListLignesConsignesModif[indexConsigne - 1].id++;

                ListLignesConsignesModif[indexConsigne] = ListLignesConsignesModif[indexConsigne - 1];
                ListLignesConsignesModif[indexConsigne - 1] = consigneTmp;



                ListViewConsignesModif.ItemsSource = null;
                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
            }
        }


        private void recherche_click(object sender, RoutedEventArgs e)
        {
            
            if (bouttonChoisi == "ListeConsignes")
            {
                if (searchName && !String.IsNullOrEmpty(searchWithName.Text))
                {
                    if (!String.IsNullOrEmpty(searchWithName.Text))
                    {
                        gridViewConsi.ItemsSource = null;

                        gridViewConsi.ItemsSource = listListConsignes.FindAll(listConsigne => listConsigne.nom.Contains(searchWithName.Text));
                        // System.Diagnostics.Debug.Write(gridViewConsi);
                    }
                    else
                    {
                        gridViewConsi.ItemsSource = null;
                        gridViewConsi.ItemsSource = listListConsignes;
                    }

                }
                if (searchDate)
                {
                    bool vd = true, vf = true;

                    foreach (ListConsignes listConsi in listListConsignes)
                    {
                        if ((DateTime.Compare(listConsi.dateTime.Date, dateDebut.Date.DateTime.Date) > 0 || DateTime.Compare(listConsi.dateTime.Date, dateDebut.Date.DateTime.Date) == 0) && vd)
                        {
                            indexDebut = listConsi.id;
                            vd = false;
                        }

                        if ((DateTime.Compare(listConsi.dateTime.Date, dateFin.Date.DateTime.Date) < 0 || DateTime.Compare(listConsi.dateTime.Date, dateFin.Date.DateTime.Date) == 0))
                        {
                            indexFin = listConsi.id + 1;
                            //vf = false;
                            //break;
                        }
                    }
                    List<ListConsignes> listListConsiTmp = listListConsignes.Skip(indexDebut).Take(indexFin - indexDebut).Cast<ListConsignes>().ToList();

                    System.Diagnostics.Debug.WriteLine(dateDebut.Date.DateTime.Date);

                    gridViewConsi.ItemsSource = null;
                    gridViewConsi.ItemsSource = listListConsiTmp;

                }             
            }


            if (bouttonChoisi == "Prescription")
            {
                if (searchName && !String.IsNullOrEmpty(searchWithName.Text))
                {
                    if (!String.IsNullOrEmpty(searchWithName.Text))
                    {
                        gridViewPresc.ItemsSource = null;

                        gridViewPresc.ItemsSource = newListPrescriptions.FindAll(prescriptionSearch => prescriptionSearch.nom.Contains(searchWithName.Text));
                        // System.Diagnostics.Debug.Write(gridViewConsi);
                    }
                    else
                    {
                        gridViewPresc.ItemsSource = null;
                        gridViewPresc.ItemsSource = listListConsignes;
                    }

                }
                if (searchDate)
                {
                    bool vd = true, vf = true;

                    foreach (Prescription prescriptionSearch in newListPrescriptions)
                    {
                        if ((DateTime.Compare(prescriptionSearch.dateTime.Date, dateDebut.Date.DateTime.Date) > 0 || DateTime.Compare(prescriptionSearch.dateTime.Date, dateDebut.Date.DateTime.Date) == 0) && vd)
                        {
                            indexDebut = prescriptionSearch.id;
                            vd = false;
                        }

                        if ((DateTime.Compare(prescriptionSearch.dateTime.Date, dateFin.Date.DateTime.Date) < 0 || DateTime.Compare(prescriptionSearch.dateTime.Date, dateFin.Date.DateTime.Date) == 0))
                        {
                            indexFin = prescriptionSearch.id + 1;
                            //vf = false;
                            //break;
                        }
                    }
                    List<Prescription> listPrescriptionTmp = newListPrescriptions.Skip(indexDebut).Take(indexFin - indexDebut).Cast<Prescription>().ToList();

                    System.Diagnostics.Debug.WriteLine(dateDebut.Date.DateTime.Date);

                    gridViewPresc.ItemsSource = null;
                    gridViewPresc.ItemsSource = listPrescriptionTmp;

                }
            }



            if (shearchPopup.IsOpen)
            {
                shearchPopup.IsOpen = false;
            }
        }

        private void RechercheListe_Click(object sender, RoutedEventArgs e)
        {
            if (!shearchPopup.IsOpen) { shearchPopup.IsOpen = true; }
        }

        private void quitSearchPopup_Click(object sender, RoutedEventArgs e)
        {
            if (shearchPopup.IsOpen)
            {
                shearchPopup.IsOpen = false;
            }
        }


        private void dateDebut_Tapped(object sender, TappedRoutedEventArgs e)
        {
            searchName = false;
            searchDate = true;
            searchWithName.Text = "";
            stackPanelSearch.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            relativePanelDate.Background = new SolidColorBrush(Windows.UI.Colors.WhiteSmoke);
        }

        private void searchConsigne_GotFocus(object sender, RoutedEventArgs e)
        {
            searchName = true;
            searchDate = false;
            stackPanelSearch.Background = new SolidColorBrush(Color.FromArgb(255, 230, 230, 230));
            relativePanelDate.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
        }

        private void TextBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void dateFin_Tapped(object sender, TappedRoutedEventArgs e)
        {
            searchName = false;
            searchDate = true;
            searchWithName.Text = "";
            stackPanelSearch.Background = new SolidColorBrush(Windows.UI.Colors.DarkGray);
            relativePanelDate.Background = new SolidColorBrush(Windows.UI.Colors.WhiteSmoke);
        }

        private void down_Tapped_modif(object sender, TappedRoutedEventArgs e)
        {
            consigneTmp = (Consigne)ListViewConsignesModif.SelectedItem;
            int indexConsigne = ListLignesConsignesModif.IndexOf(consigneTmp);

            if (indexConsigne < ListLignesConsignesModif.Count() - 1)
            {
                ListLignesConsignesModif[indexConsigne].id++;
                ListLignesConsignesModif[indexConsigne + 1].id--;

                ListLignesConsignesModif[indexConsigne] = ListLignesConsignesModif[indexConsigne + 1];
                ListLignesConsignesModif[indexConsigne + 1] = consigneTmp;



                ListViewConsignesModif.ItemsSource = null;
                ListViewConsignesModif.ItemsSource = ListLignesConsignesModif;
            }

        }

        private async void ajoutConsSave_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(popupAJoutConsigneNom.Text))
            {
                Date date = new Date();
                date.day = DateTime.Today.Day.ToString();
                date.month = DateTime.Today.Month.ToString();
                date.month = date.monthToStr();
                date.year = DateTime.Today.Year.ToString();

                ListConsignes newListConsignes = new ListConsignes { id = i, nom = popupAJoutConsigneNom.Text, date = DateTime.Today.Date.ToString("dd-MM-yyyy"), dateTime = DateTime.Today.Date, dateLettre = date };

                newListConsignes.consignes = listConsignetmpCollection.Cast<Consigne>().ToList();
                listListConsignes.Add(newListConsignes);
                i++;
                gridViewConsi.ItemsSource = null;
                gridViewConsi.ItemsSource = listListConsignes;

                await FileIO.WriteTextAsync(sampleFile, JsonConvert.SerializeObject(listListConsignes));
                //File.WriteAllText(@"ConsignesData.json", JsonConvert.SerializeObject(listListConsignes));
            }
            if (ajoutPopup.IsOpen) { ajoutPopup.IsOpen = false; }
            contentSelection.Opacity = 1;
            //barSelection.IsEnabled = true;
            gridViewConsi.IsEnabled = true;

            searchWithName.IsEnabled = true;


        }
        // ****************************************************




        private void selectionConsigne_Click(object sender, RoutedEventArgs e)
        {
            bouttonChoisi = "ListeConsignes";
            SelectionPrescription.Visibility = Visibility.Collapsed;
            contentSelection.Visibility = Visibility.Visible;
        }

        private void selectionPrescription_Click(object sender, RoutedEventArgs e)
        {
            bouttonChoisi = "Prescription";
            contentSelection.Visibility = Visibility.Collapsed;
            SelectionPrescription.Visibility = Visibility.Visible;
        }

        private void LancerExperience_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Pilot_Exp_Page));
        }

        private void OnLayoutSearchPopup(object sender, object e)
        {
            if (shearchPopupChild.ActualWidth == 0 && shearchPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.shearchPopup.HorizontalOffset;
            double ActualVerticalOffset = this.shearchPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - shearchPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - shearchPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.shearchPopup.HorizontalOffset = NewHorizontalOffset;
                this.shearchPopup.VerticalOffset = NewVerticalOffset;
            }
        }



        private void OnLayoutUpdatedIndex(object sender, object e)
        {
            if (indexPopupChild.ActualWidth == 0 && indexPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.indexPopup.HorizontalOffset;
            double ActualVerticalOffset = this.indexPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - indexPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - indexPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.indexPopup.HorizontalOffset = NewHorizontalOffset;
                this.indexPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void OnLayoutUpdatedSharing(object sender, object e)
        {
            if (sharingSpacePopupChild.ActualWidth == 0 && sharingSpacePopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.sharingSpacePopup.HorizontalOffset;
            double ActualVerticalOffset = this.sharingSpacePopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - sharingSpacePopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - sharingSpacePopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.sharingSpacePopup.HorizontalOffset = NewHorizontalOffset;
                this.sharingSpacePopup.VerticalOffset = NewVerticalOffset;
            }
        }


        private void OnLayoutUpdatedAuthen(object sender, object e)
        {
            if (authenPopupChild.ActualWidth == 0 && authenPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void OnLayoutUpdated(object sender, object e)
        {
            if (popupChild.ActualWidth == 0 && popupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.ModifierPopup.HorizontalOffset;
            double ActualVerticalOffset = this.ModifierPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - popupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - popupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.ModifierPopup.HorizontalOffset = NewHorizontalOffset;
                this.ModifierPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void OnLayoutUpdatedAjout(object sender, object e)
        {
            if (ajoutPopupChild.ActualWidth == 0 && ajoutPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.ajoutPopup.HorizontalOffset;
            double ActualVerticalOffset = this.ajoutPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - ajoutPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - ajoutPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.ajoutPopup.HorizontalOffset = NewHorizontalOffset;
                this.ajoutPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        //***************** en commun **********
        private async void enregistrementServeur( StorageFile sampleFileTmp)

        {

            if (!authenPopup.IsOpen && App.authentification == false)
            {
                authenPopup.IsOpen = true;

            }
            else
            {
                if (!sharingSpacePopup.IsOpen)
                {
                    sharingSpacePopup.IsOpen = true;
                    contentSelection.Opacity = 0.5;
                    //barSelection.IsEnabled = false;
                    gridViewConsi.IsEnabled = false;
                    searchWithName.IsEnabled = false;
                }
                var stream = await sampleFileTmp.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFileTmp.Path);

                // SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();
                for (int i = 0; i < a; i++)
                {

                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }
            }
        }

        private async void indexEnregistrement(StorageFile sampleFileTmp,String message,String Titre)
        {
            Index.showInfo(checkBoxListIndex, sampleFileTmp, idSharingSpace);
            MessageDialog saveServerFile = new MessageDialog(message, Titre);
            await saveServerFile.ShowAsync();
        }
        private void indexSave_Click(object sender, RoutedEventArgs e)
        {
            if(bouttonChoisi == "ListeConsignes")
                indexEnregistrement(sampleFile, "les listes des consignes ont bien été enregistrées ", "Enregistrement Listes des consignes dans le serveur");
            else if (bouttonChoisi == "Prescription")
                indexEnregistrement(sampleFilePrescriptionTab, "La prescription a bien été enregistrées ", "Enregistrement Prescription dans le serveur");

        }

        // **************************selection Prescription***********************

        public async void supprimerPrescription(IUICommand command)
        {
            newListPrescriptions.Remove(prescriptionTmp);
            int index = 0;
            foreach (Prescription prescriptionD in newListPrescriptions)
            {
                prescriptionD.id = index;
                index++;
            }
            gridViewPresc.ItemsSource = null;
            gridViewPresc.ItemsSource = newListPrescriptions;
            initialiserAffichagePrescription();

           

            prescriptionTabList.Remove(prescriptionTabList[prescriptionTmp.id]);
            int indexTab = 0;
            foreach (PrescriptionTab prescriptionTabD in prescriptionTabList)
            {
                prescriptionTabD.id = indexTab;
                indexTab++;
            }

            await FileIO.WriteTextAsync(sampleFilePrescription, JsonConvert.SerializeObject(newListPrescriptions));
            await FileIO.WriteTextAsync(sampleFilePrescriptionTab, JsonConvert.SerializeObject(prescriptionTabList));
        }

        private void initialiserAffichagePrescription ()
        {
            tgb_tabulaire.IsChecked = false;
            tgb_Verbale.IsChecked = true;

            prescTabulaire.Opacity = 0;
            typePresButton.Visibility = Visibility.Collapsed;
        }

        public async void loadPrescription()
        {
            initialiserAffichagePrescription();

            if (App.authentification)
            {

                sampleFilePrescription = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionData.json");
                strJsonPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-PrescriptionData.json");
                newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
                await FileIO.WriteTextAsync(sampleFilePrescription, JsonConvert.SerializeObject(newListPrescriptions));
            }
            else
            {
                sampleFilePrescription = await App.storageFolder.GetFileAsync("PrescriptionData.json");
                strJsonPresc = await FileIO.ReadTextAsync(sampleFilePrescription);
                newListPrescriptions = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
            }


            gridViewPresc.ItemsSource = null;
            gridViewPresc.ItemsSource = newListPrescriptions;
            //newListPrescriptions.Reverse();                
        }

        public async void loadPrescriptionTabList()
        {
            if (App.authentification)
            {
                sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionTabData.json");
                strJsonTabPresc = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-PrescriptionTabData.json");
                prescriptionTabList = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);
                await FileIO.WriteTextAsync(sampleFilePrescriptionTab, JsonConvert.SerializeObject(prescriptionTabList));
            }
            else
            {
                sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync("PrescriptionTabData.json");
                strJsonTabPresc = await FileIO.ReadTextAsync(sampleFilePrescriptionTab);
                prescriptionTabList = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);
            }

        }

        private async void btn_SavePresc_Click(object sender, RoutedEventArgs e)
        {
            await FileIO.WriteTextAsync(sampleFilePrescriptionTab, JsonConvert.SerializeObject(prescriptionTabList));
            enregistrementServeur(sampleFilePrescriptionTab);
        }

        private async void deletePresc_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MessageDialog deletePrescription = new MessageDialog("Attention voulez-vous vraiment supprimer cette Prescription ?", "Suppression de la Prescription");
            deletePrescription.Commands.Add(new UICommand("Supprimer", supprimerPrescription, 1));
            deletePrescription.Commands.Add(new UICommand("Annuler", CancelCommand, 2));
            await deletePrescription.ShowAsync();
        }

        public void prescriptionChoisie()
        {
            textSelectPrescBtn.Text = "Prescription choisie : " + nomPrescription;
            selectionPrescription.Background = new SolidColorBrush(Windows.UI.Colors.White);
            selectionPrescription.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionPrescription.BorderThickness = new Thickness(2.0);
            textSelectPrescBtn.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
        }

        private void preparerExperience_Click(object sender, RoutedEventArgs e)
        {
            menuSplitView.IsPaneOpen = true;
            selectionConsigne.IsEnabled = true;

            textSelectConsiBtn.Text = "Selectionner Liste Consignes";
            textSelectPrescBtn.Text = "Selectionner prescription";


            selectionPrescription.Background = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionPrescription.BorderThickness = new Thickness(0);
            textSelectPrescBtn.Foreground = new SolidColorBrush(Windows.UI.Colors.White);
           


            selectionConsigne.Background = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionConsigne.BorderThickness = new Thickness(0);
            textSelectConsiBtn.Foreground = new SolidColorBrush(Windows.UI.Colors.White);
            //selectionConsigne.IsEnabled = false;

            selectionPrescription.IsEnabled = false;
            EnregistrerExpConf.IsEnabled = false;
            LancerExperience.IsEnabled = false;

            ChoixOuPrepaExp.Visibility = Visibility.Collapsed;
            contentSelection.Visibility = Visibility.Visible;



        }

        private void ChoixExperience_Click(object sender, RoutedEventArgs e)
        {
            ListViewExpConf.ItemsSource = null;
            ListViewExpConf.ItemsSource = ListExperiencesConfTmp;
            menuSplitView.IsPaneOpen = true;
            
            textSelectConsiBtn.Text = "Selectionner Liste Consignes";
            textSelectPrescBtn.Text = "Selectionner prescription";

            
            selectionPrescription.Background = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));           
            selectionPrescription.BorderThickness = new Thickness(0);
            textSelectPrescBtn.Foreground = new SolidColorBrush(Windows.UI.Colors.White);
            
            
            
            selectionConsigne.Background = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionConsigne.BorderThickness = new Thickness(0);
            textSelectConsiBtn.Foreground = new SolidColorBrush(Windows.UI.Colors.White);

            selectionPrescription.IsEnabled = false;
            selectionConsigne.IsEnabled = false;

            EnregistrerExpConf.IsEnabled = false;

            ChoixOuPrepaExp.Visibility = Visibility.Collapsed;
            ChoixExperienceConf.Visibility = Visibility.Visible;

        }

      

        private void selectionPrescBtn_Click(object sender, RoutedEventArgs e)
        {
            if (prescChoisie)
            {
                if (App.getPrescriptionSelect() == null)
                {
                    MainMenu.progressBarValue += 50;
                }
                
                App.setPrescriptionSelect(newListPrescriptions[prescriptionTmp.id]);
                App.prescriptionTabSelect = prescriptionTabList[prescriptionTmp.id];
                prescriptionChoisie();
                EnregistrerExpConf.IsEnabled = true;
                LancerExperience.IsEnabled = true;
                msgFeedback = "la prescription " + nomPrescription + " a bien été  choisie";
            }
            else
            {
                msgFeedback = "La prescription est non choisise";
            }

            //MessageDialog selectConsigne = new MessageDialog(msgFeedback, "Selection de la prescription");
            //await selectConsigne.ShowAsync();


        }

        //private async void TextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        //{
        //    if (e.Key == Windows.System.VirtualKey.Enter)
        //    {
        //        if(!String.IsNullOrEmpty(ExperienceConfName.Text))
        //        {
        //            ListExperiencesConf = new List<ExperienceConf>();
        //            if (ListExperiencesConfTmp != null)
        //            {
        //                ListExperiencesConf = ListExperiencesConfTmp;
        //            }
        //            ListExperiencesConf.Add(new ExperienceConf { id = indexExp, nom = ExperienceConfName.Text, dateTime = DateTime.Today.Date, listeConsigne = App.consignesSelect, prescription = App.prescriptionSelect, prescriptionTab = App.prescriptionTabSelect });
        //            await FileIO.WriteTextAsync(sampleFileExperienceConf, JsonConvert.SerializeObject(ListExperiencesConf));
        //            EnregistrerExpConfFlyout.Hide();
        //            MenuFlyout m = new MenuFlyout();
        //            MenuFlyoutItem mn = new MenuFlyoutItem();
        //            mn.Text = "La configuration de l'experience à été ajoutée";
        //            m.Items.Add(mn);
        //            m.ShowAt(EnregistrerExpConf);
        //            await Task.Delay(2000);
        //            m.Hide();
        //            ListViewExpConf.ItemsSource = null;
        //            ListViewExpConf.ItemsSource = ListExperiencesConfTmp;

        //        }
        //    }
        //}

        private void gridViewExpConf_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void deleteExpConf_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        private void retour_Click(object sender, RoutedEventArgs e)
        {
            menuSplitView.IsPaneOpen = false;
            contentSelection.Visibility = Visibility.Collapsed;
            SelectionPrescription.Visibility = Visibility.Collapsed;
            ChoixExperienceConf.Visibility = Visibility.Collapsed;
            ChoixOuPrepaExp.Visibility = Visibility.Visible;
        }

        private async void ExperienceConfName_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                if (!String.IsNullOrEmpty(ExperienceConfName.Text))
                {
                    ListExperiencesConf = new List<ExperienceConf>();
                    if (ListExperiencesConfTmp != null)
                    {
                        ListExperiencesConf = ListExperiencesConfTmp;
                    }
                    ListExperiencesConf.Add(new ExperienceConf { id = indexExp, nom = ExperienceConfName.Text, dateTime = DateTime.Today.Date, listeConsigne = App.consignesSelect, prescription = App.prescriptionSelect, prescriptionTab = App.prescriptionTabSelect });
                    indexExp++;
                    await FileIO.WriteTextAsync(sampleFileExperienceConf, JsonConvert.SerializeObject(ListExperiencesConf));
                    EnregistrerExpConfFlyout.Hide();
                    MenuFlyout m = new MenuFlyout();
                    MenuFlyoutItem mn = new MenuFlyoutItem();
                    mn.Text = "La configuration de l'experience à été ajoutée";
                    m.Items.Add(mn);
                    m.ShowAt(EnregistrerExpConf);
                    await Task.Delay(2000);
                    m.Hide();
                    ListViewExpConf.ItemsSource = null;
                    ListViewExpConf.ItemsSource = ListExperiencesConfTmp;

                }
            }
        }

        private void ListViewExpConf_Tapped(object sender, TappedRoutedEventArgs e)
        {
            experienceConfTmp = (ExperienceConf)ListViewExpConf.SelectedItem;
            App.consignesSelect = experienceConfTmp.listeConsigne;
            App.prescriptionSelect = experienceConfTmp.prescription;
            App.prescriptionTabSelect = experienceConfTmp.prescriptionTab;

            textSelectConsiBtn.Text = "Liste des consignes choisie : " + App.consignesSelect.nom;
            selectionConsigne.Background = new SolidColorBrush(Windows.UI.Colors.White);
            selectionConsigne.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionConsigne.BorderThickness = new Thickness(2.0);
            textSelectConsiBtn.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));

            textSelectPrescBtn.Text = "Prescription choisie : " + App.prescriptionSelect.nom;
            selectionPrescription.Background = new SolidColorBrush(Windows.UI.Colors.White);
            selectionPrescription.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            selectionPrescription.BorderThickness = new Thickness(2.0);
            textSelectPrescBtn.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));

            LancerExperience.IsEnabled = true;

        }

        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        private void gridViewPresc_Tapped(object sender, TappedRoutedEventArgs e)
        {
            info_label.Visibility = Visibility.Collapsed;
            typePresButton.Visibility = Visibility.Visible;

            prescriptionTmp = (Prescription)gridViewPresc.SelectedItem;
            newMedicament = newListPrescriptions[prescriptionTmp.id].medicaments;

            listViewSelectPresc.ItemsSource = null;
            listViewSelectPresc.ItemsSource = newMedicament;

            gridViewNomsMedicaments.ItemsSource = null;
            gridViewNomsMedicaments.ItemsSource = newMedicament;

            nomPrescription = newListPrescriptions[prescriptionTmp.id].nom;
            //tb_nomCons_bar.Text = nomPrescription;

            for (int i = 0; i < 21; i++)
            {
                if ((prescriptionTabList[prescriptionTmp.id].momentJour[i].listMedicaments) != null)
                {
                    tabListMedicaments = prescriptionTabList[prescriptionTmp.id].momentJour[i].listMedicaments;
                    gridViewTabMedicament[i].ItemsSource = null;
                    gridViewTabMedicament[i].ItemsSource = tabListMedicaments;
                }
            }
            prescChoisie = true;

        }
        //private async void btn_selectPresc_Tapped(object sender, TappedRoutedEventArgs e)
        //{
        //    if (prescChoisie)
        //    {
        //        if (App.getPrescriptionSelect() == null)
        //        {
        //            MainMenu.progressBarValue += 50;
        //        }

        //        App.setPrescriptionSelect(newListPrescriptions[prescriptionTmp.id]);
        //        App.prescriptionTabSelect = prescriptionTabList[prescriptionTmp.id];
        //        msgFeedback = "la prescription " + nomPrescription + " a bien été  choisie";
        //    }
        //    else
        //    {
        //        msgFeedback = "La prescription est non choisise";
        //    }

        //    MessageDialog selectConsigne = new MessageDialog(msgFeedback, "Selection de la prescription");
        //    await selectConsigne.ShowAsync();
        //}

        private void tgb_Verbale_Click(object sender, RoutedEventArgs e)
        {
            if (tgb_Verbale.IsChecked == true)
            {

                prescTabulaire.Opacity = 0;
                listViewSelectPresc.Opacity = 1;
                tgb_tabulaire.IsChecked = false;
            }
        }

        private void tgb_tabulaire_Click(object sender, RoutedEventArgs e)
        {
            if (tgb_tabulaire.IsChecked == true)
            {

                prescTabulaire.Opacity = 1;
                listViewSelectPresc.Opacity = 0;
                tgb_Verbale.IsChecked = false;
            }

        }
        //*******************search**********


        //private void searchPrescription_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    if (!String.IsNullOrEmpty(searchPrescription.Text))
        //    {
        //        gridViewPresc.ItemsSource = null;
        //        gridViewPresc.ItemsSource = newListPrescriptions.FindAll(listConsigne => listConsigne.nom.Contains(searchPrescription.Text));
        //    }
        //    else
        //    {
        //        gridViewPresc.ItemsSource = null;
        //        gridViewPresc.ItemsSource = newListPrescriptions;
        //    }
        //}


        //public void showSharingSpaceInfo()
        //{
        //    idSharingSpace.Clear();
        //    // foreach (CheckBox checkbox in checkBoxListSharingSpace)
        //    for (int i = 0; i < checkBoxListSharingSpace.Count; i++)
        //    {
        //        if (checkBoxListSharingSpace[i].IsChecked == true)
        //        {
        //            string[] split = elementList[i].Split('#');
        //            idSharingSpace.Add(split[1]);
        //        }
        //    }
        //}

        //*********************Prescription configuration

        public async void loadExperienceConf()
        {
            initialiserAffichagePrescription();

            if (App.authentification)
            {

                sampleFileExperienceConf = await App.storageFolder.GetFileAsync(App.logIn + "-ExperienceConf.json");
                strJsonExpConf = await FileIO.ReadTextAsync(sampleFileExperienceConf);
                //strJsonExpConf = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-ExperienceConf.json");
                ListExperiencesConfTmp = JsonConvert.DeserializeObject<List<ExperienceConf>>(strJsonExpConf);
                await FileIO.WriteTextAsync(sampleFileExperienceConf, JsonConvert.SerializeObject(ListExperiencesConfTmp));
            }
            else
            {
                //ListExperiencesConfTmp = new List<ExperienceConf>();
                sampleFileExperienceConf = await App.storageFolder.GetFileAsync("ExperienceConf.json");
                strJsonExpConf = await FileIO.ReadTextAsync(sampleFileExperienceConf);
                ListExperiencesConfTmp = JsonConvert.DeserializeObject<List<ExperienceConf>>(strJsonExpConf);
            }

            if (ListExperiencesConfTmp != null)
            {
                indexExp = ListExperiencesConfTmp[ListExperiencesConfTmp.Count - 1].id + 1;
            }
            else
            {
                indexExp = 0;
            }

            ListViewExpConf.ItemsSource = null;
            ListViewExpConf.ItemsSource = ListExperiencesConfTmp;
            //newListPrescriptions.Reverse();                
        }
    }

}
