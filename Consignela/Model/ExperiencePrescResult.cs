﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    class ExperiencePrescResult
    {
        [JsonProperty("experimentateur")]
        public String experimentateur { get; set; }
        [JsonProperty("date")]
        public String date { get; set; }
        [JsonProperty("Code_Participant")]
        public String Code_Participant { get; set; }
        [JsonProperty("type")]
        public String type { get; set; }
        [JsonProperty("utilisation")]
        public String utilisation { get; set; }
        [JsonProperty("pilulier")]
        public String pilulier { get; set; }
        [JsonProperty("format")]
        public String format { get; set; }
        [JsonProperty("mode")]
        public String mode { get; set; }
        [JsonProperty("navigation")]
        public String navigation { get; set; }
        [JsonProperty("etapes")]
        public List<Etape> etapes { get; set; }
        [JsonProperty("prescription")]
        public PrescriptionTab prescription { get; set; }
        [JsonProperty("Resultat")]
        public Resultat Resultat { get; set; }


    }
}
