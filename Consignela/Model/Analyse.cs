﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    class Analyse
    {
        [JsonProperty("prescriptionID")]
        public int prescriptionID { get; set; }
        [JsonProperty("momentJour")]
        public List<MomentJour> momentJour { get; set; }
    }
}
