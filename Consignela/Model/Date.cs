﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    public class Date
    {
        public String day { get; set; }
        public String month { get; set; }
        public String year { get; set; }
        public String time { get; set; }

        public String monthToStr ()
        {
            switch (month)
            {
                case "1":
                    return "Janvier";
                case "2":
                    return "Février";
                case "3":
                    return "Mars";
                case "4":
                    return "Avril";
                case "5":
                    return "Mai";
                case "6":
                    return "Juin";
                case "7":
                    return "Juillet";
                case "8":
                    return "Aout";
                case "9":
                    return "Septembre";
                case "10":
                    return "Octobre";
                case "11":
                    return "Novembre";
                case "12":
                    return "Décembre";
                    
            }
            return "mois";
            
        }
    }

}
