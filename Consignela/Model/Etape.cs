﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    class Etape
    {
        [JsonProperty("Temps-ecoule")]
        public int Temps_ecoule { get; set; }
        [JsonProperty("Action")]
        public String Action { get; set; }
        [JsonProperty("Codage-evenement")]
        public String Codage_evenement { get; set; }
    }
}
