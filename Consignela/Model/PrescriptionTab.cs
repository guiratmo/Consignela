﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    public class PrescriptionTab
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("numero")]
        public int numero { get; set; }
        [JsonProperty("nom")]
        public String nom { get; set; }
        [JsonProperty("momentJour")]
        public List<MomentJour> momentJour { get; set; }
    }
}
