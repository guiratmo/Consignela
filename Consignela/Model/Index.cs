﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Consignela.Model;
using Windows.Storage;

namespace Consignela.Model
{
    class Index
    {
        static JObject jsonNote;
        static string user, jsonFromUrl;
        static List<CheckBox> checkBoxList;
        static string[] ArrayForSharingSpace, jsonArrayIndexTab;

        static List<string> nom_cpt, prefixOrConceptPrefix, PrefixList, ConceptPrefixList, distributor, SharingSpaceList, indexOfSelectedCHeckBox, nouveauList;

        static List<string> index;
        public static List<string> getnom_cpt()
        {
            return nom_cpt;
        }
        public static void getIndex()
        {
            nom_cpt = new List<string>();
            index = new List<string>();
            prefixOrConceptPrefix = new List<string>();
            ConceptPrefixList = new List<string>();
            PrefixList = new List<string>();
            PrefixList = SharingSpace.getPrefixList();
            ConceptPrefixList = SharingSpace.getConceptPrefixList();
            jsonFromUrl = SharingSpace.getJsonFromUrl("http://memorae.hds.utc.fr/api/" + ConceptPrefixList[0] + "/graph");
            //    System.Diagnostics.Debug.Write("\njsonfrom Url  " + jsonFromUrl);
            JObject jsonObject = JObject.Parse(jsonFromUrl);
            foreach (JProperty p in jsonObject.Properties())
            {
                if (p.Name.Equals("concepts"))
                {

                    JArray jsonArrayConcept = JArray.Parse(p.Value.ToString());
                    //   System.Diagnostics.Debug.Write("\n jsonArrayConcept  " + jsonArrayConcept);

                    foreach (JObject o in jsonArrayConcept.Children<JObject>())
                    {
                        foreach (JProperty pp in o.Properties())
                        {
                            if (pp.Name.Equals("nom_cpt"))
                            {
                                string x = pp.Value.ToString();
                                nom_cpt.Add(x);
                            }

                            if (pp.Name.Equals("type_cpt"))
                            {
                                string y = pp.Value.ToString();
                                if (y.Equals("0"))
                                {
                                    index.Add(ConceptPrefixList[0]);
                                    prefixOrConceptPrefix.Add(ConceptPrefixList[0]);
                                    foreach (JProperty ppp in o.Properties())
                                    {
                                        if (ppp.Name.Equals("id_cpt_second"))
                                            index.Add(ppp.Value.ToString());
                                    }


                                }
                                else
                                {
                                    if (y.Equals("1"))
                                    {

                                        index.Add(PrefixList[0]);
                                        prefixOrConceptPrefix.Add(ConceptPrefixList[0]);
                                        foreach (JProperty ppp in o.Properties())
                                        {
                                            if (ppp.Name.Equals("id_cpt_second"))
                                                index.Add(ppp.Value.ToString());
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public static async void showInfo(List<CheckBox> listCheckBoxTmp,StorageFile sampleFile,List<string> idSharingSpaceClass)
        {

            nouveauList = new List<string>();
            List<string> idSharingSpace = new List<string>();
            user = "amc2:" + SharingSpace.getElementFromCheckAccess();
            distributor = new List<String>();

            PrefixList = new List<String>();
            PrefixList = SharingSpace.getPrefixList();
            JArray jsonArrayIndex = new JArray();
            indexOfSelectedCHeckBox = new List<String>();

            string ElementFromCheckAccessList = SharingSpace.getElementFromCheckAccess();
            distributor.Add(user);
            checkBoxList = new List<CheckBox>();
            checkBoxList = listCheckBoxTmp;
            for (int i = 0; i < checkBoxList.Count; i++)
            {
                if (checkBoxList[i].IsChecked == true)
                {

                     System.Diagnostics.Debug.Write("\nName: " + nom_cpt[i] + "\n");
                    // sharingSpacePage.showSharingSpaceInfo();
                    string[] split = index[2 * i + 1].Split('#');

                    indexOfSelectedCHeckBox.Add(index[2 * i] + ":" + split[1]);
                    nouveauList.Add(index[2 * i]);

                    JObject jsonObjectIndex = new JObject(new JProperty(index[2 * i], index[2 * i + 1]));
                    //  System.Diagnostics.Debug.Write("jsonObjectIndex "+jsonObjectIndex);

                    jsonArrayIndex.Add(jsonObjectIndex);
                    //    System.Diagnostics.Debug.Write("jsonArrayIndex " + jsonArrayIndex);
                }
            }
            /******************************************** we are here ***********************************/
            idSharingSpace = idSharingSpaceClass;

            ArrayForSharingSpace = new string[idSharingSpace.Count];
            for (int h = 0; h < idSharingSpace.Count; h++)
            {

                ArrayForSharingSpace[h] = (string)PrefixList[0] + ":" + idSharingSpace[h];

            }

            jsonArrayIndexTab = new string[indexOfSelectedCHeckBox.Count];
            for (int j = 0; j < indexOfSelectedCHeckBox.Count; j++)
            {
                jsonArrayIndexTab[j] = indexOfSelectedCHeckBox[j];
            }
   

            for (int i = 0; i < nouveauList.Count; i++)
            {
               // System.Diagnostics.Debug.WriteLine(nouveauList[i]);
               await allInformations("http://memorae.hds.utc.fr/api/" + nouveauList[i] + "/note", sampleFile);

            }
            // System.Diagnostics.Debug.Write("retour " + retourPost);

        }

        public static async Task<string> allInformations(string url,StorageFile sampleFile)
        {

            try
            {
                List<string> EmptyList = new List<string>();
                // string a = MainWindow.getNomDuFichierAEnvoyer();
                string[] title = sampleFile.Name.Split('.');
                jsonNote = new JObject(new JProperty("distributor", distributor),
                    new JProperty("element", SharingSpace.getElementFromCheckAccess()),
                    new JProperty("sharingspace", ArrayForSharingSpace),
                    new JProperty("title", title[0]),
                    new JProperty("date", DateTime.Now.ToString("dd-MM-yyyy")),
                    new JProperty("body", sampleFile.Name),
                    new JProperty("index", indexOfSelectedCHeckBox),
                    new JProperty("tag", indexOfSelectedCHeckBox),
                    new JProperty("creator", user),
                    new JProperty("supportDocumnet", null),
                    new JProperty("type", "note"),
                    new JProperty("contained_by", null),
                    new JProperty("isbookmarked", null)
                   );


                await MainMenu.Post(url, jsonNote.ToString());


                return "";


            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }



        public List<String> getNom_cpt()
        {
            return nom_cpt;
        }

        public String getUser()
        {
            return user;
        }
        public List<String> getDistributor()
        {
            return distributor;
        }


    }

}

