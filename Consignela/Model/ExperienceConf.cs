﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    class ExperienceConf
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("nom")]
        public String nom { get; set; }
        [JsonProperty("dateTime")]
        public DateTime dateTime { get; set; }
        [JsonProperty("listeConsigne")]
        public ListConsignes listeConsigne { get; set; }
        [JsonProperty("prescription")]
        public Prescription prescription { get; set; }
        [JsonProperty("prescriptionTab")]
        public PrescriptionTab prescriptionTab { get; set; }
    }
}
