﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    class AnalyseExperience
    {
        [JsonProperty("experimentateur")]
        public String experimentateur { get; set; }
        [JsonProperty("Code_Participant")]
        public String Code_Participant { get; set; }
        [JsonProperty("date")]
        public String date { get; set; }
        [JsonProperty("pilulier")]
        public String pilulier { get; set; }
        [JsonProperty("format")]
        public String format { get; set; }
        [JsonProperty("Analyse")]
        public Analyse Analyse { get; set; }
    }
}
