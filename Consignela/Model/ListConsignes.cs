﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Consignela.Model;

namespace Consignela
{
    
    public class ListConsignes
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("nom")]
        public String nom { get; set; }
        [JsonProperty("consignes")]
        public List<Consigne> consignes = new List<Consigne>();
        [JsonProperty("date")]
        public String date { get; set; }
        [JsonProperty("dateTime")]
        public DateTime dateTime { get; set; }
        [JsonProperty("dateLettre")]
        public Date dateLettre { get; set; }

    }
}
