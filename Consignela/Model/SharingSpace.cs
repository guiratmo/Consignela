﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Storage.Pickers;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.IO;

namespace Consignela.Model
{

    class SharingSpace
    {
        static string element, jsonFromCheckAccess ;
       
        static string urlSpace = "http://memorae.hds.utc.fr/api/";
        static List<string> elementFromCheckAccessList, jsonArrayGroupList, jsonArrayOrganizationList, jsonObjectURI, prefixList, conceptPrefixList, IdList, nameListSharingSpace, elementListSharingSpace;
        static string x="";
        static JArray jsonArray;
        public static void getSharingSapce()
        {
            elementFromCheckAccessList = new List<string>();
            jsonObjectURI = new List<string>();
            prefixList = new List<string>();
            conceptPrefixList = new List<string>();
            jsonArrayGroupList = new List<string>();
            jsonArrayOrganizationList = new List<string>();
            jsonArray = new JArray();
            IdList = new List<string>();
            IdList.Clear();
            elementFromCheckAccessList.Clear();
            jsonObjectURI.Clear();
            prefixList.Clear();
            conceptPrefixList.Clear();
            jsonArrayGroupList.Clear();
            jsonArrayOrganizationList.Clear();
            jsonArray.Clear();
           
               jsonFromCheckAccess = App.txtresponseLogin;
            
          
           //   System.Diagnostics.Debug.Write("json "+jsonFromCheckAccess);
             jsonArray = JArray.Parse(jsonFromCheckAccess);
            //  System.Diagnostics.Debug.Write(json);
            foreach (JObject o in jsonArray.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("element"))
                    {
                        element = (string)p.Value;
                        elementFromCheckAccessList.Add(element);
                        //System.Diagnostics.Debug.Write("element " + element);

                    }

                    if (p.Name.Equals("group"))
                    {
                        JArray jsonArrayGroup = (JArray)p.Value;
                        for (int i = 0; i < jsonArrayGroup.Count; i++)
                        {
                            jsonArrayGroupList.Add(jsonArrayGroup[i].ToString());
                            //    System.Diagnostics.Debug.Write("\n group" + i + ": " + jsonArrayGroupList[i]);
                        }
                        //jsonArrayGroupList.Add(jsonArrayGroup.ToString());




                    }
                    if (p.Name.Equals("organization"))
                    {
                        JArray jsonArrayOrganization = (JArray)p.Value;

                        for (int i = 0; i < jsonArrayOrganization.Count; i++)
                        {
                            jsonArrayOrganizationList.Add(jsonArrayOrganization[i].ToString());
                            //       System.Diagnostics.Debug.Write("\n organization"+i+": " + jsonArrayOrganizationList[i]);
                        }

                        //  System.Diagnostics.Debug.Write("\norganization " + jsonArrayOrganization);
                        foreach (JObject oo in jsonArrayOrganization.Children<JObject>())
                        {
                            foreach (JProperty pp in oo.Properties())
                            {

                                if (pp.Name.Equals("name") && pp.Value.ToString() != "MEMORAe")
                                {

                                    // System.Diagnostics.Debug.Write("\nname value " + pp.Value);

                                    foreach (JProperty ppp in oo.Properties())
                                    {
                                        if (ppp.Name.Equals("uri"))
                                        {
                                           System.Diagnostics.Debug.Write("\n uri value " + ppp.Value);
                                            jsonObjectURI.Add(ppp.Value.ToString());

                                            // System.Diagnostics.Debug.Write("\n jsonObjectURI " +uri);

                                        }

                                        if (ppp.Name.Equals("prefix"))
                                        {
                                            //System.Diagnostics.Debug.Write("\n prefix " + ppp.Value);
                                            prefixList.Add(ppp.Value.ToString());
                                            //System.Diagnostics.Debug.Write("\n prefixList " + prefixList[0]);
                                        }
                                        if (ppp.Name.Equals("conceptPrefix"))
                                        {
                                            //System.Diagnostics.Debug.Write("\n conceptPrefix " + ppp.Value);
                                            conceptPrefixList.Add(ppp.Value.ToString());
                                            //  System.Diagnostics.Debug.Write("\n conceptPrefixList " + conceptPrefixList[0]);
                                        }


                                    }


                                }
                            }
                        }

                    }





                }

            }
           
              x = jsonObjectURI[0];

            x = x.Replace("<", "").Replace(">", "");

            //  System.Diagnostics.Debug.Write("\nx= " + x);
            for (int i = 0; i < jsonArrayGroupList.Count; i++)
            {
                string y = jsonArrayGroupList[i];



                if (y.Contains(x))
                {
                    string[] words = y.Split('#');
                    IdList.Add(words[1]);
                    //    System.Diagnostics.Debug.Write("\nid from groups " + words[1]);
                }

            }
            getNamesFromGroupUrl();






            //UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), filePath);
            //this.Frame.Navigate(typeof(sharingSpace), null);

        }
        public static void getNamesFromGroupUrl()
        {
            nameListSharingSpace = new List<string>();
            elementListSharingSpace = new List<string>();
            string json = getJsonFromUrl(urlSpace+prefixList[0]+"/space");
            JArray jsonArray = JArray.Parse(json);
            //  System.Diagnostics.Debug.Write(json);
            foreach (JObject o in jsonArray.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("group"))

                    {
                        foreach (string id in IdList)
                        {

                            if (!p.Value.Equals("null") && p.Value.ToString().Contains(id))

                            {

                                foreach (JProperty pp in o.Properties())
                                {
                                    if (pp.Name.Equals("name"))
                                    {
                                        if (pp.Value.ToString() != "null")
                                        {
                                            // System.Diagnostics.Debug.Write("\n name= "+ pp.Value);
                                            nameListSharingSpace.Add(pp.Value.ToString());





                                        }
                                    }
                                    if (pp.Name.Equals("element"))
                                    {

                                        //   System.Diagnostics.Debug.Write("\n element= " + pp.Value);
                                        elementListSharingSpace.Add(pp.Value.ToString());

                                    }

                                }
                            }
                        }

                    }


                }
            }


        }

        public static int getIdlIstCount()
        {
            return IdList.Count;
        }

      


        public static List<String> getNameListSharingSpace()
        {
            return nameListSharingSpace;
        }
        public static List<String> getElementListSharingSpace()
        {
            return elementListSharingSpace;
        }


        public static string getJsonFromUrl(string url)
        {
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            var response = client.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                // by calling .Result you are performing a synchronous call
                var responseContent = response.Content;

                // by calling .Result you are synchronously reading the result
                string responseString = responseContent.ReadAsStringAsync().Result;

                return responseString;
            }
            else
            {
                return "error";
            }

        }


        public List<string> getJsonObjectURI()
        {
            return jsonObjectURI;
        }
        public static List<string> getPrefixList()
        {
            return prefixList;
        }
        public static string getElementFromCheckAccess()
        {
            String[] parts = elementFromCheckAccessList[0].Split('#');
            return parts[1];
        }
        public static List<string> getConceptPrefixList()
        {
            return conceptPrefixList;
        }

        public static async Task<string> UploadFile(byte[] file, Uri url, string filePath)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                MultipartFormDataContent form = new MultipartFormDataContent();
                var content = new StreamContent(new MemoryStream(file));
                form.Add(content, "file", filePath);
                var response = await client.PostAsync(url, form);
                return await response.Content.ReadAsStringAsync();
            }
        }
    }



}
