﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Consignela.Model;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;

namespace Consignela
{
    
    public class Consigne
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("description")]
        public String description { get; set; }
        [JsonProperty("img_path")]
        public String img_path { get; set; }
        [JsonProperty("imgBitmap")]
        public BitmapImage imgBitmap { get; set; }
        [JsonProperty("imgNom")]
        public String imgNom { get; set; }



    }
}
