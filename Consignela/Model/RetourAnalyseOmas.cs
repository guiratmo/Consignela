﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    class RetourAnalyseOmas
    {
        public string experimentateur { get; set; }
        public string Code_Participant { get; set; }
        public string date { get; set; }
        public string pilulier { get; set; }
        public string format { get; set; }
        public Analyse Analyse { get; set; }
    }
}
