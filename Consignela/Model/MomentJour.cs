﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela.Model
{
    public class MomentJour
    {

        [JsonProperty("moment")]
        public String moment { get; set; }
        [JsonProperty("jour")]
        public String jour { get; set; }
        [JsonProperty("abs")]
        public int abs { get; set; }
        [JsonProperty("coord")]
        public int coord { get; set; }
        [JsonProperty("listMedicaments")]
        public List<Medicament> listMedicaments { get; set; } = new List<Medicament>();

    }
}
