﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Consignela.Model;
using Windows.UI;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Param_Exp_Page : Page
    {
       

        public Param_Exp_Page()
        {
            this.InitializeComponent();
            this.CheckConsigne();
            this.CheckPrescription();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }

        //private void back_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.Frame.CanGoBack)
        //    {
        //        this.Frame.GoBack();
        //    }
        //}

        private void btn_PrescSelect_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Select_Presc_Page), null);
        }

        private void btn_ConsiSelect_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Select_Consi_Page), null);
        }
        public void CheckConsigne()
        {
            if(App.consignesSelect != null)
            {
                tb_ConsiSelect.Text = App.consignesSelect.nom;
                tb_ConsiSelect.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
                stroke_SelectCons.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            }
           
        }
        public void CheckPrescription()
        {
            if (App.getPrescriptionSelect() != null)
            {
                tb_PrescSelect.Text = App.getPrescriptionSelect().nom;
                tb_PrescSelect.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
                stroke_SelectPresc.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            }

        }
    }
}
