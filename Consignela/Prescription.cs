﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Consignela.Model;

namespace Consignela
{

    public class Prescription
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("date")]
        public Date date { get; set; }
        [JsonProperty("dateTime")]
        public DateTime dateTime { get; set; }
        [JsonProperty("nom")]
        public String nom { get; set; }
        [JsonProperty("medicaments")]
        public List<Medicament> medicaments = new List<Medicament>();
    }
}
