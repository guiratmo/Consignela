﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consignela
{
    class ListeConsigne
    {
        public int id { get; set; }
        public Consigne consigne { get; set; }
        public String nom { get; set; }
    }
}
