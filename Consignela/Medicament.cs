﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consignela.Model;

namespace Consignela
{
    public class Medicament
    {
        public String nom { get; set; }
        public int nbr { get; set; }
        public String regulier { get; set; }
        public int nbrReg { get; set; }
        public String momentReg { get; set; }
        public String jourRD { get; set; }
        public String jourRF { get; set; }
        public String irregulier { get; set; }
        public int nbrIrreg { get; set; }
        public String momentIrreg { get; set; }
        public String jourIrreg { get; set; }
        public String descrReg { get; set; }
        public String descrIrreg { get; set; }
        public MediImages imageMedicament { get; set; }
        public int nbrTab { get; set; }
        public List<Prise> Prises { get; set; }
        public String analyseResult { get; set; }
        public int nbrAnalyse { get; set; }
        public int nbrResultat { get; set; }
        public int nbrPrescription { get; set; }
    }

    
}
