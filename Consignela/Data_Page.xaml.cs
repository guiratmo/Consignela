﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;
using Consignela.Model;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.Web.Http;
using Newtonsoft.Json;
using Windows.Storage;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{

    //public class Notes
    //{
    //    public String Name { get; set; }
    //    public int Amount { get; set; }
    //}
    public class ExpFormat
    {
        public string format { get; set; }
        public int number { get; set; }
    }
    public class Exp
    {
        public string type { get; set; }
        public int number { get; set; }
    }
    public sealed partial class Data_Page : Page
    {
        List<AnalyseExperience> ListAnalyseExp;
        internal List<Medicament> tabListMedicaments;
        List<Medicament> newMedicament;
        String jsonOmasExpResultat;
        String TxtResponse;
        string user, jsonFromUrl;
        static string stringForSharingSpace;
        List<string> typeList, numberList, avgTimeList, formatList;
        internal StorageFile sampleFile;

        List<string> nom_cpt, prefixOrConceptPrefix, PrefixList, ConceptPrefixList, distributor, SharingSpaceList, indexOfSelectedRadioButton;

        static List<string> index = new List<string>();
        List<string> nameList = new List<string>();
        List<string> elementList = new List<string>();
        static List<string> idSharingSpace = new List<string>();
        List<RadioButton> radioButtonList = new List<RadioButton>();
        //internal List<Notes> lstNotes = new List<Notes>();
        static string jsonFromOmas, jsonOmasExpParType, jsonOmasRecupNbre, jsonOmasAvgTime;
        internal List<GridView> gridViewTabMedicament = new List<GridView>();
        internal AnalyseExperience AnalyseExperienceTmp , AnalyseExperienceTmp1;
        internal List<String> CodeParicipants = new List<string>();
        internal List<Analyse> Analyses = new List<Analyse>();

        public Data_Page()
        {
            this.InitializeComponent();          
            

        }
        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            prescTabulaire.Visibility = Visibility.Collapsed;
            //LoadChartContents();
            loadGridView();

            if (!authenPopup.IsOpen && App.authentification == false)
            {
                authenPopup.IsOpen = true;
                pageContent.Opacity = 0.5;
                nbr_exp.IsEnabled = false;
                temps_ecoule.IsEnabled = false;
                nbr_Exp_format.IsEnabled = false;

            }
            else
            {

                //if (!formulairePopup.IsOpen) { formulairePopup.IsOpen = true; }
            }


        }
        private async void loadExperiences()
        {
            JArray memorae_data = new JArray(
            new JObject(new JProperty("document-name", "brahim-experiences.json"))

                    );

            JArray pattern = new JArray(new JObject(new JProperty("type", ""), new JProperty("number", "")));
            JObject pillbox_data = new JObject();
            JObject JsonToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("action", "analyse-experience"),
                new JProperty("pillbox-data", pillbox_data),
                new JProperty("pattern", pattern),
                 new JProperty("timeout", 200),
                 new JProperty("language", "French"),
                 new JProperty("contract-net-strategy", "take-first-answer")


                );

            //System.Diagnostics.Debug.Write(JsonToOMAS.ToString());
            //sampleFile = await App.storageFolder.GetFileAsync("Test.json");
            //jsonOmasExpResultat = await FileIO.ReadTextAsync(sampleFile);

            await Data_Page.PostOmas("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", JsonToOMAS.ToString());
            jsonOmasExpResultat = jsonFromOmas;
            System.Diagnostics.Debug.Write(jsonOmasExpResultat);

            ListAnalyseExp = JsonConvert.DeserializeObject<List<AnalyseExperience>>(jsonOmasExpResultat);
            foreach(AnalyseExperience analyseExp in ListAnalyseExp)
            {
                CodeParicipants.Add(analyseExp.Code_Participant);
                Analyses.Add(analyseExp.Analyse);
            }

          

            listViewExp.ItemsSource = null;
            listViewExp.ItemsSource = ListAnalyseExp;
        }
        private void LoadChartContents()
        {
            typeList = new List<string>();
            numberList = new List<string>();
            JArray jsonArrayFromOmas = JArray.Parse(jsonOmasRecupNbre);
            foreach (JObject o in jsonArrayFromOmas.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("type"))
                    {
                        typeList.Add(p.Value.ToString());
                    }
                    if (p.Name.Equals("number"))
                    {
                        numberList.Add(p.Value.ToString());
                    }


                }
            }
            List<Exp> lstSource = new List<Exp>();
            for (int i = 0; i < typeList.Count; i++)
            {
                lstSource.Add(new Exp() { type = typeList[i], number = int.Parse(numberList[i]) });

                //  (ColumnChart.Series[0] as ColumnSeries).ItemsSource = lstSource;

            }
              (PieChart.Series[0] as PieSeries).ItemsSource = lstSource;
        }
        //private void LoadChartContents()
        //{
        //    //List<Notes> lstNotes = new List<Notes>();
        //    //lstNotes.Add(new Notes() { Name = "Osama", Amount = 20 });
        //    //lstNotes.Add(new Notes() { Name = "Marie Hélene", Amount = 50 });
        //    //lstNotes.Add(new Notes() { Name = "Mohamed", Amount = 10 });
        //    //lstNotes.Add(new Notes() { Name = "Brahim", Amount = 20 });
        //    (noteColumnCharte.Series[0] as ColumnSeries).ItemsSource = lstNotes;
        //    (notePieCharte.Series[0] as PieSeries).ItemsSource = lstNotes;
        //}
        private void OnLayoutUpdatedFormulaire(object sender, object e)
        {
            if (formulairePopupChild.ActualWidth == 0 && formulairePopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.formulairePopup.HorizontalOffset;
            double ActualVerticalOffset = this.formulairePopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - formulairePopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - formulairePopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.formulairePopup.HorizontalOffset = NewHorizontalOffset;
                this.formulairePopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void quitformulairePopup_Click(object sender, RoutedEventArgs e)
        {
            if (formulairePopup.IsOpen){ formulairePopup.IsOpen = false;}
        }

        private void indexSave_Click(object sender, RoutedEventArgs e)
        {
            showInfo();
            progressRing.IsActive = true;  
            if (indexPopup.IsOpen) { indexPopup.IsOpen = false; }
        }

        private void envoieFormulaire_Click(object sender, RoutedEventArgs e)
        {
            if (formulairePopup.IsOpen) { formulairePopup.IsOpen = false; }
            System.Diagnostics.Debug.Write(dateDebut.Date.DateTime);
            Charts.Visibility = Visibility.Visible;

            SharingSpace.getSharingSapce();

            int a = SharingSpace.getIdlIstCount();
            //System.Diagnostics.Debug.Write("\na= " + a);

            nameList = SharingSpace.getNameListSharingSpace();
            elementList = SharingSpace.getElementListSharingSpace();


            for (int i = 0; i < a; i++)
            {
                //Create the button
                RadioButton radioButton = new RadioButton();
                radioButton.Content = nameList[i];
                radioButton.Margin = new Thickness(20, 20, 0, 0);
                radioButton.Name = "sharingSpace" + i;

                radioButtonList.Add(radioButton);
                SharingSpaceListview.ItemsSource = radioButtonList;
                

            }
            if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }
        }

       
        private void OnLayoutUpdatedIndex(object sender, object e)
        {
            if (indexPopupChild.ActualWidth == 0 && indexPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.indexPopup.HorizontalOffset;
            double ActualVerticalOffset = this.indexPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - indexPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - indexPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.indexPopup.HorizontalOffset = NewHorizontalOffset;
                this.indexPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void OnLayoutUpdatedSharing(object sender, object e)
        {
            if (sharingSpacePopupChild.ActualWidth == 0 && sharingSpacePopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.sharingSpacePopup.HorizontalOffset;
            double ActualVerticalOffset = this.sharingSpacePopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - sharingSpacePopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - sharingSpacePopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.sharingSpacePopup.HorizontalOffset = NewHorizontalOffset;
                this.sharingSpacePopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private async void connecter_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login.Text + "\"" + ",\"pass\":" + "\"" + pass.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login.Text;
                App.password = pass.Password;
                App.authentification = true;

                if (authenPopup.IsOpen)
                {
                    authenPopup.IsOpen = false;
                    pageContent.Opacity = 1;
                    nbr_exp.IsEnabled = true;
                    temps_ecoule.IsEnabled = true;
                    nbr_Exp_format.IsEnabled = true;
                    App.verifDataPage = true;
                    MainMenu.rootFrame.Navigate(typeof(MainMenu));
                    //MainMenu.profilNom.Text = "hhhhhhhhh";


                }

                //if (!formulairePopup.IsOpen) { formulairePopup.IsOpen = true; }

            }
            else
            {
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
            
        }
        public async Task Post(string website, string args)
        {

            //init client
            Windows.Web.Http.HttpClient hc = new Windows.Web.Http.HttpClient();
            //send post request
            Windows.Web.Http.HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            TxtResponse = await hrm.Content.ReadAsStringAsync();
            App.txtresponseLogin = TxtResponse;
            //System.Diagnostics.Debug.Write(TxtResponse);
            hc.Dispose();
        }

        private async void analyseResultatExp_Click(object sender, RoutedEventArgs e)
        {
            loadExperiences();
            stackPanelOmasButtons.Visibility = Visibility.Collapsed;
            listViewExp.Visibility = Visibility.Visible;
            prescTabulaire.Visibility = Visibility.Visible;
        }

        private void listViewExp_Tapped(object sender, TappedRoutedEventArgs e)
        {
            AnalyseExperienceTmp = (AnalyseExperience)listViewExp.SelectedItem;
            //foreach(AnalyseExperience AnalyseExp in ListAnalyseExp)
            //{
            //    if(AnalyseExp == AnalyseExperienceTmp)
            //    {
            //        newMedicament = AnalyseExp.Analyse.momentJour[0].;
            //    }
            //}

            for (int i = 0; i < 21; i++)
            {
                if ((Analyses[listViewExp.SelectedIndex].momentJour[i].listMedicaments) != null)
                {
                    tabListMedicaments = Analyses[listViewExp.SelectedIndex].momentJour[i].listMedicaments;
                    gridViewTabMedicament[i].ItemsSource = null;
                    gridViewTabMedicament[i].ItemsSource = tabListMedicaments;
                    if (i == 20)
                        System.Diagnostics.Debug.WriteLine(tabListMedicaments);
                }
            }
        }      

        private void backAnalyse_Click(object sender, RoutedEventArgs e)
        {
            listViewExp.Visibility = Visibility.Collapsed;
            stackPanelOmasButtons.Visibility = Visibility.Visible;
            prescTabulaire.Visibility = Visibility.Collapsed;
        }

        private async void nbr_exp_Click(object sender, RoutedEventArgs e)
        {
            chargerDonnee.IsActive = true;
            PieChart.Visibility = Visibility.Visible;
            ColumnChart.Visibility = Visibility.Collapsed;
            AverageChart.Visibility = Visibility.Collapsed;

            JArray memorae_data = new JArray(
 new JObject(new JProperty("document-name", "brahim-experiences.json"))

 );


            JArray pattern = new JArray(new JObject(new JProperty("type", "type-data")), new JObject(new JProperty("number", "number-data")));

            JObject JsonToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("action", "number-exp-patient-type"),
                new JProperty("pattern", pattern),
                 new JProperty("timeout", 200),
                 new JProperty("language", "English"),
                 new JProperty("contract-net-strategy", "take-first-answer")


                );

            System.Diagnostics.Debug.Write(JsonToOMAS.ToString());

            await PostOmas("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", JsonToOMAS.ToString());

            jsonOmasRecupNbre = jsonFromOmas;
            System.Diagnostics.Debug.Write(jsonOmasRecupNbre);
            LoadChartContents();
            chargerDonnee.IsActive = false;
        }
        public static async Task PostOmas(string website, string args)
        {
            jsonFromOmas = "";
            //init client
            Windows.Web.Http.HttpClient hc = new Windows.Web.Http.HttpClient();
            //send post request

            Windows.Web.Http.HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            //System.Diagnostics.Debug.Write(await hrm.Content.ReadAsStringAsync());
            jsonFromOmas = await hrm.Content.ReadAsStringAsync();
            hc.Dispose();
        }
        private void LoadChartContentsTemps()
        {
            typeList = new List<string>();
            avgTimeList = new List<string>();
            JArray jsonArrayFromOmas = JArray.Parse(jsonOmasAvgTime);

            // retour =>   [{"type":"AG", "avg-time":382277/6},{"type":"PK", "avg-time":18595/2},{"type":"CO", "avg-time":1055748},{"type":"AU", "avg-time":0}]
            //[{"type":"AG", "avg-time":63712},{"type":"PK", "avg-time":9297},{"type":"CO", "avg-time":1055748},{"type":"AU", "avg-time":0}]
            System.Diagnostics.Debug.Write(jsonArrayFromOmas);
            foreach (JObject o in jsonArrayFromOmas.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("type"))
                    {
                        typeList.Add(p.Value.ToString());
                    }
                    if (p.Name.Equals("avg-time"))
                    {
                        avgTimeList.Add(p.Value.ToString());
                    }


                }
            }
         

            List<Exp> lstSource = new List<Exp>();
            for (int i = 0; i < typeList.Count; i++)
            {
                lstSource.Add(new Exp() { type = typeList[i], number = int.Parse(avgTimeList[i]) });

                //  (ColumnChart.Series[0] as ColumnSeries).ItemsSource = lstSource;

            }
             (AverageChart.Series[0] as ColumnSeries).ItemsSource = lstSource;


        }
        private async void temps_ecoule_Click(object sender, RoutedEventArgs e)
        {
            chargerDonnee.IsActive = true;
            PieChart.Visibility = Visibility.Collapsed;
            ColumnChart.Visibility = Visibility.Collapsed;
            AverageChart.Visibility = Visibility.Visible;

            JArray memorae_data = new JArray(
  new JObject(new JProperty("document-name", "brahim-experiences.json"))

  );


            JArray pattern = new JArray(new JObject(new JProperty("type", "type-data")), new JObject(new JProperty("avg-time", "avg-time-data")));

            JObject JsonToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("action", "time-show-pillbox-patient-type"),
                new JProperty("pattern", pattern),
                 new JProperty("timeout", 200),
                 new JProperty("language", "English"),
                 new JProperty("contract-net-strategy", "take-first-answer")


                );

            System.Diagnostics.Debug.Write(JsonToOMAS.ToString());

            await PostOmas("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", JsonToOMAS.ToString());
            jsonOmasAvgTime = jsonFromOmas;
            LoadChartContentsTemps();
            chargerDonnee.IsActive = false;
        }

        private async void nbr_Exp_format_Click(object sender, RoutedEventArgs e)
        {
            chargerDonnee.IsActive = true;
            PieChart.Visibility = Visibility.Collapsed;
            ColumnChart.Visibility = Visibility.Visible;
            AverageChart.Visibility = Visibility.Collapsed;

            JArray memorae_data = new JArray(
 new JObject(new JProperty("document-name", "brahim-experiences.json"))

 );


            JArray pattern = new JArray(new JObject(new JProperty("format", "format-data"), new JProperty("number", "number-data")));

            JObject JsonToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("action", "number-exp-format-prescrip"),
                new JProperty("pattern", pattern),
                 new JProperty("timeout", 200),
                 new JProperty("language", "English"),
                 new JProperty("contract-net-strategy", "take-first-answer")


                );

            System.Diagnostics.Debug.Write(JsonToOMAS.ToString());

            await PostOmas("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", JsonToOMAS.ToString());
            jsonOmasExpParType = jsonFromOmas;
            LoadChartContentsFormat();
            chargerDonnee.IsActive = false;
        }
        private void LoadChartContentsFormat()
        {
            formatList = new List<string>();
            numberList = new List<string>();
            JArray jsonArrayFromOmas = JArray.Parse(jsonFromOmas);

            // retour =>   [{"format":"verb", "number":8}, {"format":"tab", "number":2}]
            //[{"type":"AG", "avg-time":63712},{"type":"PK", "avg-time":9297},{"type":"CO", "avg-time":1055748},{"type":"AU", "avg-time":0}]
            System.Diagnostics.Debug.Write(jsonArrayFromOmas);
            foreach (JObject o in jsonArrayFromOmas.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    if (p.Name.Equals("format"))
                    {
                        formatList.Add(p.Value.ToString());
                    }
                    if (p.Name.Equals("number"))
                    {
                        numberList.Add(p.Value.ToString());
                    }


                }
            }    

            List<ExpFormat> lstSource = new List<ExpFormat>();
            for (int i = 0; i < formatList.Count; i++)
            {
                lstSource.Add(new ExpFormat() { format = formatList[i], number = int.Parse(numberList[i]) });

                //  (ColumnChart.Series[0] as ColumnSeries).ItemsSource = lstSource;

            }
            (ColumnChart.Series[0] as ColumnSeries).ItemsSource = lstSource;


        }

        private void OnLayoutUpdatedAuthen(object sender, object e)
        {
            if (authenPopupChild.ActualWidth == 0 && authenPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void quitSharingPopup_Click(object sender, RoutedEventArgs e)
        {
            if (sharingSpacePopup.IsOpen)
            {
                sharingSpacePopup.IsOpen = false;
            }
        }
        private void quitIndexPopup_Click(object sender, RoutedEventArgs e)
        {
            if (indexPopup.IsOpen)
            {
                indexPopup.IsOpen = false;
            }

        }

        private void showShSpInfo_Click(object sender, RoutedEventArgs e)
        {
            if (sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = false; }

            indexInfo();
            showSharingSpaceInfo();
            Index.getIndex();
            List<string> nom_cpt = Index.getnom_cpt();
            for (int i = 0; i < nom_cpt.Count; i++)
            {
                RadioButton radioButton = new RadioButton();
                radioButton.Content = nom_cpt[i];
                radioButton.Margin = new Thickness(20, 20, 0, 0);
                radioButton.Name = "radioButton" + i;


                radioButtonList.Add(radioButton);
                //IndexStackPanel.Children.Add(radioButton);
                IndexListview.ItemsSource = radioButtonList;
                //if (i >= 7)
                //{
                //    for (i = 8; i < nom_cpt.Count; i++)
                //    {
                //        RadioButton radioButton1 = new RadioButton();
                //        radioButton1.Content = nom_cpt[i];
                //        radioButton1.Margin = new Thickness(20, 20, 0, 0);
                //        radioButton1.Name = "radioButton" + i;

                //        radioButtonList.Add(radioButton1);
                //        IndexStackPanel1.Children.Add(radioButton1);
                //    }
                //}
            }
            if (!indexPopup.IsOpen) { indexPopup.IsOpen = true; }
        }
        public void indexInfo()
        {
            nom_cpt = new List<string>();
            index = new List<string>();
            prefixOrConceptPrefix = new List<string>();
            ConceptPrefixList = new List<string>();
            PrefixList = new List<string>();
            PrefixList = SharingSpace.getPrefixList();
            ConceptPrefixList = SharingSpace.getConceptPrefixList();
            jsonFromUrl = SharingSpace.getJsonFromUrl("http://memorae.hds.utc.fr/api/" + ConceptPrefixList[0] + "/graph");
            //    System.Diagnostics.Debug.Write("\njsonfrom Url  " + jsonFromUrl);
            JObject jsonObject = JObject.Parse(jsonFromUrl);
            foreach (JProperty p in jsonObject.Properties())
            {
                if (p.Name.Equals("concepts"))
                {

                    JArray jsonArrayConcept = JArray.Parse(p.Value.ToString());
                    //   System.Diagnostics.Debug.Write("\n jsonArrayConcept  " + jsonArrayConcept);

                    foreach (JObject o in jsonArrayConcept.Children<JObject>())
                    {
                        foreach (JProperty pp in o.Properties())
                        {
                            if (pp.Name.Equals("nom_cpt"))
                            {
                                string x = pp.Value.ToString();
                                nom_cpt.Add(x);
                            }

                            if (pp.Name.Equals("type_cpt"))
                            {
                                string y = pp.Value.ToString();
                                if (y.Equals("0"))
                                {
                                    index.Add(ConceptPrefixList[0]);
                                    prefixOrConceptPrefix.Add(ConceptPrefixList[0]);
                                    foreach (JProperty ppp in o.Properties())
                                    {
                                        if (ppp.Name.Equals("id_cpt_second"))
                                            index.Add(ppp.Value.ToString());
                                    }


                                }
                                else
                                {
                                    if (y.Equals("1"))
                                    {

                                        index.Add(PrefixList[0]);
                                        prefixOrConceptPrefix.Add(ConceptPrefixList[0]);
                                        foreach (JProperty ppp in o.Properties())
                                        {
                                            if (ppp.Name.Equals("id_cpt_second"))
                                                index.Add(ppp.Value.ToString());
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public void showSharingSpaceInfo()
        {

            // foreach (CheckBox checkbox in checkBoxList)
            for (int i = 0; i < radioButtonList.Count; i++)
            {
                if (radioButtonList[i].IsChecked == true)
                {
                    string[] split = elementList[i].Split('#');
                    idSharingSpace.Add(split[1]);
                }
            }
        }
    
        public async void showInfo()
        {
            //List<string> idSharingSpace = new List<string>();
            user = "amc2:" + SharingSpace.getElementFromCheckAccess();
            distributor = new List<String>();
            SharingSpaceList = new List<String>();
            PrefixList = new List<String>();
            PrefixList = SharingSpace.getPrefixList();
            JArray jsonArrayIndex = new JArray();
            indexOfSelectedRadioButton = new List<String>();

            string ElementFromCheckAccessList = SharingSpace.getElementFromCheckAccess();
            distributor.Add(user);
            System.Diagnostics.Debug.Write("\nradio button count " + radioButtonList.Count + "\n"); // 15 radio button
            for (int i = 0; i < radioButtonList.Count; i++)
            {
                if (radioButtonList[i].IsChecked == true)
                {

                    //   System.Diagnostics.Debug.Write("\n radio button numero : " + i+ "\n");
                    // sharingSpacePage.showSharingSpaceInfo();
                    string[] split = index[2 * i + 1].Split('#');

                    indexOfSelectedRadioButton.Add(index[2 * i] + ":" + split[1]);


                }
            }
            /******************************************** we are here ***********************************/
            //idSharingSpace = SendToOmasSharingSpace.getIdSharingSpace();
 
            string sharingSpace = PrefixList[0] + ":" + idSharingSpace[0];



            /************************************envoi d'un json vers OMAS**********************************/
            //   {“service” : “retrieve - number - type - time”, “pillbox - data” : [“time” : 20], 
            //“memorae-data” : [“login” : “personne-login”, “mot-de-passe” : “personne-mot-de-passe”, “sharing-space” : “personne-sharing-space”, …], 
            //“pattern” : [“patient-type” : “nombre”]    }

            JObject memorae_data = new JObject(
                            new JProperty("login", App.logIn),
                            new JProperty("pass", App.password),
                            new JProperty("sharingSpace", "aswars:spaceAPIConcept54bfd81f6dc17"),
                            //new JProperty("element", SharingSpace.getElementFromCheckAccess()),
                            //new JProperty("title", "getPerson"),
                            //new JProperty("body", "getPerson.json"),
                            new JProperty("index", indexOfSelectedRadioButton[0]),
                             new JProperty("creator", user)
                            );
            JObject dateReq = new JObject(
                new JProperty("dateDebut", dateDebut.Date.DateTime.ToString("dd-MM-yyyy")),
                new JProperty("dateFin", dateFin.Date.DateTime.ToString("dd-MM-yyyy"))
                );
            JObject str = new JObject(new JProperty("Name", "n"), new JProperty("Amount", 0)) ;
            
          JObject AllInformationToOMAS = new JObject(new JProperty("memorae-data", memorae_data),

                new JProperty("service", "note-utilisateur"),
                 new JProperty("pillbox-data", dateReq),
                   new JProperty("pattern", str)

                );
            //   string information = user + "#,#" + indexOfSelectedRadioButton[0] + "#,#" + sharingSpace + "#,#" + "title:getjson";

           
            System.Diagnostics.Debug.Write("\ninformation " + AllInformationToOMAS);
            await MainMenu.Post("http://ace4sd.hds.utc.fr/webqueryrequest?user-input=", AllInformationToOMAS.ToString());

            //lstNotes = JsonConvert.DeserializeObject<List<Notes>>(App.txtresponseLogin);
            LoadChartContents();
            progressRing.IsActive = false;

            //System.Diagnostics.Debug.WriteLine(App.txtresponseLogin);
            //{ "annabi":(67), "Abel":(45), "alaya":(21), "bellili":(16)}

        }


    }
}
