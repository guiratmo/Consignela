﻿using Consignela.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Popups;
using Windows.Storage;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Ajout_pres_page : Page
    {
       internal List<Medicament> Medicaments;
        internal List<MediImages> flipviewMediImages { get; set; }
        List<Medicament> medicament = new List<Medicament>();
        internal static int i;
        internal int j = 0,p=0, k,l;
        int abs, coord;
        internal List<Prescription> listPresTemp;
        internal List<PrescriptionTab> listPresTabTemp;
        internal List<MomentJour> ListmomentJourNew = new List<MomentJour>();
        internal StorageFile sampleFilePrescription;
        internal StorageFile sampleFilePrescriptionTab;
        internal String strJsonPresc;
        internal String strJsonTabPresc;
        public List<String> Jours = new List<string>(new string[] { "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche" });
        public List<String> Moments = new List<string>(new string[] { "Matin", "Midi", "Soir"});
        public List<String> AjoutIrrigulier = new List<string>();
        List<Prise> PriseTmp = new List<Prise>();
        byte[] bytes;
        public Ajout_pres_page()
        {
            this.InitializeComponent();
            this.loadmediImages();
            this.loadPrescriptonData();
            this.genereTabMomentJour();
            this.loadPrescriptonTabData();
            this.disableAjoutRegulier();
            this.disableAjoutIrregulier();
        }
        private bool verifierNbrMedi()
        {
            if (togb_ajoutReg.IsChecked == true && !string.IsNullOrEmpty(nbr_medi_reg.Text) && !string.IsNullOrEmpty(nbr_medi.Text) && !(cb_jourRD.SelectedItem == null) && !(cb_jourRF.SelectedItem == null) && !(cb_momentR.SelectedItem == null))
            {
                int nbrJour = jourToInt(((ComboBoxItem)cb_jourRF.SelectedItem).Content.ToString()) - jourToInt(((ComboBoxItem)cb_jourRD.SelectedItem).Content.ToString()) + 1;
                if (Int32.Parse(nbr_medi_reg.Text) * nbrJour != Int32.Parse(nbr_medi.Text))
                {
                    nbrMediError.Opacity = 1;
                    fieldMissing.Opacity = 0;
                    return false;
                }
            }
            if (togb_ajoutIrr.IsChecked == true && !string.IsNullOrEmpty(nbr_medi_irreg.Text) && !string.IsNullOrEmpty(nbr_medi.Text) && !(cb_momentIr.SelectedItem == null))
            {
                if (Int32.Parse(nbr_medi_irreg.Text) != Int32.Parse(nbr_medi.Text))
                {
                    nbrMediError.Opacity = 1;
                    fieldMissing.Opacity = 0;
                    return false;

                }
            }
            nbrMediError.Opacity = 0;
            return true;
        }

        private void disableAjoutRegulier()
        {
            nbr_medi_reg.Text = "";
            cb_momentR.SelectedIndex = -1;
            cb_jourRD.SelectedIndex = -1;
            cb_jourRF.SelectedIndex = -1;

            nbr_medi_reg.IsEnabled = false;
            cb_momentR.IsEnabled = false;
            cb_jourRD.IsEnabled = false;
            cb_jourRF.IsEnabled = false;
        }
        private void disableAjoutIrregulier()
        {
            nbr_medi_irreg.Text = "";
            cb_momentIr.SelectedIndex = -1;
            cb_jourIr.SelectedIndex = -1;

            nbr_medi_irreg.IsEnabled = false;
            cb_momentIr.IsEnabled = false;
            cb_jourIr.IsEnabled = false;
            
        }
        private void enableAjoutRegulier()
        {
            nbr_medi_reg.IsEnabled = true;
            cb_momentR.IsEnabled = true;
            cb_jourRD.IsEnabled = true;
            cb_jourRF.IsEnabled = true;
        }
        private void enableAjoutIrregulier()
        {
            nbr_medi_irreg.IsEnabled = true;
            cb_momentIr.IsEnabled = true;
            cb_jourIr.IsEnabled = true;

        }
        private void genereTabMomentJour()
        {
            for(k=1; k <=3;k++)
            {
                for(l=1;l<=7;l++)
                {
                    MomentJour momentJourNew = new MomentJour { abs = k, coord = l,moment=Moments[k-1],jour=Jours[l-1] };
                    ListmomentJourNew.Add(momentJourNew);
                }
            }
        }
        private async void loadPrescriptonData()
        {
            if (App.authentification)
            {
                sampleFilePrescription = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionData.json");
            }
            else
            {
                sampleFilePrescription = await App.storageFolder.GetFileAsync("PrescriptionData.json");
            }
            
            if (sampleFilePrescription != null)
            {
                strJsonPresc = await FileIO.ReadTextAsync(sampleFilePrescription);
            }

                listPresTemp = JsonConvert.DeserializeObject<List<Prescription>>(strJsonPresc);
            if(listPresTemp != null)
            {
                i = listPresTemp[listPresTemp.Count - 1].id + 1;
            }
            else
            {
                i = 0;
            }
           
        }
        private async void loadPrescriptonTabData()
        {
            if (App.authentification)
            {
                sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync(App.logIn + "-PrescriptionTabData.json");
            }
            else
            {
                sampleFilePrescriptionTab = await App.storageFolder.GetFileAsync("PrescriptionTabData.json");
            }

            if (sampleFilePrescriptionTab != null)
            {
                strJsonTabPresc = await FileIO.ReadTextAsync(sampleFilePrescriptionTab);
            }

                listPresTabTemp = JsonConvert.DeserializeObject<List<PrescriptionTab>>(strJsonTabPresc);       
        }

        //private void back_Click(object sender, RoutedEventArgs e)
        //{
            
        //    if (this.Frame.CanGoBack)
        //    {
        //        this.Frame.GoBack();
        //    }
        //}
        
        private async void btn_savePresc_Click(object sender, RoutedEventArgs e)
        {
            if (gridViewPresc.Items.Count != 0 && !String.IsNullOrEmpty(presc_name.Text))
            {
                fieldMissing.Opacity = 0;
                List<Prescription> listPrescriptions = new List<Prescription>();
                

                Date newDate = new Date { day = presc_date.Date.Day.ToString(), month = presc_date.Date.Month.ToString(), year = presc_date.Date.Year.ToString(), time = presc_date.Date.TimeOfDay.ToString() };
                newDate.month = newDate.monthToStr();

                Prescription prescription = new Prescription { id = i, date = newDate, dateTime = DateTime.Today.Date, nom = presc_name.Text };

                prescription.medicaments = medicament;
               
                if(listPresTemp != null)
                {
                    listPrescriptions = listPresTemp;
                }
                
                listPrescriptions.Add(prescription);

                await FileIO.WriteTextAsync(sampleFilePrescription, JsonConvert.SerializeObject(listPrescriptions));
                //File.WriteAllText(@"PrescriptionData.json", JsonConvert.SerializeObject(listPrescriptions));

                List<PrescriptionTab> listPrescriptionTab = new List<PrescriptionTab>();

                PrescriptionTab prescriptionTabNew = new PrescriptionTab { id = i, nom = presc_name.Text };
                prescriptionTabNew.momentJour = ListmomentJourNew;

                if (listPresTabTemp != null)
                {
                    listPrescriptionTab = listPresTabTemp;
                }

                listPrescriptionTab.Add(prescriptionTabNew);

                await FileIO.WriteTextAsync(sampleFilePrescriptionTab, JsonConvert.SerializeObject(listPrescriptionTab));

                //File.WriteAllText(@"PrescriptionTabData.json", JsonConvert.SerializeObject(listPrescriptionTab));
                var stream = await sampleFilePrescription.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFilePrescription.Path);

                var stream1 = await sampleFilePrescriptionTab.OpenStreamForReadAsync();
                bytes = new byte[(int)stream1.Length];
                stream1.Read(bytes, 0, (int)stream1.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFilePrescriptionTab.Path);
                feedbackAjout();
            }
            else
            {
                fieldMissing.Opacity = 1;
                nbrMediError.Opacity = 0;
            }

        }
        private void prescToTab(int abs, int coord, Medicament medicament, int nbrTabulaire)
        {
            
            Medicament newMedicament = new Medicament();
            //newMedicament.momentReg = "";
            //newMedicament.momentIrreg = "";
            //newMedicament.jourRD = "";
            //newMedicament.jourRF = "";
            //newMedicament.jourIrreg = "";
            //newMedicament.descrReg = "";
            //newMedicament.descrIrreg = "";
            


            foreach (MomentJour momentJour in ListmomentJourNew)
            {
                if(momentJour.abs == abs && momentJour.coord == coord)
                {
                    newMedicament.nom = medicament.nom;
                    newMedicament.nbrTab = nbrTabulaire;
                    newMedicament.imageMedicament = medicament.imageMedicament;
                    newMedicament.momentReg = "";
                    newMedicament.momentIrreg = "";
                    newMedicament.jourRD = "";
                    newMedicament.jourRF = "";
                    newMedicament.jourIrreg = "";
                    newMedicament.descrReg = "";
                    newMedicament.descrIrreg = "";
                    momentJour.listMedicaments.Add(newMedicament);
                }
            }
            //ListmomentJourNew.Select(MomentJour => MomentJour.abs == 1).

        }

        private async void feedbackAjout()
        {
            
            MessageDialog selectConsigne = new MessageDialog("Prescription ajoutée avec succées", "Ajout de la prescription");
            await selectConsigne.ShowAsync();
        }

        private void btn_saveDate_Click(object sender, RoutedEventArgs e)
        {
            tb_date_presc.Text = "Date : "+presc_date.Date.Day+" / "+ presc_date.Date.Month+" / "+ presc_date.Date.Year;
            
        }

        private void btn_saveMedi_Click(object sender, RoutedEventArgs e)
        {
            

            if (!string.IsNullOrEmpty(nom_medi.Text) && !string.IsNullOrEmpty(nbr_medi.Text) && !(cb_medicamentImg.SelectedItem == null))
            {
                int nbrTabIrreg;
                fieldMissing.Opacity = 0;
                //********flashBack
                Medicaments = GetMedicaments();
                //********flashBack

                gridViewPresc.ItemsSource = null;
                gridViewPresc.ItemsSource = Medicaments;
                flipviewMediImages[cb_medicamentImg.SelectedIndex].enabled = "true";
                flipviewMediImages[cb_medicamentImg.SelectedIndex].opacity = 0.2F;
                cb_medicamentImg.ItemsSource = null;
                cb_medicamentImg.ItemsSource = flipviewMediImages;

                // ajouter le medicament au tablulaire
                //******flashBack
                if (Medicaments[p].irregulier.Equals("True"))
                {
                    foreach (Prise prise in Medicaments[p].Prises)
                    {
                        abs = momentToInt(prise.moment);
                        coord = jourToInt(prise.jour);
                        nbrTabIrreg = prise.nbr;
                        prescToTab(abs, coord, Medicaments[p], nbrTabIrreg);
                    }
                }

                if (Medicaments[p].regulier.Equals("True"))
                {
                    int intJourRD = jourToInt(Medicaments[p].jourRD);
                    int intJourRF = jourToInt(Medicaments[p].jourRF);
                    int nbrTabReg = Medicaments[p].nbrReg;
                    for (int c = intJourRD; c <= intJourRF; c++)
                    {
                        abs = momentToInt(Medicaments[p].momentReg);
                        coord = c;
                        prescToTab(abs, coord, Medicaments[p], nbrTabReg);
                    }
                }
                //******flashBack
                p++;

                nom_medi.Text = "";
                nbr_medi.Text = "";
                nbr_medi_reg.Text = "";
                nbr_medi_irreg.Text = "";
                AjoutIrrigulier.Clear();
                listAjoutIrrigulier.ItemsSource = null;
                listAjoutIrrigulier.ItemsSource = AjoutIrrigulier;

            }
            else
            {
                fieldMissing.Opacity = 1;
                nbrMediError.Opacity = 0;
            }

        }

        private int jourToInt(String jour)
        {
            switch (jour)
            {
                case "Lundi":
                    return 1;
                case "Mardi":
                    return 2;
                case "Mercredi":
                    return 3;
                case "Jeudi":
                    return 4;
                case "Vendredi":
                    return 5;
                case "Samedi":
                    return 6;
                case "Dimanche":
                    return 7;
            }
            return 0;
        }

        private int momentToInt(String moment)
        {
            switch(moment)
            {
                case "Matin":
                    return 1;
                case "Midi":
                    return 2;
                case "Soir":
                    return 3;
            }
            return 0;
        }

        private void togb_ajoutReg_Checked(object sender, RoutedEventArgs e)
        {
            enableAjoutRegulier();
            togb_ajoutIrr.IsChecked = false;
            disableAjoutIrregulier();
        }

        private void togb_ajoutIrr_Checked(object sender, RoutedEventArgs e)
        {
            enableAjoutIrregulier();
            togb_ajoutReg.IsChecked = false;
            disableAjoutRegulier();
        }

        private void nbr_medi_irreg_TextChanged(object sender, TextChangedEventArgs e)
        {
            verifierNbrMedi();
        }

        private void nbr_medi_reg_TextChanged(object sender, TextChangedEventArgs e)
        {
            verifierNbrMedi();
        }

        private void cb_jourRD_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            verifierNbrMedi();
        }

        private void cb_jourRF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            verifierNbrMedi();
        }

        private void nbr_medi_TextChanged(object sender, TextChangedEventArgs e)
        {
            verifierNbrMedi();
        }

        private void presc_name_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(presc_name.Text))
            {
                nomPrescAffichage.Text = presc_name.Text;
            }
            else
            {
                nomPrescAffichage.Text = "La préscription";
            }
            
        }

        private void confirmer_Click(object sender, RoutedEventArgs e)
        {
            if(!String.IsNullOrEmpty(nbr_medi_irreg.Text) && (cb_jourIr.SelectedItem != null) && (cb_momentIr.SelectedItem != null))
            {

                AjoutIrrigulier.Add(nbr_medi_irreg.Text + " Comprimés le " + ((ComboBoxItem)cb_jourIr.SelectedItem).Content.ToString() + " " + ((ComboBoxItem)cb_momentIr.SelectedItem).Content.ToString());
                listAjoutIrrigulier.ItemsSource = null;
                listAjoutIrrigulier.ItemsSource = AjoutIrrigulier;

                PriseTmp.Add(new Prise {nbr = Int32.Parse(nbr_medi_irreg.Text), jour = ((ComboBoxItem)cb_jourIr.SelectedItem).Content.ToString(), moment = ((ComboBoxItem)cb_momentIr.SelectedItem).Content.ToString() });
                
            }
                       
        }

        private void confirmerReg_Click(object sender, RoutedEventArgs e)
        {
            descritionRegulier.Text = "Prendre " + nbr_medi_reg.Text + " Comprimés le " + ((ComboBoxItem)cb_momentR.SelectedItem).Content.ToString() + " de " + ((ComboBoxItem)cb_jourRD.SelectedItem).Content.ToString()+ " à "+ ((ComboBoxItem)cb_jourRF.SelectedItem).Content.ToString();
;        }

        private void delete_Click(object sender, TappedRoutedEventArgs e)
        {
            AjoutIrrigulier.Remove(listAjoutIrrigulier.SelectedItem.ToString());
            PriseTmp.Remove(PriseTmp[listAjoutIrrigulier.SelectedIndex]);
            listAjoutIrrigulier.ItemsSource = null;
            listAjoutIrrigulier.ItemsSource = AjoutIrrigulier;

        }

        private void loadmediImages()
        {
            flipviewMediImages = new List<MediImages>();
            flipviewMediImages.Add(new MediImages() { name = "Comprimé Bleu", imageUri = "Assets/medicaments/comprimeB.png",enabled= "false" , opacity = 1});
            flipviewMediImages.Add(new MediImages() { name = "Comprimé Jaune", imageUri = "Assets/medicaments/comprimeJ.png", enabled = "false", opacity = 1 });
            flipviewMediImages.Add(new MediImages() { name = "Géllule Orange", imageUri = "Assets/medicaments/gelluleO.png", enabled = "false", opacity = 1 });
            flipviewMediImages.Add(new MediImages() { name = "Géllule Verte", imageUri = "Assets/medicaments/gelluleV.png", enabled = "false", opacity = 1 });
            flipviewMediImages.Add(new MediImages() { name = "Autre médicament", imageUri = "Assets/medicaments/gelluleAutre.png", enabled = "false", opacity = 1 });
            //fv_mediImg.ItemsSource = flipviewMediImages;

            cb_medicamentImg.ItemsSource = flipviewMediImages;
        }

        private void nbr_medi_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            //if(!char.IsControl(e.KeyChar))
        }
        //********flashBack
        public List<Medicament> GetMedicaments()
        {
            String descrIrreg = " Prendre ";

            MediImages imgMedi = new MediImages { name = flipviewMediImages[cb_medicamentImg.SelectedIndex].name, imageUri = flipviewMediImages[cb_medicamentImg.SelectedIndex].imageUri };
            if (togb_ajoutReg.IsChecked == true)
            {
                medicament.Add(new Medicament
                {
                    nom = nom_medi.Text,
                    nbr = Int32.Parse(nbr_medi.Text),
                    imageMedicament = imgMedi,
                    regulier = togb_ajoutReg.IsChecked.Value.ToString(),
                    nbrReg = Int32.Parse(nbr_medi_reg.Text),
                    momentReg = ((ComboBoxItem)cb_momentR.SelectedItem).Content.ToString(),
                    jourRD = ((ComboBoxItem)cb_jourRD.SelectedItem).Content.ToString(),
                    jourRF = ((ComboBoxItem)cb_jourRF.SelectedItem).Content.ToString(),
                    irregulier = togb_ajoutIrr.IsChecked.Value.ToString(),
                    nbrIrreg = 0,
                    momentIrreg = null,
                    jourIrreg = null,

                });
                medicament[j].descrReg = " Prendre " + medicament[j].nbrReg + " le " + medicament[j].momentReg + " de " + medicament[j].jourRD + " à " + medicament[j].jourRF;
                medicament[j].descrIrreg = null;
            }
            if (togb_ajoutIrr.IsChecked == true)
            {
                medicament.Add(new Medicament
                {
                    nom = nom_medi.Text,
                    nbr = Int32.Parse(nbr_medi.Text),
                    imageMedicament = imgMedi,
                    regulier = togb_ajoutReg.IsChecked.Value.ToString(),
                    nbrReg = 0,
                    momentReg = null,
                    jourRD = null,
                    jourRF = null,
                    irregulier = togb_ajoutIrr.IsChecked.Value.ToString(),
                    nbrIrreg = Int32.Parse(nbr_medi_irreg.Text),
                    momentIrreg = ((ComboBoxItem)cb_momentIr.SelectedItem).Content.ToString(),
                    jourIrreg = ((ComboBoxItem)cb_jourIr.SelectedItem).Content.ToString(),
                    Prises = PriseTmp

                });
                medicament[j].descrReg = null;
                foreach (Prise prise in PriseTmp)
                {
                    descrIrreg = descrIrreg + prise.nbr + "comprimés le " + prise.jour + " " + prise.moment + ", ";
                }
                medicament[j].descrIrreg = descrIrreg;
            }

            j++;

            return medicament;
        }

        //********flashBack

    }
}
