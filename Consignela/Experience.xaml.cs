﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Consignela.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Experience : Page
    {
        internal List<Medicament> listMedicament;
        internal List<Medicament> tabListMedicaments;
        internal List<GridView> gridViewTabMedicament = new List<GridView>();
        internal Prescription prescriptionTmp = App.getPrescriptionSelect();
        internal PrescriptionTab prescriptionTab = App.prescriptionTabSelect;

        public Experience()
        {
            this.InitializeComponent();
            this.initializeExperience();
        }
        private void initializeExperience()
        {
            if(Pilulier.retourPrescriptionV)
            {
                prescTabulaire.Visibility = Visibility.Visible;
                flipViewConsignes.Visibility = Visibility.Collapsed;
            }
            else
            {
                prescTabulaire.Visibility = Visibility.Collapsed;
            }
            //System.Diagnostics.Debug.Write(App.codeParticipant);
            //expPageName.Text = "Expérience : " + App.codeParticipant;
            flipViewConsignes.ItemsSource = App.consignesSelect.consignes;

            
            listMedicament = prescriptionTmp.medicaments;
            nomPrescription.Text = prescriptionTmp.nom;
            loadGridView();
            System.Diagnostics.Debug.Write(prescriptionTab);

            for (int i = 0; i < 21; i++)
            {
                if (prescriptionTab.momentJour[i].listMedicaments.Count() != 0)
                {                
                    tabListMedicaments = prescriptionTab.momentJour[i].listMedicaments;
                    gridViewTabMedicament[i].ItemsSource = null;
                    gridViewTabMedicament[i].ItemsSource = tabListMedicaments;
                }
            }
   

        }
        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }
        //private void back_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.Frame.CanGoBack)
        //    {
        //        this.Frame.GoBack();
        //    }
        //}

        private void flipViewConsignes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void next_Click(object sender, RoutedEventArgs e)
        {
            
            if (prescTabulaire.Visibility == Visibility.Visible)
            {
                this.Frame.Navigate(typeof(Pilulier), null);
            }          
            else if (flipViewConsignes.SelectedIndex == App.consignesSelect.consignes.Count - 1)
            {
                prescTabulaire.Visibility = Visibility.Visible;
                flipViewConsignes.Visibility = Visibility.Collapsed;
            }
            else
            {
                flipViewConsignes.SelectedItem = flipViewConsignes.Items[flipViewConsignes.SelectedIndex + 1];
            }
            
            
        }
    }
}
