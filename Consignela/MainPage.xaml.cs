﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Newtonsoft.Json;
using Consignela.Model;
using System.Collections.Generic;




// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        internal StorageFile sampleFileExperience;

        public MainPage()
        {
            this.InitializeComponent();
            
        }
        
       
        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void btn_config_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Config_page), null);
        }

        private void btn_paramExp_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Param_Exp_Page), null);
        }

        private void btn_pilotage_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Pilot_Exp_Page), null);
        }

        private void btn_data_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Data_Page), null);
        }

        private void btn_quit_Click(object sender, RoutedEventArgs e)
        {
            CoreApplication.Exit();
        }

      
        private void menuButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void experience_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.authentification)
            {
                bienvenueLogin.Text = "Bienvenue " + App.logIn;
            }
            else
            {
                bienvenueLogin.Text = "";
            }

            //BitmapImage bitmapImage = new BitmapImage();
            //bitmapImage.UriSource = new Uri(imgTest.BaseUri, "E:/pilule-du-sport.jpg");

            //imgTest.Source = new BitmapImage(new Uri("E:/pilule-du-sport.jpg"));

            //*****flashBack2
          //  sampleFileExperience = await App.storageFolder.CreateFileAsync("brahim-experiences.json", CreationCollisionOption.OpenIfExists);
          //  sampleFileExperience = await App.storageFolder.GetFileAsync("brahim-experiences.json");
          //  String strJsonExp = await FileIO.ReadTextAsync(sampleFileExperience);
        

          //List<ExperiencePrescResult> newListExp  = JsonConvert.DeserializeObject<List<ExperiencePrescResult>>(strJsonExp);
          //  System.Diagnostics.Debug.WriteLine(newListExp);
        }

        private static async Task<BitmapImage> LoadImage(StorageFile file)
        {
            BitmapImage bitmapImage = new BitmapImage();
            FileRandomAccessStream stream = (FileRandomAccessStream)await file.OpenAsync(FileAccessMode.Read);

            bitmapImage.SetSource(stream);

            return bitmapImage;

        }
    }
}
