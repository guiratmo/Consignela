﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Web.Http;
using Consignela.Model;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainMenu : Page
    {
        public static String TxtResponse;
        internal ListBoxItem listBoxitemTmp = new ListBoxItem();
        public static int progressBarValue = 5;
        public static StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
        public static Frame rootFrame = Window.Current.Content as Frame;
        


        public MainMenu()
        {
            this.InitializeComponent();

        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           

            if (App.verifDataPage)
            {
                framePage.Navigate(typeof(Data_Page));
                App.verifDataPage = false;
            }
            else
            {
                framePage.Navigate(typeof(MainPage));
                //home.IsSelected = true;
            }


            //experience.IsEnabled = false;

            if(App.authentification)
            {
                profilNom.Text = App.logIn;
                conexion.Background = new SolidColorBrush(Color.FromArgb(255, 240, 196, 1));
            }
            else
            {
                profilNom.Text = "Connexion";
                conexion.Background = new SolidColorBrush(Color.FromArgb(255, 2, 172, 126));
            }
            //authenEtExpPopupOpen();

            //deconnexion.Visibility = Visibility.Collapsed;
            //if (!String.IsNullOrEmpty(App.logIn))
            //{
            //    profilNom.Text = App.logIn;
            //}
            //else
            //{
            //    profilNom.Text = "Se connecter";
            //}

            
        }


        private void menuButton_Click(object sender, RoutedEventArgs e)
        {
            //choiConsignesTb.Foreground = new SolidColorBrush(Colors.Blue);
            progressBarExp.Value = progressBarValue;

            menuSplitView.IsPaneOpen = !menuSplitView.IsPaneOpen;

            if (App.consignesSelect != null)
            {         
                choiConsignesTb.Text = App.consignesSelect.nom;              
                choiConsignesTb.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
                choixPrescriptionTb.Foreground = new SolidColorBrush(Colors.Blue);
            }
            if (App.getPrescriptionSelect() != null)
            {
                choixPrescriptionTb.Text = App.getPrescriptionSelect().nom;
                choixPrescriptionTb.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
                progressBarExp.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
                lancerExpTb.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
                
                //experience.IsEnabled = true;

                //Uri imageUri = new Uri("ms-appx:///Assets/pilulierO.png");
                //BitmapImage imageBitmap = new BitmapImage(imageUri);
                //expIcon.Source = imageBitmap;

                //expListBox.Foreground = new SolidColorBrush(Color.FromArgb(255, 253, 153, 8));
            }
            if(App.authentification == true)
            {
                //profilNom.Text = App.logIn;
                //profileIcon.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 215, 0));
                //profilNom.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 215, 0));
            }
            

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (framePage.CanGoBack)
            {
                framePage.GoBack();
                framePage.Navigate(typeof(MainPage));
                //home.IsSelected = true;
                //menuListBox.SelectedItem = framePage;
            }

        }

        private void quit_Click(object sender, RoutedEventArgs e)
        {
            CoreApplication.Exit();
        }

        private void authenEtExpPopupOpen()
        {

            if (!authenPopup.IsOpen && App.authentification == false)
            {
                authenPopup.IsOpen = true;
                framePage.IsEnabled = false;

            }

        }
        private void authenPoupOpen()
        {
            
            if (!authenPopup1.IsOpen && App.authentification == false)
            {
                authenPopup1.IsOpen = true;
                framePage.IsEnabled = false;
                
            }           

        }
        private void quitAuthenPopup_Click(object sender, RoutedEventArgs e)
        {
            if (authenPopup1.IsOpen) { authenPopup1.IsOpen = false; }
        }
        private void OnLayoutUpdatedAuth(object sender, object e)
        {
            if (authenPopupChild1.ActualWidth == 0 && authenPopupChild1.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup1.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup1.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild1.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild1.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup1.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup1.VerticalOffset = NewVerticalOffset;
            }
        }
        private void OnLayoutUpdated(object sender, object e)
        {
            if (authenPopupChild.ActualWidth == 0 && authenPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (home.IsSelected)
            //{
            //    //BackButton.Visibility = Visibility.Collapsed;
            //    framePage.Navigate(typeof(MainPage));
            //    titleTextBlock.Text = "Consignela";
            //    listBoxitemTmp = home;
            //}
            if (PreparerExperience.IsSelected)
            {
                //BackButton.Visibility = Visibility.Visible;
                framePage.Navigate(typeof(PreparerExperience));
                titleTextBlock.Text = "Preparation d'une experience";
                listBoxitemTmp = PreparerExperience;
            }
            //else if (consignes.IsSelected)
            //{
            //    //BackButton.Visibility = Visibility.Visible;
            //    framePage.Navigate(typeof(Select_Consi_Page));
            //    titleTextBlock.Text = "Liste des consignes";
            //    listBoxitemTmp = consignes;
            //}
            //else if (prescription.IsSelected)
            //{
            //    //BackButton.Visibility = Visibility.Visible;
            //    framePage.Navigate(typeof(Select_Presc_Page));
            //    titleTextBlock.Text = "Prescription";
            //    listBoxitemTmp = prescription;
            //}
            else if(configuration.IsSelected)
            {
                //BackButton.Visibility = Visibility.Visible;
                framePage.Navigate(typeof(Config_page));
                titleTextBlock.Text = "Configuration de l'expérimentation";
                listBoxitemTmp = configuration;
            }
            else if (RecuperationDonnee.IsSelected)
            {
                //BackButton.Visibility = Visibility.Visible;
                framePage.Navigate(typeof(Data_Page));
                titleTextBlock.Text = "Recuperation des données";
                listBoxitemTmp = RecuperationDonnee;
            }

            //else if (experience.IsSelected)
            //{
            //    //BackButton.Visibility = Visibility.Visible;
            //    framePage.Navigate(typeof(Pilot_Exp_Page));
            //    titleTextBlock.Text = "Pilotage de l'expérience";
            //    listBoxitemTmp = experience;
            //}

        }
        private async void connect_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login1.Text + "\"" + ",\"pass\":" + "\"" + pass1.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login1.Text;
                App.password = pass1.Password;
                App.authentification = true;
                //MessageDialog loginMessage = new MessageDialog("Bienvenue " + App.logIn);
                //await loginMessage.ShowAsync();
                if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
                menuSplitView.IsEnabled = true;
                guidePanel.Opacity = 1;
                framePage.IsEnabled = true;
                profilNom.Text = App.logIn;
                conexion.Background = new SolidColorBrush(Color.FromArgb(255, 240, 196, 1));
                //profileIcon.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 215, 0));
                //profilNom.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 215, 0));
                //deconnexion.Visibility = Visibility.Visible;
                StorageFile fileConsigne = await storageFolder.CreateFileAsync(App.logIn + "-consignes.json", CreationCollisionOption.OpenIfExists);
                StorageFile sampleFilePrescription = await storageFolder.CreateFileAsync(App.logIn + "-PrescriptionData.json", CreationCollisionOption.OpenIfExists);
                StorageFile sampleFilePrescriptionTab = await storageFolder.CreateFileAsync(App.logIn + "-PrescriptionTabData.json", CreationCollisionOption.OpenIfExists);
                StorageFile sampleFileExperienceConf = await storageFolder.CreateFileAsync(App.logIn + "-ExperienceConf.json", CreationCollisionOption.OpenIfExists);
                StorageFile sampleFileExp = await storageFolder.CreateFileAsync(App.logIn + "-ExperiencePrescResult.json", CreationCollisionOption.OpenIfExists);
                SharingSpace.getSharingSapce();

                rootFrame.Navigate(typeof(MainMenu));

                if (authenPopup1.IsOpen && App.authentification == true)
                { authenPopup1.IsOpen = false;}

            }
            else
            {
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
        }
        private async void btn_connect_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login.Text + "\"" + ",\"pass\":" + "\"" + pass.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login.Text;
                App.password = pass.Password;
                App.authentification = true;
                MessageDialog loginMessage = new MessageDialog("Bienvenue " + App.logIn);
                await loginMessage.ShowAsync();
                if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
                menuSplitView.IsEnabled = true;
                guidePanel.Opacity = 1;
                framePage.IsEnabled = true;
                profilNom.Text = App.logIn;
                conexion.Background = new SolidColorBrush(Color.FromArgb(255, 240, 196, 1));
                //profilNom.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 215, 0));
                //deconnexion.Visibility = Visibility.Visible;
                StorageFile fileConsigne = await storageFolder.CreateFileAsync(App.logIn + "-consignes.json", CreationCollisionOption.OpenIfExists);
                StorageFile sampleFilePrescription = await storageFolder.CreateFileAsync(App.logIn + "-PrescriptionData.json", CreationCollisionOption.OpenIfExists);
                StorageFile sampleFilePrescriptionTab = await storageFolder.CreateFileAsync(App.logIn + "-PrescriptionTabData.json", CreationCollisionOption.OpenIfExists);
                rootFrame.Navigate(typeof(MainMenu));
            }
            else
            {  
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
        }

        public static async Task Post(string website, string args)
        {

            //init client
            HttpClient hc = new HttpClient();
            //send post request
            HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            TxtResponse = await hrm.Content.ReadAsStringAsync();
            App.txtresponseLogin = TxtResponse;
            hc.Dispose();
            System.Diagnostics.Debug.Write(TxtResponse);
        }

        private void deconnexion_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (App.authentification)
            {
                App.logIn = "";
                App.password = "";
                App.authentification = false;
                App.consignesSelect = null;
                App.prescriptionSelect = null;
                App.prescriptionTabSelect = null;
                //profilNom.Text = "Se Connecter";
                //profileIcon.Foreground = new SolidColorBrush(Color.FromArgb(255, 4, 110, 170));
                //profilNom.Foreground = new SolidColorBrush(Color.FromArgb(255, 58, 79, 91));
                authenEtExpPopupOpen();
            }
            
        }

        private void experience_Click(object sender, RoutedEventArgs e)
        {
            App.expSansAUthen = true;
            if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
            menuSplitView.IsEnabled = true;
            guidePanel.Opacity = 1;
            framePage.IsEnabled = true;
        }

        private void profil_Tapped(object sender, TappedRoutedEventArgs e)
        {
            authenPoupOpen();
        }

        private void conexion_Click(object sender, RoutedEventArgs e)
        {
            authenPoupOpen();
            if(App.authentification)
            {
                var button = sender as Button;
                Flyout flyout = new Flyout();
                var txtblock = new TextBlock();
                txtblock.HorizontalAlignment = HorizontalAlignment.Center;
                txtblock.Text = "Déconnexion";
                Button deconnexion = new Button();
                deconnexion.Content = txtblock;
                deconnexion.Width = 200;
                deconnexion.Height = 50;

                flyout.Placement = FlyoutPlacementMode.Top;
                flyout.Content = deconnexion;

                FlyoutBase.SetAttachedFlyout(button, flyout);

                flyout.ShowAt(button);
                deconnexion.Click += Deconnexion_Click;
            }
            
        }

        private void Deconnexion_Click(object sender, RoutedEventArgs e)
        {
            if (App.authentification)
            {
                App.logIn = "";
                App.password = "";
                App.authentification = false;
                App.consignesSelect = null;
                App.prescriptionSelect = null;
                App.prescriptionTabSelect = null;

                rootFrame.Navigate(typeof(MainMenu));

            }
        }
    }
}
