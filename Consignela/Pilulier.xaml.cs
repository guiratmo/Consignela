﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Consignela.Model;
using Windows.Storage;
using Newtonsoft.Json;
using Consignela.Model;
using System.IO;
using Windows.UI.Xaml;
using Windows.Web.Http;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Consignela
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Pilulier : Page
    {
        internal Medicament medicamentChoisi;       
        internal List<Medicament> listMedicament = new List<Medicament>();
        internal List<List<Medicament>> tabListMedicaments = new List<List<Medicament>>();
        internal List<GridView> gridViewTabMedicament = new List<GridView>();
        internal Prescription prescriptionTmp ;
        private Prescription prescriptionTmp2;
        internal List<bool> open = new List<bool> ();
        internal String openCase = "";
        bool singleTap, nvMedicament, mediSelected=false;
        int j;
        internal int IndexgridviewSelect;
        internal int indexMediSelect;
        internal Medicament medicamentSelect;
        public static bool retourPrescriptionV = false;
        List<int> ListnbrTab;
        Resultat resultatFinal;

        internal StorageFile sampleFileExp;
        internal ExperiencePrescResult ExpPrescResulat;
        internal List<ExperiencePrescResult> ListExpPrescResulat = new List<ExperiencePrescResult>();
        public String str_Exp_json;
        byte[] bytes;
        List<string> nameList = new List<string>();
        List<string> elementList = new List<string>();
        List<CheckBox> checkBoxListSharingSpace = new List<CheckBox>();
        public static List<CheckBox> checkBoxListIndex = new List<CheckBox>();
        internal String TxtResponse;
        static List<string> idSharingSpace = new List<string>();

        public Pilulier()
        {
            this.InitializeComponent();
            chargerListMedicament();
            gridViewBoiteMedicaments.ItemsSource = listMedicament;
            //tabListMedicaments.Add(listMedicament);
            loadGridView();
            loadTabListMedicaments();
            loadOpen();


           
        }
        private async void Page_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (App.authentification)
            {
                
                sampleFileExp = await App.storageFolder.GetFileAsync(App.logIn + "-experiences.json");
                str_Exp_json = getJsonFromUrl("http://memorae.hds.utc.fr/api/documents/" + App.logIn + "-experiences.json");
                if (str_Exp_json == "error")
                {          
                    str_Exp_json = await FileIO.ReadTextAsync(sampleFileExp);                    
                }
                ListExpPrescResulat = JsonConvert.DeserializeObject<List<ExperiencePrescResult>>(str_Exp_json);
                await FileIO.WriteTextAsync(sampleFileExp, JsonConvert.SerializeObject(ListExpPrescResulat));
            }
            else
            {
                sampleFileExp = await App.storageFolder.GetFileAsync("ExperiencePrescResult.json");
                str_Exp_json = await FileIO.ReadTextAsync(sampleFileExp);
                ListExpPrescResulat = JsonConvert.DeserializeObject<List<ExperiencePrescResult>>(str_Exp_json);
            }


        }
        public static string getJsonFromUrl(string url)
        {
            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
            var response = client.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                // by calling .Result you are performing a synchronous call
                var responseContent = response.Content;

                // by calling .Result you are synchronously reading the result
                string responseString = responseContent.ReadAsStringAsync().Result;

                return responseString;
            }
            else
            {
                return "error";
            }

        }
        public void chargerListMedicament ()
        {
            prescriptionTmp2 = App.getPrescriptionSelect();
            prescriptionTmp = prescriptionTmp2;


            foreach (Medicament medicament in prescriptionTmp.medicaments)
            {
                listMedicament.Add(medicament);
            }       
        }
        public void loadOpen()
        {
            for (int i = 0; i < 21; i++)
            {
                open.Add(false);
            }
        }
        public void loadTabListMedicaments()
        {
            for(int i = 0; i<21; i++)
            {
                tabListMedicaments.Add(new List<Medicament>());
            }
            
        }
        public void loadGridView()
        {
            gridViewTabMedicament.Add(gridViewTabMedicament11);
            gridViewTabMedicament.Add(gridViewTabMedicament12);
            gridViewTabMedicament.Add(gridViewTabMedicament13);
            gridViewTabMedicament.Add(gridViewTabMedicament14);
            gridViewTabMedicament.Add(gridViewTabMedicament15);
            gridViewTabMedicament.Add(gridViewTabMedicament16);
            gridViewTabMedicament.Add(gridViewTabMedicament17);
            gridViewTabMedicament.Add(gridViewTabMedicament21);
            gridViewTabMedicament.Add(gridViewTabMedicament22);
            gridViewTabMedicament.Add(gridViewTabMedicament23);
            gridViewTabMedicament.Add(gridViewTabMedicament24);
            gridViewTabMedicament.Add(gridViewTabMedicament25);
            gridViewTabMedicament.Add(gridViewTabMedicament26);
            gridViewTabMedicament.Add(gridViewTabMedicament27);
            gridViewTabMedicament.Add(gridViewTabMedicament31);
            gridViewTabMedicament.Add(gridViewTabMedicament32);
            gridViewTabMedicament.Add(gridViewTabMedicament33);
            gridViewTabMedicament.Add(gridViewTabMedicament34);
            gridViewTabMedicament.Add(gridViewTabMedicament35);
            gridViewTabMedicament.Add(gridViewTabMedicament36);
            gridViewTabMedicament.Add(gridViewTabMedicament37);
        }

        private void gridViewBoiteMedicaments_Tapped(object sender, TappedRoutedEventArgs e)
        {
            medicamentChoisi = (Medicament)gridViewBoiteMedicaments.SelectedItem;
            //medicamentChoisi.nbrTab = 1;
            j = gridViewBoiteMedicaments.SelectedIndex;
        }
        private void ExecSingleTap(int i,GridView gridviewTab)
        {
            
            
            nvMedicament = true;
            if (open[i] && listMedicament[j].nbr > 0 && !mediSelected && medicamentChoisi != null)
            {
                System.Diagnostics.Debug.WriteLine(tabListMedicaments[i].Count);
                //if there is this medecines
                for (int k = 0; k < tabListMedicaments[i].Count; k++)
                {

                    if (tabListMedicaments[i][k].nom == medicamentChoisi.nom)
                    {
                        
                        tabListMedicaments[i][k].nbrTab++;

                        
                        //medicamentChoisi.nbrTab = 0;
                        nvMedicament = false;
                        break;
                    }
                }
                if (nvMedicament)
                {
                    medicamentChoisi.nbrTab = 1;
                    tabListMedicaments[i].Add(medicamentChoisi);
                }


                listMedicament[j].nbr--;

                gridViewBoiteMedicaments.ItemsSource = null;
                gridViewBoiteMedicaments.ItemsSource = listMedicament;

                gridviewTab.ItemsSource = null;
                gridviewTab.ItemsSource = tabListMedicaments[i];

                medicamentChoisi = null;
            }
            else if(listMedicament[j].nbr <= 0)
            {
                //MessageDialog boiteVide = new MessageDialog("La boite de " + listMedicament[j].nom + " est vide");
                //boiteVide.ShowAsync();
            }
            else if(medicamentChoisi == null)
            {
                //MessageDialog selesctMedi = new MessageDialog("choisissez un médicament svp");
                //selesctMedi.ShowAsync();
            }
        }
        private void ExecDoubleTap(int i , GridView gridviewTab)
        {
            if(!open[i])
            {
                if(openCase == "")
                {
                    open[i] = true;
                    openCase = gridviewTab.Name;
                    gridviewTab.Background = new SolidColorBrush(Colors.White);
                }
                else
                {
                    //MessageDialog autreCaseOuverte = new MessageDialog("fermer l'autre case ouverte d'abord");
                    //autreCaseOuverte.ShowAsync();
                }
            }
            else
            {
                open[i] = false;
                openCase = "";
                gridviewTab.Background = new SolidColorBrush(Colors.WhiteSmoke);
            }

            //if(openCase != "")
            //{
            //    open[i] = !open[i];
            //    if (open[i] == true)
            //    {
            //        openCase = gridviewTab.Name;
            //        gridviewTab.Background = new SolidColorBrush(Colors.White);
            //    }
            //    else
            //    {
            //        openCase = "";
            //        gridviewTab.Background = new SolidColorBrush(Colors.WhiteSmoke);
            //    }

            //}
            
          
        }
        private void gridViewTabMedicament11_ItemClick(object sender, ItemClickEventArgs e)
        {
            mediSelected = true;
            System.Diagnostics.Debug.WriteLine(((Medicament)e.ClickedItem).nom);
            IndexgridviewSelect = 0;
            medicamentSelect = (Medicament)e.ClickedItem;
            indexMediSelect = gridViewTabMedicament11.Items.IndexOf(e.ClickedItem) ;
            

        }
        private void gridViewTabMedicament11_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(0, gridViewTabMedicament11);
        }
        

        private async void gridViewTabMedicament11_Tapped(object sender, TappedRoutedEventArgs e)
        {

            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)

                ExecSingleTap(0, gridViewTabMedicament11);            
        }
        private void gridViewTabMedicament12_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(1, gridViewTabMedicament12);
        }


        private async void gridViewTabMedicament12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(1, gridViewTabMedicament12);
        }

        private void gridViewTabMedicament13_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(2, gridViewTabMedicament13);
        }


        private async void gridViewTabMedicament13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(2, gridViewTabMedicament13);
        }

        private void gridViewTabMedicament14_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(3, gridViewTabMedicament14);
        }


        private async void gridViewTabMedicament14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(3, gridViewTabMedicament14);
        }

        private void gridViewTabMedicament15_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(4, gridViewTabMedicament15);
        }


        private async void gridViewTabMedicament15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(4, gridViewTabMedicament15);
        }

        private void gridViewTabMedicament16_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(5, gridViewTabMedicament16);
        }


        private async void gridViewTabMedicament16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(5, gridViewTabMedicament16);
        }

        private void gridViewTabMedicament17_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(6, gridViewTabMedicament17);
        }


        private async void gridViewTabMedicament17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(6, gridViewTabMedicament17);
        }

        private void gridViewTabMedicament21_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(7, gridViewTabMedicament21);
        }


        private async void gridViewTabMedicament21_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(7, gridViewTabMedicament21);
        }

        private void gridViewTabMedicament22_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(8, gridViewTabMedicament22);
        }


        private async void gridViewTabMedicament22_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(8, gridViewTabMedicament22);
        }

        private void gridViewTabMedicament23_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(9, gridViewTabMedicament23);
        }


        private async void gridViewTabMedicament23_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(9, gridViewTabMedicament23);
        }

        private void gridViewTabMedicament24_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(10, gridViewTabMedicament24);
        }


        private async void gridViewTabMedicament24_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(10, gridViewTabMedicament24);
        }

        private void gridViewTabMedicament25_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(11, gridViewTabMedicament25);
        }


        private async void gridViewTabMedicament25_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(11, gridViewTabMedicament25);
        }

        private void gridViewTabMedicament26_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(12, gridViewTabMedicament26);
        }


        private async void gridViewTabMedicament26_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(12, gridViewTabMedicament26);
        }

        private void gridViewTabMedicament27_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(13, gridViewTabMedicament27);
        }


        private async void gridViewTabMedicament27_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(13, gridViewTabMedicament27);
        }

        private void gridViewTabMedicament31_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(14, gridViewTabMedicament31);
        }


        private async void gridViewTabMedicament31_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(14, gridViewTabMedicament31);
        }

        private void gridViewTabMedicament32_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(15, gridViewTabMedicament32);
        }


        private async void gridViewTabMedicament32_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(15, gridViewTabMedicament32);
        }

        private void gridViewTabMedicament33_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(16, gridViewTabMedicament33);
        }


        private async void gridViewTabMedicament33_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(16, gridViewTabMedicament33);
        }

        private void gridViewTabMedicament34_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(17, gridViewTabMedicament34);
        }


        private async void gridViewTabMedicament34_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(17, gridViewTabMedicament34);
        }

        private void gridViewTabMedicament35_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(18, gridViewTabMedicament35);
        }


        private async void gridViewTabMedicament35_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(18, gridViewTabMedicament35);
        }

        private void gridViewTabMedicament36_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(19, gridViewTabMedicament36);
        }


        private async void gridViewTabMedicament36_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(19, gridViewTabMedicament36);
        }

        private void gridViewTabMedicament37_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            this.singleTap = false;
            ExecDoubleTap(20, gridViewTabMedicament37);
        }

        private void RetourPrescription_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            retourPrescriptionV = true;
            this.Frame.Navigate(typeof(Experience), null);
        }

        private void gridViewTabMedicament12_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament13_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament14_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament15_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament16_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament17_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament21_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament22_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament23_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament24_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament25_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament26_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament27_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament31_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament32_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament33_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament34_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament35_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament36_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void gridViewTabMedicament37_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }

        private async void finirExp_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            int h = 0;
            resultatFinal = new Resultat();
            resultatFinal.Code_Participant = App.codeParticipant;
            resultatFinal.momentJour = new List<MomentJour>();

            for (int p = 0; p < 3; p++)
            {
                for (int k = 0; k < 7; k++)
                {
                    resultatFinal.momentJour.Add(new MomentJour { abs = p, coord = k, listMedicaments = tabListMedicaments[h] });
                    //resultatFinal.momentJour[k].abs = p;
                    //resultatFinal.momentJour[k].coord = k;
                    //resultatFinal.momentJour[k].listMedicaments = tabListMedicaments[h];
                    h++;
                }

            }
            
            ExpPrescResulat = new ExperiencePrescResult();
            ExpPrescResulat.experimentateur = App.experimentateur;
            ExpPrescResulat.date = App.date;
            ExpPrescResulat.Code_Participant = App.codeParticipant;
            ExpPrescResulat.type = App.type;
            ExpPrescResulat.utilisation = App.utilisation;
            ExpPrescResulat.pilulier = App.pilulier;
            ExpPrescResulat.format = App.format;
            ExpPrescResulat.mode = App.mode;
            ExpPrescResulat.navigation = App.navigation;
            ExpPrescResulat.etapes = new List<Etape>();
            ExpPrescResulat.prescription = App.prescriptionTabSelect;
            ExpPrescResulat.Resultat = resultatFinal;

            

            ListExpPrescResulat.Add(ExpPrescResulat);
            await FileIO.WriteTextAsync(sampleFileExp, JsonConvert.SerializeObject(ListExpPrescResulat));

            var stream = await sampleFileExp.OpenStreamForReadAsync();
            bytes = new byte[(int)stream.Length];
            stream.Read(bytes, 0, (int)stream.Length);
            //fileToByte();
            await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFileExp.Path);
            //MessageDialog experiencefinie = new MessageDialog("Experience terminée");
            // await experiencefinie.ShowAsync();
            enregistrementServeur();


        }
        private async void enregistrementServeur()

        {
            if (!authenPopup.IsOpen && App.authentification == false)
            {
                authenPopup.IsOpen = true;

            }
            else
            {
                if (!sharingSpacePopup.IsOpen)
                {
                    sharingSpacePopup.IsOpen = true;

                }
                var stream = await sampleFileExp.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                //fileToByte();
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFileExp.Path);

                //SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();
                for (int i = 0; i < a; i++)
                {

                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }
            }
        }



        private async void gridViewTabMedicament37_Tapped(object sender, TappedRoutedEventArgs e)
        {
            this.singleTap = true;
            await Task.Delay(200);
            if (this.singleTap)
                ExecSingleTap(20, gridViewTabMedicament37);
        }

        private void delete_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if(medicamentSelect != null)
            {
                tabListMedicaments[IndexgridviewSelect][indexMediSelect].nbrTab--;
                gridViewTabMedicament[IndexgridviewSelect].ItemsSource = null;
                gridViewTabMedicament[IndexgridviewSelect].ItemsSource = tabListMedicaments[IndexgridviewSelect];
                listMedicament[j].nbr--;

                for(int l =0;l <listMedicament.Count; l++)
                {
                    if(listMedicament[l].nom == medicamentSelect.nom)
                    {
                        listMedicament[l].nbr++;
                        break;
                    }
                }

                //foreach (Medicament medi in listMedicament)
                //{
                //    if (medi.nom == medicamentSelect.nom)
                //    {
                //        medi.nbr++;
                //        break;
                //    }
                //}
                gridViewBoiteMedicaments.ItemsSource = null;
                gridViewBoiteMedicaments.ItemsSource = listMedicament;
            }
        }
        private void OnLayoutUpdatedIndex(object sender, object e)
        {
            if (indexPopupChild.ActualWidth == 0 && indexPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.indexPopup.HorizontalOffset;
            double ActualVerticalOffset = this.indexPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - indexPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - indexPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.indexPopup.HorizontalOffset = NewHorizontalOffset;
                this.indexPopup.VerticalOffset = NewVerticalOffset;
            }
        }

        private void quitSharingPopup_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (sharingSpacePopup.IsOpen)
            {
                sharingSpacePopup.IsOpen = false;
            }
        }

        private void quitAuthenPopup_Click(object sender, RoutedEventArgs e)
        {
            if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
        }

        private void quitIndexPopup_Click(object sender, RoutedEventArgs e)
        {
            if (indexPopup.IsOpen)
            {
                indexPopup.IsOpen = false;
            }
        }

        private void OnLayoutUpdatedSharing(object sender, object e)
        {
            if (sharingSpacePopupChild.ActualWidth == 0 && sharingSpacePopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.sharingSpacePopup.HorizontalOffset;
            double ActualVerticalOffset = this.sharingSpacePopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - sharingSpacePopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - sharingSpacePopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.sharingSpacePopup.HorizontalOffset = NewHorizontalOffset;
                this.sharingSpacePopup.VerticalOffset = NewVerticalOffset;
            }
        }
        private void OnLayoutUpdatedAuthen(object sender, object e)
        {
            if (authenPopupChild.ActualWidth == 0 && authenPopupChild.ActualHeight == 0)
            {
                return;
            }

            double ActualHorizontalOffset = this.authenPopup.HorizontalOffset;
            double ActualVerticalOffset = this.authenPopup.VerticalOffset;

            double NewHorizontalOffset = (Window.Current.Bounds.Width - authenPopupChild.ActualWidth) / 2;
            double NewVerticalOffset = (Window.Current.Bounds.Height - authenPopupChild.ActualHeight) / 2;

            if (ActualHorizontalOffset != NewHorizontalOffset || ActualVerticalOffset != NewVerticalOffset)
            {
                this.authenPopup.HorizontalOffset = NewHorizontalOffset;
                this.authenPopup.VerticalOffset = NewVerticalOffset;
            }
        }
        public void showSharingSpaceInfo()
        {
            idSharingSpace.Clear();
            // foreach (CheckBox checkbox in checkBoxListSharingSpace)
            for (int i = 0; i < checkBoxListSharingSpace.Count; i++)
            {
                if (checkBoxListSharingSpace[i].IsChecked == true)
                {
                    string[] split = elementList[i].Split('#');
                    idSharingSpace.Add(split[1]);
                }
            }
        }
        private async void showShSpInfo_Click(object sender, RoutedEventArgs e)
        {
            showSharingSpaceInfo();
            if (idSharingSpace.Count != 0)
            {

                Index.getIndex();
                List<string> nom_cpt = Index.getnom_cpt();
                checkBoxListIndex.Clear();
                IndexStackPanel1.Children.Clear();

                for (int i = 0; i < nom_cpt.Count; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nom_cpt[i];
                    newCheckBox.Name = "checkbox" + i;

                    checkBoxListIndex.Add(newCheckBox);
                    IndexStackPanel1.Children.Add(newCheckBox);

                }
                if (!indexPopup.IsOpen) { indexPopup.IsOpen = true; }
            }
            else
            {
                MessageDialog SharingSpaceNotSelected = new MessageDialog("Veuillez séléctionner l'espace de partage");
                await SharingSpaceNotSelected.ShowAsync();
            }

        }
        public async Task Post(string website, string args)
        {

            //init client
            Windows.Web.Http.HttpClient hc = new Windows.Web.Http.HttpClient();
            //send post request
            Windows.Web.Http.HttpResponseMessage hrm = await hc.PostAsync(new Uri(website), new HttpStringContent(args));
            //show response
            TxtResponse = await hrm.Content.ReadAsStringAsync();
            App.txtresponseLogin = TxtResponse;
            //System.Diagnostics.Debug.Write(TxtResponse);
            hc.Dispose();
        }

        private async void connecter_Click(object sender, RoutedEventArgs e)
        {
            await Post("http://memorae.hds.utc.fr/api/amc2/Checkaccess", "{\"login\":" + "\"" + login.Text + "\"" + ",\"pass\":" + "\"" + pass.Password + "\"" + "}");
            if (TxtResponse != "\"Wrong login or password\"" && TxtResponse != "[]")
            {
                App.logIn = login.Text;
                App.password = pass.Password;
                App.authentification = true;

                if (authenPopup.IsOpen) { authenPopup.IsOpen = false; }
                //fileToByte();
                var stream = await sampleFileExp.OpenStreamForReadAsync();
                bytes = new byte[(int)stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
                await SharingSpace.UploadFile(bytes, new Uri("http://memorae.hds.utc.fr/api/upload1.php"), sampleFileExp.Path);

                SharingSpace.getSharingSapce();

                int a = SharingSpace.getIdlIstCount();
                //System.Diagnostics.Debug.Write("\na= " + a);

                nameList = SharingSpace.getNameListSharingSpace();
                elementList = SharingSpace.getElementListSharingSpace();

                checkBoxListSharingSpace.Clear();
                SharingSpaceStackPanel.Children.Clear();

                for (int i = 0; i < a; i++)
                {
                    //Create the button
                    CheckBox newCheckBox = new CheckBox();
                    newCheckBox.Content = nameList[i];
                    //newCheckBox.Margin = new Thickness(173, 20, 0, 0);
                    newCheckBox.Name = "sharingSpace" + i;

                    //   System.Diagnostics.Debug.Write("\n element= " + newCheckBox.IsChecked.Value);
                    //   System.Diagnostics.Debug.Write("\n element= " + elementList[i]);

                    checkBoxListSharingSpace.Add(newCheckBox);
                    SharingSpaceStackPanel.Children.Add(newCheckBox);

                }
                if (!sharingSpacePopup.IsOpen) { sharingSpacePopup.IsOpen = true; }



            }
            else
            {
                MessageDialog loginMessage = new MessageDialog("Le login ou le mot de passe ne correspond à aucun compte!");
                await loginMessage.ShowAsync();
            }
        }
        private async void indexSave_Click(object sender, RoutedEventArgs e)
        {
            Index.showInfo(checkBoxListIndex, sampleFileExp, idSharingSpace);
            MessageDialog saveServerFile = new MessageDialog("L'experience a bien été enregistrée");
            await saveServerFile.ShowAsync();
        }

    }
}
